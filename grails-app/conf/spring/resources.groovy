import org.springframework.web.servlet.i18n.SessionLocaleResolver

// Place your Spring DSL code here
beans = {
    localeResolver(SessionLocaleResolver) {
        Locale.default = Locale.ITALY
        defaultLocale = Locale.default
    }
    myInterceptor(org.springframework.boot.context.embedded.FilterRegistrationBean) {
        filter = bean(security.InterceptFilter)
        order = org.springframework.core.Ordered.HIGHEST_PRECEDENCE
    }
}
