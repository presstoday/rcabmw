<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>Elenco</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
    <style>
    .not-active {
        pointer-events: none;
        cursor: default;
        opacity: 0.65!important;
    }
    </style>
</head>

<body>
<div class="col-md-12 col-md-push-11"><asset:image src="/loghi/mansutti.png" height="30px" style="margin-bottom: 25px;" /></div>
<div class="row">
    <div class="col-md-12 col-xs-10">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto " > ${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.message =="RICHIESTA_INVIATA"}"><script type="text/javascript">swal({
            title: "RICHIESTA INVIATA!",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:if><g:elseif test="${flash.message=="POSTICIPATA"}" ><script type="text/javascript">swal({
        title: "Polizza posticipata!",
        type: "warning",
        timer: "1800",
        showConfirmButton: false
        });</script></g:elseif>
        <g:if test="${flash.errorePratica}">
            <div class="labelErrore marginesotto">
                <p>Il caricamento delle pratiche ha riportato i seguenti errori:</p>
                <g:each var="errore" in="${flash.errorePratica}">
                    <p>${errore.toString().replaceAll("\\<.*?>","")}</p>
                </g:each>
            </div>
        </g:if>
        <g:elseif test="${flash.praticheNuove}">
            <div class="labelErrore marginesotto">
                <p>PRATICHE CORRETTAMENTE CARICATE:</p>
                <g:each var="messageP" in="${flash.praticheNuove}">
                    <p>${messageP.toString().replaceAll("\\<.*?>","")}</p>
                </g:each>
            </div>
        </g:elseif>
        <div class="col-md-12 col-xs-12 col-xs-offset-1">
            <f:form  id="listform" method="get">
                <div class="col-md-3 col-xs-6 col-xs-pull-1  area-ricerca">
                    <div class="input-group input-ricerca">
                        <input type="text" class="form-control" placeholder="ricerca pratiche" name="search" value="${params.search}">
                        <span class="input-group-btn">
                            <button type="submit" id="button_search" class="btn btn-secondary btn-ricerca"><i class="glyphicon glyphicon-search"></i></button>
                        </span>
                    </div>
                </div>
                <sec:ifHasRole role="ADMIN">
                    <div class="col-md-2 col-xs-3 col-md-pull-1">
                        <a href="#" id="new-pratica" class="btn  btn-small" title="Nuova pratica"><b>Caricare pratiche</b> <i class="fa fa-upload"></i></a>
                    </div>
                    <div class="col-md-2 col-xs-3 col-md-pull-1" style="margin-bottom: 4px;">
                        <a href="${createLink(controller: "polizze", action: "generaFilePolizzeattivate")}" target="_blank" class="btn  btn-small" title="Estrazione polizze"><b>Estrarre polizze </b> <i class="fa fa-download"></i></a>
                    </div>
                    <div class="col-md-2 col-xs-3 col-md-pull-1" style="margin-bottom: 4px;">
                        <a href="${createLink(controller: "polizze", action: "scaricaFascicoloInformativo")}" target="_blank" class="btn  btn-small" title="Scaricare fascicolo informativo"><b>Fascicolo informativo</b> <i class="fa fa-download"></i></a>
                    </div>
                    <div class="col-md-2 col-xs-3" style="margin-bottom: 4px;">
                        <a href="${createLink(controller: "polizze", action: "scaricamoduloAdesione")}" target="_blank" class="btn  btn-small" title="scaricare modulo di adesione"><b>modulo di adesione</b> <i class="fa fa-download"></i></a>
                    </div>
                </sec:ifHasRole>
                <sec:ifHasNotRole role="ADMIN">
                    <div class="col-md-3 col-xs-2" style="margin-bottom: 4px;">
                        <a href="${createLink(controller: "polizze", action: "scaricaFascicoloInformativo")}" target="_blank" class="btn  btn-small" title="Scaricare fascicolo informativo"><b>Fascicolo informativo</b> <i class="fa fa-download"></i></a>
                    </div>
                    <div class="col-md-3 col-xs-2 col-xs-push-1" style="margin-bottom: 4px;">
                        <a href="${createLink(controller: "polizze", action: "scaricamoduloAdesione")}" target="_blank" class="btn  btn-small" title="scaricare modulo di adesione"><b>modulo di adesione</b> <i class="fa fa-download"></i></a>
                    </div>
                </sec:ifHasNotRole>
            </f:form>
        </div>
       <sec:ifHasRole role="ADMIN">
            <div class="col-md-12 col-xs-12">
                <a href="${createLink(action: "nuovaPolizza")}" class="btn btn-primary btn-xs">Nuova polizza</a>
            </div>
        </sec:ifHasRole>
        <div class="table-responsive col-md-12 col-xs-12">
            <table class="table table-condensed" id="tabellaPolizza">
                <thead>
                <sec:ifHasNotRole role="DEALER">
                    <th class="text-left vcenter-column">DEALER</th>
                </sec:ifHasNotRole>
                    <th class="text-justify"></th>
                <sec:ifHasRole role="ADMINPOSTICIPATI">
                    <th class="text-left">DATA POSTICIPATA</th>
                </sec:ifHasRole>

                    <th class="text-left">STATO</th>

                    <th class="text-left">NO. PRATICA</th>
                    <th class="text-left">ASSICURATO</th>
                    <th class="text-left">PARTITA IVA</th>
                    <th class="text-center">SESSO</th>
                    <th class="text-center">CERTIFICATO<br>DI POLIZZA</th>
                    <th class="text-center">MODULO<br>DI ADESIONE</th>
                </tr>
                </thead>
                <tbody>
                <g:each var="polizza" in="${polizze}">
                    <input type="hidden" name="idPolizzasel" id="idPolizzasel" value="${polizza.id}"/>
                    <tr class="polizza">
                        <sec:ifHasNotRole role="DEALER">
                            <td class="text-left vcenter-column dealerNome" data-id="${polizza.dealer.id}"  id="dealerNome" name="dealerNome" value="${polizza.dealer.id}"><b>${polizza.dealer.ragioneSociale}</b><br>${polizza.dealer.indirizzo}, ${polizza.dealer.provincia}</td>
                        </sec:ifHasNotRole>
                        <sec:ifHasRole role="ADMINPOSTICIPATI">
                            <td class="text-left vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA POSTICIPATA"}"><i class="fa fa-pencil" style="cursor: pointer; font-size: 1.25em;" onclick="return modalPolizza(${polizza.id});"></i></g:if>%{--<g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA"}"><a class="iconoPdf" href="${createLink(controller: "polizze", action: "modulo_polizza", id: polizza.id)}" target="_blank"  title="Modulo polizza ${polizza.noPolizza}"><i class="fa fa-file-pdf-o" style="cursor: pointer; font-size: 1.25em;"></i></a></g:if>--}%</td>
                            <td class="text-left">${polizza.dataPosticipata?.format("dd-MM-yyyy")} </td>
                        </sec:ifHasRole>
                        <sec:ifHasNotRole role="ADMINPOSTICIPATI">
                            <td class="text-left vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="PREVENTIVO"}"><i class="fa fa-pencil" style="cursor: pointer; font-size: 1.25em;" onclick="return modalPolizza(${polizza.id});"></i></g:if>%{--<g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA"}"><a class="iconoPdf" href="${createLink(controller: "polizze", action: "modulo_polizza", id: polizza.id)}" target="_blank"  title="Modulo polizza ${polizza.noPolizza}"><i class="fa fa-file-pdf-o" style="cursor: pointer; font-size: 1.25em;"></i></a></g:if>--}%</td>
                        </sec:ifHasNotRole>
                        <td class="text-left vcenter-column" id="statoPolizza"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA POSTICIPATA"}">POSTICIPATA</g:if><g:else>${polizza.stato.toString().toUpperCase()}</g:else> </td>
                        <td class="text-left vcenter-column">${polizza.noPratica}%{--<sup class="dealerNome" data-id="${polizza.id}"  id="dealerNome" name="dealerNome" value="${polizza.id}"></sup>--}%</td>
                        <td class="text-justify vcenter-column"><g:if test="${polizza.tipoContraente.toString()=="garante" }">${polizza.nomeGarante.toString().toUpperCase()} ${polizza.cognomeGarante.toString().toUpperCase()}</g:if><g:else>${polizza.nomeInt.toString().toUpperCase()} ${polizza.cognomeInt.toString().toUpperCase()}</g:else></td>
                        <td class="text-justify vcenter-column"><g:if test="${polizza.tipoContraente.toString()=="garante" }">${polizza.partitaIvaGarante}</g:if><g:else>${polizza.partitaIvaInt}</g:else></td>
                        <td class="text-center vcenter-column"><g:if test="${polizza.tipoContraente.toString()=="garante" }">${polizza.tipoClienteGarante.value.toString().toUpperCase()}</g:if><g:else>${polizza.tipoClienteInt.value.toString().toUpperCase()}</g:else></td>
                        <td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA" }"><a class="bottoneDoc" href="${createLink(controller: "polizze", action: "scaricaCertificato", id: polizza.id)}" target="_blank"  title="Anticipo polizza ${polizza.noPolizza}"><i class="fa fa-file-pdf-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a></g:if></td>
                        <td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="RICHIESTA INVIATA" }"><a class="bottoneDoc" href="${createLink(controller: "polizze", action: "compilaModuloSalvato", id: polizza.id)}" target="_blank"  title="Modulo Adessione ${polizza.noPolizza}"><i class="fa fa-file-pdf-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a></g:if></td>
                     %{--<td class="text-center vcenter-column"><input type="checkbox"  class="centered annullata" name="annullata${polizza.id}"  value="${polizza.id}" id="annullata${polizza.id}" <g:if test="${polizza.annullata}">checked="checked" </g:if>/></td>--}%
                    </tr>
                </g:each>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="18" class="text-center"><b>Sono state trovate ${polizze?.totalCount ?: 0} polizze</b></th>
                </tr>
                </tfoot>
            </table>
        </div>
        <nav aria-label="Page navigation">
            <ul class="pagination pull-right">
                <g:if test="${totalPages>1}">
                <li>
                    <link:list page="1" controller="${controllerName}">
                        <span aria-hidden="true">&laquo;</span>
                    </link:list>
                </li>
                </g:if>
                <g:each var="p" in="${pages}">
                    <g:if test="${p == page}"><li class="active"></g:if><g:else><li></g:else>
                    <g:if test="${params.search}"><link:list page="${p}" controller="${controllerName}" search="${params.search}">${p}</link:list></g:if>
                    <g:else><link:list page="${p}" controller="${controllerName}">${p}</link:list></g:else>
                    </li>
                </g:each>
                <g:if test="${totalPages>1}">
                <li>
                    <link:list page="${totalPages}" controller="${controllerName}">
                        <span aria-hidden="true">&raquo;</span>
                    </link:list>
                </li>
                </g:if>
            </ul>
        </nav>

    </div>
    <div class="modal modal-wide fade windowPolizza" tabindex="-1" id="modalPolizza" role="dialog" aria-labelledby="myLargeModalLabel" style="overflow-y: scroll; ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header insPolizzaHeader">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 class="text-center testo-navBarTitolo"><fa:icon name="newspaper-o"/> Inserimento Polizza</h3>
                </div>
                <f:form action="${createLink(action: "aggiornaPolizza")}" id="polizzaForm"  method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="datiPoli"></div>
                        <div class="bs-example">
                            <div id="tabs">
                                <ul class="nav nav-tabs">
                                    <sec:ifHasNotRole role="ADMINPOSTICIPATI">
                                        <li class="active"><a  href="#sectionA" class="legendaTabs"><span class="fa fa-user"></span> DATI DEL CLIENTE</a></li>
                                        <li><a  href="#sectionB" class="legendaTabs"><span class="fa fa-files-o"></span> ACQ. POSIZIONE ASSICURATIVA</a></li>
                                        <li><a  href="#sectionC" class="legendaTabs"><span class="fa fa-users"></span> DATI DEL CONCESSIONARIO</a></li>
                                    </sec:ifHasNotRole>
                                    <sec:ifHasRole role="ADMINPOSTICIPATI">
                                        <li class="active"><a  href="#sectionA" class="legendaTabs" data-toggle="tab"><span class="fa fa-user"></span> DATI DEL CLIENTE</a></li>
                                        <li><a  href="#sectionB" class="legendaTabs" data-toggle="tab"><span class="fa fa-files-o"></span> ACQ. POSIZIONE ASSICURATIVA</a></li>
                                        <li><a  href="#sectionC" class="legendaTabs" data-toggle="tab"><span class="fa fa-users"></span> DATI DEL CONCESSIONARIO</a></li>
                                    </sec:ifHasRole>
                                </ul>
                            </div>
                            <div class="tab-content">
                                    <div id="sectionA" class="tab-pane fade in active">
                                        <input type="hidden" name="idPolizza" id="idPolizza" value=""/>
                                    <sec:ifHasNotRole role="ADMINPOSTICIPATI">
                                        <div class="col-xs-6 col-md-6 marginesopra" >
                                            <label class="testoParagraph control-label ">Se l'intestatario è diverso dal proprietario del veicolo:</label></div>
                                        <div class="col-xs-6 col-md-2 col-md-pull-1 marginesopra">
                                            <f:radiobutton  class="centered" name="tipoContraente"  value="GARANTE" id="garante"  text="SI"/>
                                            <f:radiobutton  class="centered" name="tipoContraente"  value="INTESTATARIO" id="cliente" checked="true" text="NO"/>
                                        </div>
                                        <div class="col-xs-6 col-md-4 marginesopra">
                                            <label class="testoParagraph control-label" >NR.PRATICA</label>
                                            <input class="form-control testoParagraph" type="text" name="noPratica"  id="noPratica" value="" readonly="true"/>
                                        </div>
                                    </sec:ifHasNotRole>
                                    <sec:ifHasRole role="ADMINPOSTICIPATI">
                                        <div class="col-xs-6 col-md-12">
                                            <div class="col-xs-6 col-md-4" style="margin-left: -15px;">
                                                <label class="testoParagraph control-label" >NR.PRATICA</label>
                                                <input class="form-control testoParagraph" type="text" name="noPratica"  id="noPratica" value="" readonly="true"/>
                                            </div>
                                        </div>
                                    </sec:ifHasRole>
                                        <div class="col-xs-6 col-md-4 marginesopra">
                                            <label class="testoParagraph control-label ">COGNOME</label>
                                            <input class="form-control testoParagraph" type="text" name="cognomeInt" id="cognomeInt" value="" readonly="true"/>
                                        </div>
                                        <div class="col-xs-6 col-md-4 marginesopra">
                                            <label class="testoParagraph control-label ">NOME</label>
                                            <input class="form-control testoParagraph" type="text" name="nomeInt" id="nomeInt" value="" readonly="true"/>
                                        </div>
                                        <div class="col-xs-6 col-md-4 marginesopra">
                                            <label class="testoParagraph control-label">C.FISCALE / P.IVA</label>
                                            <input class="form-control testoParagraph" type="text" name="partitaIvaInt"  id="partitaIvaInt" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-5">
                                            <label class="testoParagraph control-label">INDIRIZZO</label>
                                            <input  type="text" name="indirizzoInt" class="form-control testoParagraph"  id="indirizzoInt" readonly="true" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-5">
                                            <label class="testoParagraph control-label">LOCALITA'</label>
                                            <input type="text" name="localitaInt" class="form-control testoParagraph" id="localitaInt" readonly="true" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-2" >
                                            <label class="testoParagraph control-label">SESSO</label>
                                            %{--<input class="form-control testoParagraph" type="text" name="tipoClienteInt" id="tipoClienteInt" value="" readonly="true"/>--}%
                                            <select class="form-control testoParagraph" name="tipoClienteInt" id="tipoClienteInt" disabled="true">
                                                <option value="F" selected>F</option>
                                                <option value="M" >M</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-6 col-md-4 marginesotto">
                                            <label class="testoParagraph control-label">CELLULARE</label>
                                            <input class="form-control testoParagraph" type="text" name="cellulare"  id="cellulare" value=""/>
                                        </div>
                                        <div class="col-xs-6 col-md-4 marginesotto">
                                            <label class="testoParagraph control-label">EMAIL</label>
                                            <input class="form-control testoParagraph" type="text" name="emailInt"  id="emailInt" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-2 marginesotto">
                                            <label class="testoParagraph control-label">PROVINCIA</label>
                                           %{-- <input class="form-control testoParagraph" type="text" name="provinciaInt"  id="provinciaInt" readonly="true" value="" />--}%
                                            <select class="form-control testoParagraph" name="provinciaInt" id="provinciaInt" disabled="true" >
                                                <g:each var="provincia" in="${province}">
                                                    <option>${provincia}</option>
                                                </g:each>
                                            </select>
                                        </div>
                                        <div class="col-xs-6 col-md-2 marginesotto">
                                            <label class="testoParagraph control-label">CAP</label>
                                            <input type="text" name="capInt" class="form-control testoParagraph" id="capInt" readonly="true" value="" />
                                        </div>
                                        <div class="row"></div>
                                        <legend class="legenda"><span class="fa fa-motorcycle"></span> DATI DEL MOTOVEICOLO</legend>
                                        <div class="col-xs-6 col-md-3">
                                            <label class="testoParagraph targaLabel control-label">TARGA</label>
                                            <f:input control-class="form"  type="text" name="targa" class="maiuscola testoParagraph"  id="targa" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <label class="testoParagraph control-label">TELAIO</label>
                                            <f:input control-class="form" type="text" name="telaio" class="maiuscola testoParagraph"  id="telaio" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <label class="testoParagraph control-label">MARCHIO VETTURA</label>
                                            <f:input control-class="form" type="text" name="marchio"  class="maiuscola testoParagraph" id="marchio" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <label class="testoParagraph control-label">DESC. VERSIONE</label>
                                            <f:input control-class="form" type="text" name="versione"  id="versione" class="maiuscola testoParagraph" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <label class="testoParagraph control-label" style="margin-top: 20px;">DATA IMMATR.</label>
                                            <f:input control-class="form" type="text" name="dataImmatricolazione"  id="dataImmatricolazione" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true" data-date-end-date="0d"
                                                     data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph"
                                                     data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" required="true"/>
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <label class="testoParagraph control-label" style="margin-top: 20px;">CV</label>
                                            <f:input control-class="form" type="text" name="cv"  class="maiuscola testoParagraph" id="cv" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-3" style="margin-bottom: 20px; margin-top: 20px;">
                                            <label class="testoParagraph control-label">VALORE FATTURA (&euro;)</label>
                                            <f:input control-class="form" type="number" name="valoreAssicurato"  required="true" id="valoreAssicurato" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-1" style="margin-top: 20px;">
                                            <label class="testoParagraph control-label">CATEGORIA</label>
                                            <f:checkbox  class="centered " name="motoveicolo"  value="" id="motoveicolo" text="MOTOVEICOLO" checked="true"  disabled="true"/>
                                        </div>
                                        <div class="row"></div>
                                        <legend class="legenda"><span class="fa fa-calendar"></span> DECORRENZA POLIZZA <i>(non anteriore alla data di richiesta)</i></legend>
                                        <div class="col-xs-12 col-md-12">
                                            <sec:ifHasRole role="ADMINPOSTICIPATI">
                                                <div class="col-xs-6 col-md-6">
                                                    <f:radiobutton  class="centered" name="decorrenza"  value="" id="decorrenzaAnti" checked="" text="Si richiede la copertura con decorrenza del:"/>
                                                </div>
                                            </sec:ifHasRole>

                                            <sec:ifHasNotRole role="ADMINPOSTICIPATI">
                                                <div class="col-xs-6 col-md-3">
                                                <f:radiobutton  class="centered" name="decorrenza"  value="" id="decorrenzaAnti" checked="true" text="IMMEDIATA"/>
                                                <p>Si richiede la copertura con decorrenza del</p>
                                                </div>
                                            </sec:ifHasNotRole>
                                            <div class="col-xs-6 col-md-3" style="margin-bottom: 10px; margin-top: 10px;">
                                                <f:input control-class="form" type="text" name="dataDecorrenza"  id="dataDecorrenza" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"  data-date-start-date="+1d"
                                                         data-date-end-date="+1m" data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                                         data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" required="false"/>
                                            </div>
                                            <sec:ifHasNotRole role="ADMINPOSTICIPATI">
                                            <div class="col-xs-6 col-md-3">
                                                <f:radiobutton  class="centered " name="decorrenza"  value="" id="decorrenzaPost"  text="POSTICIPATA" />
                                                <p>La polizza attualmente in uso scadrà il</p>
                                            </div>
                                            <div class="col-xs-3 col-md-3" style="margin-bottom: 20px; margin-top: 10px; ">
                                                <f:input control-class="form" type="text" name="dataPosticipata"  id="dataPosticipata" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true" data-date-start-date="1d"
                                                         data-date-end-date="+1y" data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                                         data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" required="false" />
                                            </div>
                                            </sec:ifHasNotRole>
                                        </div>
                                                <sec:ifHasRole role="ADMINPOSTICIPATI">
                                                    <legend class="legenda legendaCommenti"><span class='fa fa-list-alt'></span> COMMENTI</legend>
                                                    <div class='commenti col-md-6' id='tabellaCommenti'>
                                                    <div class='col-md-12' style='margin-top: 10px;'>
                                                    <table class='table tablecommenti' id="tablecommenti"><thead><tr> <th>Data</th><th>Commento</th></tr></thead>
                                                    <tbody>
                                                    </tbody></table></div></div>
                                                    <div class='col-md-6 icona' style='margin-top: 10px;'><span class='fa fa-plus-square fa-2x' onclick='return aggiungeCommento();'></span></div>
                                                </sec:ifHasRole>
                                        <div class="logAttivita"></div>
                                        <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                            <sec:ifHasRole role="ADMINPOSTICIPATI">
                                                <div class="col-xs-6 col-md-4">
                                                    <button type="button" id="bottonNonInteressato" onclick="return chiedeconfermaAnnulla();" class="btn btn-annulla pull-left nInteressato" data-dismiss="modal">Non interessato</button>
                                                    %{--<button type="button" id="bottonChiude" class="btn btn-annulla pull-left chiusura" data-dismiss="modal">Chiudere</button>--}%
                                                </div>

                                                <div class="col-xs-6 col-md-4">
                                                    <button type="submit" id="bottonEmmette"  class="btn btnDisabled pull-left"  disabled="disabled">Emette polizza</button>
                                                </div>
                                                <div class="col-xs-6 col-md-4">
                                                    <button href="#sectionB" data-toggle="tab" id="avanti1" name="avanti1" class="btn  pull-right disabilita" onclick="attivaTab('sectionB')">Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>
                                                </div>
                                            </sec:ifHasRole>
                                            <sec:ifHasNotRole role="ADMINPOSTICIPATI">
                                                %{--<div class="col-xs-6 col-md-6">
                                                    <a  href="${createLink(controller: "polizze", action: "compilaModulo")}" target="_blank" class="btn  btn-small not-active"  name="linkModulo" id="linkModulo" title="scaricare modulo di adesione"><b>modulo di adesione</b> <i class="fa fa-download"></i></a>
                                                 </div>--}%
                                     <div class="col-xs-6 col-md-6 col-md-push-6">
                                     %{--<button href="#sectionB" data-toggle="tab" id="avanti1" name="avanti1" class="btn  pull-right disabilita" --}%%{--onclick="attivaTab('sectionB')"--}%%{-->Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>--}%
                                                <button href="" data-toggle="tab" id="avanti1" name="avanti1" class="btn  pull-right disabilita" %{--onclick="attivaTab('sectionB')"--}%>Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>
                                                </div>
                                            </sec:ifHasNotRole>
                                                %{--<g:actionSubmit class="btn pull-right" value="Salva" action="aggiornaPolizza" />--}%
                                        </div>
                                    </div>
                                    <div id="sectionB" class="tab-pane fade">
                                        <div class="col-xs-12 col-md-12">
                                            <sec:ifHasRole role="ADMINPOSTICIPATI">
                                                <div class="col-xs-6 col-md-12">
                                                    <div class="col-xs-3 col-md-12" style="margin-top: 25px;">
                                                        <f:radiobutton  class="centered radiotipoAcquisizione" name="tipoAcquisizione"  value="NUOVA_POLIZZA" id="acquisizioneNuovaPolizza" checked="true" text="Nuova Polizza" />
                                                    </div>
                                                    <div class="col-xs-3 col-md-12" style="margin-top: 25px;">
                                                        <f:radiobutton  class="centered radiotipoAcquisizione" name="tipoAcquisizione"  value="PASSAGGIO" id="acquisizionePassaggio" text="Passaggio da altro veicolo: targa del vecchio veicolo e documento che attesti la perdita di possesso" />
                                                    </div>
                                                    <div class="col-xs-3 col-md-12" style="margin-top: 25px;">
                                                        <f:radiobutton  class="centered radiotipoAcquisizione" name="tipoAcquisizione"  value="BERSANI" id="acquisizioneBersani"  text="Bersani: targa del veicolo da cui ereditare la classe e autocertificazione stato di famiglia che attesta la convivenza con il contraente" />
                                                    </div>
                                                    <div class="col-xs-3 col-md-12" style="margin-top: 25px;">
                                                        <div class="col-xs-3 col-md-3"><label class="testoParagraph control-label">TARGA </label><f:input control-class="form"  type="text"  name="targaAcquisizione" class="maiuscola testoParagraph"  id="targaAcquisizione" value="" /></div>
                                                        <div class="col-xs-3 col-md-9">
                                                            <label class="testoParagraph control-label">DOCUMENTO</label>
                                                            %{--<f:field type="file" label="File" required="true"/>--}%
                                                            <input id="input-idAcquisizione" type="file"  class="file allegatoPassaggio" multiple data-preview-file-type="text" name="fileContentAcquisizione" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">
                                                        </div>
                                                    </div>
                                                </div>
                                            </sec:ifHasRole>
                                            <sec:ifHasNotRole role="ADMINPOSTICIPATI">
                                                <div class="col-xs-6 col-md-12">
                                                    <div class="col-xs-6 col-md-10" style="margin-top:20px;">
                                                        <label>Richiedi al Cliente la documentazione per verificare la posizione assicurativa:</label>
                                                        <button type="button" id="invioRichiesta" name="invioRichiesta" onclick="return inviaMailRichiesta();" class="btn btn-primary">Invio richiesta</button>
                                                    </div>
                                                </div>
                                            </sec:ifHasNotRole>
                                        </div>
                                            <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                                <div class="col-xs-6 col-md-6">
                                                    <a href="#sectionA" data-toggle="tab" id="indietro" class="btn pull-left button-ladda" onclick="attivaTab('sectionA')"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <button href="#sectionC" data-toggle="tab" id="avanti2" name="avanti2" class="btn  pull-right" onclick="attivaTab('sectionC')">Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>
                                                    %{--<g:actionSubmit class="btn pull-right" value="Salva" action="aggiornaPolizza" />--}%
                                                </div>
                                            </div>
                                    </div>
                                    <div id="sectionC" class="tab-pane fade">
                                        <div class="col-xs-6 col-md-5" style="margin-top: 9px;">
                                            <label class="testoParagraph control-label">RAGIONE SOCIALE</label>
                                            <f:input control-class="form" type="text" name="ragionesociale" class="maiuscol, testoParagraph" id="ragionesociale" readonly="true" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-4" style="margin-top: 9px;">
                                            <label class="testoParagraph control-label">INDIRIZZO</label>
                                            <f:input control-class="form" type="text" name="indirizzo" class="maiuscola testoParagraph"  id="indirizzo" readonly="true" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-3" style="margin-top: 9px;">
                                            <label class="testoParagraph control-label">PARTITA IVA</label>
                                            <f:input control-class="form" type="text" name="piva" class="maiuscola testoParagraph"  id="piva" readonly="true" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-5">
                                            <label class="testoParagraph control-label">COMUNE</label>
                                            <f:input control-class="form" type="text" name="comune" class="maiuscola testoParagraph" id="comune" readonly="true" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-2">
                                            <label class="testoParagraph control-label">CAP</label>
                                            <f:input control-class="form" type="text" name="cap" class="maiuscola testoParagraph" id="cap" readonly="true" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-2">
                                            <label class="testoParagraph control-label">PROV.</label>
                                            <f:input control-class="form" type="text" name="provincia" class="maiuscola testoParagraph" id="provincia" readonly="true" value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-5">
                                            <label class="testoParagraph control-label">MAIL</label>
                                            <f:input control-class="form" type="text" name="email"  id="email" class="testoParagraph" value="" />
                                        </div>

                                        <div class="col-xs-6 col-md-4">
                                            <label class="testoParagraph control-label">TELEFONO</label>
                                            <f:input control-class="form" type="text" name="telefono" class="maiuscola testoParagraph" id="telefono"  value="" />
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <label class="testoParagraph control-label">FAX</label>
                                            <f:input control-class="form" type="text" name="fax"  id="fax" class="testoParagraph"  value="" />
                                        </div>
                                        <sec:ifHasNotRole role="ADMINPOSTICIPATI">
                                            <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                                <div class="col-xs-6 col-md-6">
                                                   %{-- <a href="#sectionB" data-toggle="tab" id="indietro1" class="btn pull-left button-ladda" --}%%{--onclick="attivaTab('sectionB')"--}%%{--><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>--}%
                                                    <a href="" data-toggle="tab" id="indietro1" class="btn pull-left button-ladda" %{--onclick="attivaTab('sectionB')"--}%><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <button type="submit" id="bottonSalva" value="Salva" class="btn pull-right">Salva</button>
                                                    %{--<g:actionSubmit class="btn pull-right" value="Salva" action="aggiornaPolizza" />--}%
                                                </div>
                                            </div>
                                        </sec:ifHasNotRole>
                                        <sec:ifHasRole role="ADMINPOSTICIPATI">
                                            <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                                <div class="col-xs-6 col-md-4">
                                                    <a href="#sectionB" data-toggle="tab" id="indietro2" class="btn pull-left pull-left" onclick="attivaTab('sectionB')"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>
                                                </div>
                                            </div>
                                        </sec:ifHasRole>
                                    </div>
                            </div>
                        </div>
                    </div>
                </f:form>
            </div>
        </div>
    </div>
    <div class="modal modalCommento" id="modalCommento">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <h3 class="article-title text-center testo-navBarTitolo"> Commento</h3>
                    </div>
                    <div class="modal-body" style="background: lightgrey">
                        <div class='col-md-8' style="margin-bottom: 10px; margin-top: 10px;">
                            <label>Commento</label> <textarea name='areaLog' id='areaLog' rows='3' cols='50' onchange='return validatextarea();' class='form-control' placeholder="Inserire commenti"></textarea>
                        </div>
                        <div class='col-md-4' style="margin-bottom: 10px; margin-top: 10px;">
                            <label>Data</label>
                            <f:input control-class="form" type="text" name="dataLog"  id="dataLog" class="testoParagraph" readonly="true" value="${new Date().format("dd-MM-yyyy")}" />
                        </div>
                    </div>

                    <div class="modal-footer" style="background: lightgrey; border-bottom-left-radius: 4px; border-bottom-right-radius: 4px;">
                        <div class='col-md-8' style='margin-top: 15px;'><button type='button' id='bottonanullaComm' onclick='return anullaSegnala();' class='btn btn-annulla pull-left' data-dismiss="modal">Annulla</button></div>
                        <div class='col-md-4' style='margin-top: 15px;'><button type='button' id='bottonSalvaComm' name='bottonSalvaComm' disabled='disabled' onclick='return inserisceComm();' class='btnLog pull-right btnSalvaDisabled' data-dismiss="modal" >&nbsp;Salva&nbsp;&nbsp;</button></div>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-pratica">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center testo-navBarTitolo"> Caricamento Pratica</h3>
                    </div>
                    <div class="modal-body" style="background: lightgrey">
                        <form action="${createLink(action: "caricaPratica", params:[_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelPratica" type="file" name="excelPratica" class="file" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#new-pratica").click(function() { $("#form-pratica").modal("show"); });
    $(document).ready(function() {
        //  $('#tabellaPolizza').dataTable({bFilter: false, bInfo: false, bPaginate:false, aoColumnDefs: [{ 'bSortable': false, 'aTargets': [ 1,6 ] }]});
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    });

    var txtpartitaIvaCliente="";
    var txtNomeCliente="";
    var txtCognomeCliente="";
    var txtIndirizzoCliente="";
    var txtLocalitaCliente="";
    var txtcapCliente="";
    var txtprovinciaCliente="";
    var txtsessoCliente="";
    var txtemailCliente="";
    var txtemailDealer="";
    var txtcellCliente="";
    var txttargaCliente="";
    var txttelaioCliente="";
    var txtmarchioCliente="";
    var txtversioneCliente="";
    var txtdataImmCliente="";
    var txtcvCliente="";
    var txtvaloreCliente="";
    var txtimmCliente="";
    var txtpostCliente="";
    var txtposizionePolizza="";
    var txttargaAcquisizione="";
    function attivaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };

    function modalPolizza(x){
        $.post("${createLink(controller: "polizze", action: 'getPolizza')}",{ idPolizza: x,  _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true && response.stato=="PREVENTIVO"){
                $( ".alert" ).remove();
                $("#cognomeInt").val(response.cognomeInt);
                $("#nomeInt").val(response.nomeInt);
                $("#tipoClienteInt").val(response.tipoClienteInt);
                $("#noPratica").val(response.noPratica);
                $("#partitaIvaInt").val(response.partitaIvaInt);
                $("#indirizzoInt").val(response.indirizzoInt);
                $("#localitaInt").val(response.localitaInt);
                $("#capInt").val(response.capInt);
                $("#provinciaInt").val(response.provinciaInt);
                $("#targa").prop("disabled",false);
                $("#telaio").prop("disabled",false);
                $("#marchio").prop("disabled",false);
                $("#dataImmatricolazione").prop("disabled",false);
                $("#versione").prop("disabled",false);
                $("#motoveicolo").prop("disabled",true);
                $("#ragionesociale").val(response.dealer);
                $("#piva").val(response.piva);
                $("#indirizzo").val(response.indirizzo);
                $("#cap").val(response.cap);
                $("#provincia").val(response.provincia);
                $("#comune").val(response.comune);
                var telefono;
                if(response.telefono=="null"){
                    telefono='';
                }else{
                    telefono=response.telefono;
                }
                $("#telefono").val(telefono);
                var fax;
                if(response.fax=="null"){
                    fax='';
                }else{
                    fax=response.fax;
                }
                $("#fax").val(fax);
                var email;
                if(response.email=="null"){
                    email='';
                }else{
                    email=response.email;
                }
                $("#email").val(email);
                $("#idPolizza").val(x);
                $("#bottonSalva").prop('disabled', false);
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                removeParameters();
                $("#avanti2").prop('disabled', true);
                $("#bottonSalva").prop('disabled', true);
                $("#modalPolizza").modal({
                    keyboard: true
                });
            }else{
                if(response.risposta==true && response.stato=="POLIZZA POSTICIPATA"){
                    /*$( ".avviso" ).remove();
                    $( ".datiPoli" ).before( "<div class=\"avviso\"> <span>La polizza è stata già emessa, non può essere modficata</span></div>" );*/
                    $("#nomeInt").val(response.nomeInt).prop("disabled",true);
                    $("#cognomeInt").val(response.cognomeInt).prop("disabled",true);
                    $("#cellulare").val(response.cellulare);
                    $("#emailInt").val(response.emailInt);
                    $("#tipoClienteInt").val(response.tipoClienteInt).prop("disabled",true);
                    $("#noPratica").val(response.noPratica).prop("disabled",true);
                    $("#partitaIvaInt").val(response.partitaIvaInt);
                    $("#indirizzoInt").val(response.indirizzoInt);
                    $("#localitaInt").val(response.localitaInt);
                    $("#capInt").val(response.capInt);
                    $("#provinciaInt").val(response.provinciaInt);
                    $("#targa").val(response.targa);
                    $("#telaio").val(response.telaio);
                    $("#marchio").val(response.marchio);
                    $("#dataImmatricolazione").val(response.dataImmatricolazione);
                    $("#dataPosticipata").val(response.dataPosticipata);
                    $("#versione").val(response.versione);
                    $("#cv").val(response.cv);
                    $("#valoreAssicurato").val(response.valoreAssicurato);
                    if(response.motoveicolo==true){
                        $("#motoveicolo").attr("checked", true);
                        $("#motoveicolo").prop("disabled",true);
                    }
                    $("#immediata").attr("checked", false);
                    $("#targaAcquisizione").prop("disabled",true);
                    if($('#input-idAcquisizione').val()!=''){
                        $('#input-idAcquisizione').fileinput('clear').fileinput('disable');
                    }
                    $("#ragionesociale").val(response.dealer).prop("disabled",true);
                    $("#piva").val(response.piva).prop("disabled",true);
                    $("#indirizzo").val(response.indirizzo).prop("disabled",true);
                    $("#cap").val(response.cap).prop("disabled",true);
                    $("#provincia").val(response.provincia).prop("disabled",true);
                    $("#comune").val(response.comune).prop("disabled",true);

                    var telefono;
                    if(response.telefono=="null"){
                        telefono='';
                    }else{
                        telefono=response.telefono;
                    }
                    $("#telefono").val(telefono).prop("disabled",true);

                    var fax;
                    if(response.fax=="null"){
                        fax='';
                    }else{
                        fax=response.fax;
                    }
                    $("#fax").val(fax).prop("disabled",true);
                    var email;
                    if(response.email=="null"){
                        email='';
                    }else{
                        email=response.email;
                    }
                    $("#email").val(email).prop("disabled",true);
                    if(response.logAttivita_data){
                        var tbl = document.getElementById('tablecommenti'); // table reference
                        var tblBody = document.createElement("tbody");
                        tbl.appendChild(tblBody);
                        var trow;
                        $.each(response.logAttivita_data, function(i, star) {
                           trow = document.createElement("tr");
                           tblBody.appendChild(trow);
                           var valori=star.toString().split(',');
                            var data=new Date(valori[0]);
                            var datimese=data.getMonth() + 1;
                            var mesefix;
                            if(datimese<10){mesefix='0'+datimese} else{mesefix=''+datimese}
                            var commenti=valori[1];
                            var datareplace=commenti.toString().replace(/;/g,",");
                            createCell(tbl.rows[i+1].insertCell(tbl.rows[i+1].cells.length),(data.getDate() + '/' +mesefix+ '/' + data.getFullYear()) , 'col');
                            createCell(tbl.rows[i+1].insertCell(tbl.rows[i+1].cells.length),datareplace , 'col');

                        });
                    }
                    $("#idPolizza").val(x);
                    $("#avanti1").prop('disabled', true);
                    $("#bottonEmmette").prop('disabled', true);
                    $(".nInteressato").show();
                    //$(".chiusura").hide();
                    $("#modalPolizza").modal({
                        keyboard: true
                    });
                }
            }
        }, "json");
    }
    function createCell(cell, text, style) {
        var txt = document.createTextNode(text); // create text node
        cell.appendChild(txt);                   // append DIV to the table cell
    }
    /*function aggiornaPolizza(){
        var idPolizza=$("#idPolizza").val();
        var targa=$("#targa").val();
        var telaio=$("#telaio").val();
        var dataImmatricolazione=$("#dataImmatricolazione").val();
        var marchio=$("#marchio").val();
        var versione=$("#versione").val();
        var motoveicolo=$("#motoveicolo").prop("checked");
        var dataDecorrenza=$("#dataDecorrenza").val();
        var dataPosticipata=$("#dataPosticipata").val();
        var cv=$("#cv").val();
        var cellulare=$("#cellulare").val();
        var emailCliente=$("#emailCliente").val();
        var valoreAssicurato=$("#valoreAssicurato").val();
        var targaAcquisizone=$("#targaAcquisizone").val();
        var allegatiacquisizone=$("#allegatiacquisizone").val();
        //$.post("\${createLink(controller: "polizze", action: 'aggiornaPolizza')}",{ idPolizza: idPolizza, targa:targa, telaio:telaio, dataImmatricolazione:dataImmatricolazione, marchio:marchio, versione:versione, motoveicolo:motoveicolo,dataDecorrenza:dataDecorrenza, dataPosticipata:dataPosticipata,cv:cv,valoreAssicurato:valoreAssicurato,cellulare:cellulare,emailCliente:emailCliente,targaAcquisizone:targaAcquisizone,allegatiacquisizone:allegatiacquisizone, _csrf: "\${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $('#tabellaPolizza').load(window.location.href + ' #tabellaPolizza');
                swal({
                    title: "Polizza salvata!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                });
            }else{
                swal({
                    title: "Errore salvataggio!",
                    text: ""+ response.errore+"",
                    type: "error",
                    showConfirmButton: true
                });
            }
        }, "json");
    }*/
    function motivoSegnala(){
        var lista="";
        var idPolizza=$("#idPolizza").val();
        //$(".nInteressato").hide();
        //$(".chiusura").show();
        $("#bottonSalva").prop('disabled', true);
        $('#tablecommenti tbody').remove();
        $.ajax({url:"${createLink(controller: "polizze", action:'elencoCommenti')}",
        type:'POST',
        dataType:'json',
        cache:'false',
        data: { id:idPolizza, _csrf: "${request._csrf.token}" }
        }).done(function(response){
            if(response.logAttivita_data){
                var tbl = document.getElementById('tablecommenti'); // table reference
                var tblBody = document.createElement("tbody");
                tbl.appendChild(tblBody);
                var trow;
                $.each(response.logAttivita_data, function(i, star) {
                    trow = document.createElement("tr");
                    tblBody.appendChild(trow);
                    var valori=star.toString().split(',');
                    var data=new Date(valori[0]);
                    var datimese=data.getMonth() + 1;
                    var mesefix;
                    if(datimese<10){mesefix='0'+datimese} else{mesefix=''+datimese}
                    var commenti=valori[1];
                    var datareplace=commenti.toString().replace(/;/g,",");
                    createCell(tbl.rows[i+1].insertCell(tbl.rows[i+1].cells.length),(data.getDate() + '/' +mesefix+ '/' + data.getFullYear()) , 'col');
                    createCell(tbl.rows[i+1].insertCell(tbl.rows[i+1].cells.length),datareplace, 'col');

                });
            }else if (response.risposta ==false){
            }
        })
        $("#bottonNonInteressato").prop('disabled',false);
        $("#bottonEmmette").prop('disabled',true).addClass("btnDisabled");
    }
    function anullaSegnala(){
        $("#bottonNonInteressato").prop('disabled',false);
        $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
    }
    function chiedeconfermaAnnulla(){
        swal({
            title: "Confermi?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#1995dc",
            cancelButtonColor: "#9a161a",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm){
            //swal("Deleted!", "Your imaginary file has been deleted.", "success");
            if (isConfirm) {
                nonInteressato();
            }
        });
    }
    function nonInteressato(){

        var idPolizza=$("#idPolizza").val();
        $.post("${createLink(controller: "polizze", action: 'nonInteressato')}",{ idPolizza: idPolizza, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $('#tabellaPolizza').load(window.location.href + ' #tabellaPolizza');
                /*swal({
                    title: "Polizza annullata!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                });*/
            }else{
                swal({
                    title: "Errore aggiornamento preventivo!",
                    text: ""+ response.errore+"",
                    type: "error",
                    showConfirmButton: true,
                    confirmButtonColor: "#1995dc"
                });
            }
        }, "json");
    }
    function inserisceComm(){
        var idPolizza=$("#idPolizza").val();
        var areaLog=$("#areaLog").val();
        var dataLog=$("#dataLog").val();
        $.post("${createLink(controller: "polizze", action: 'inserisceLog')}",{ idPolizza:idPolizza,areaLog: areaLog, dataLog:dataLog, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                motivoSegnala();
                swal({
                    title: "Commento inserito!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                });
            }else{
                swal({
                    title: "Errore salvataggio commenti!",
                    text: ""+ response.errore+"",
                    type: "error",
                    showConfirmButton: true,
                    confirmButtonColor: "#9A161A"
                });
            }
        }, "json");
    }
    $(".windowPolizza").on("hidden.bs.modal", function(){
        $( ".alert" ).remove();
        $(this).find('form')[0].reset();
        if($("#dataImmatricolazione").val()!= ''){
            $("#dataImmatricolazione").data('datepicker').setDate(null);
        }
        $("#cognomeInt").prop("readonly",true);
        $("#nomeInt").prop("readonly",true);
        $("#tipoClienteInt").prop("disabled",true);
        $("#localitaInt").prop("readonly",true);
        $("#provinciaInt").prop("disabled",true);
        $("#capInt").prop("readonly",true);
        $("#indirizzoInt").prop("readonly",true);
        $('#tablecommenti tbody').remove();
        $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");

    });
    $(".modalCommento").on("hidden.bs.modal", function(){
        $("#areaLog").val('');
        $("#bottonSalvaComm").prop('disabled',true);

    });
    function verificapartitaIvaCliente(){
        var valore= $("#partitaIvaInt").val();
        if(valore!=''){ return true;}
        else{
            txtpartitaIvaCliente="inserire la partita iva";
            return false;
        }
    }
    function verificaNomeCliente(){
        var valore= $("#nomeInt").val();
        if(valore!=''){ return true;}
        else{
            txtNomeCliente="inserire il nome";
            return false;
        }
    }
    function verificaCognomeCliente(){
        var valore= $("#cognomeInt").val();
        if(valore!=''){ return true;}
        else{
            txtCognomeCliente="inserire il cognome";
            return false;
        }
    }
    function verificaIndirizzoCliente(){
        var valore= $("#indirizzoInt").val();
        if(valore!=''){ return true;}
        else{
            txtIndirizzoCliente="inserire il indirizzo";
            return false;
        }
    }
    function verificaLocalitaCliente(){
        var valore= $("#localitaInt").val();
        if(valore!=''){ return true;}
        else{
            txtLocalitaCliente="inserire la localita'";
            return false;
        }
    }
    function verificaSessoCliente(){
        var valore= $("#tipoClienteInt").val();
        if(valore!=''){ return true;}
        else{
            txtsessoCliente="inserire il sesso";
            return false;
        }
    }
    function verificaProvinciaCliente(){
        var valore= $("#provinciaInt").val();
        if(valore!='' && valore != null ){

            return true;
        }
        else{
            txtprovinciaCliente="la provincia del cliente non e' stata caricata conttatare l'assitenza";
            return false;
        }
    }
    function verificaCapCliente(){
        var valore= $("#capInt").val();
        if(valore!=''){ return true;}
        else{
            txtcapCliente="il cap del cliente non e' statao caricato conttatare l'assitenza";
            return false;
        }
    }
    function verificaEmailCliente() {
        var valore= $("#emailInt").val();
        var regemailDef=/^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;
        if(valore!='' && regemailDef.test(valore)){
            $( ".alert" ).remove();
            return true;
        }
        else{
            if(!regemailDef.test(valore) && valore!=''){ txtemailCliente="verificare il formato del campo email";}
            else {txtemailCliente="compilare il campo email";}
            return false;
        }
    }
    function verificaEmailDealer() {
        var valore= $("#email").val();
        var regemailDef=/^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;
        if(valore!='' && regemailDef.test(valore)){
            $( ".alert" ).remove();
            return true;
        }
        else{
            if(!regemailDef.test(valore) && valore!=''){ txtemailDealer="verificare il formato del campo email";}
            else {txtemailDealer="compilare il campo email del dealer";}
            return false;
        }
    }
    function verificaTarga() {
        var valore= $.trim($("#targa").val());
        var regtarga=/^[a-zA-Z]{2}[0-9]{5}$/;
        if(valore!='' && regtarga.test(valore)){
            return true;
        }
        else {
            if(!regtarga.test(valore) && valore!=''){txttargaCliente="il formato della targa è sbagliato";}
            else {txttargaCliente="compilare il campo targa";}
            return false;
        }
    }
    function verificaDataImmatricolazione(){
        var data1 = $("#dataImmatricolazione").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataImmCliente="compilare la data Immatricolazione";
            return false;
        }
    }
    function verificaTelaio() {
        var valore= $.trim($("#telaio").val());
        if (valore!=''){
            return true;
        }
        else {
            txttelaioCliente="compilare il telaio";
            return false;
        }
    }
    function verificaMarchio() {
        var valore= $.trim($("#marchio").val());
        if (valore!=''){ return true; }
        else {
            txtmarchioCliente="compilare il marchio vettura";
            return false;
        }
    }
    function verificaCV() {
        var valore= $.trim($("#cv").val());
        if (valore!=''){ return true; }
        else {
            txtmarchioCliente="compilare il cv";
            return false;
        }
    }
    function verificaVersione() {
        var valore= $.trim($("#versione").val());
        if (valore!=''){ return true; }
        else {
            txtversioneCliente="compilare la descrizione versione";
            return false;
        }
    }
    function verificaValore() {
        var valore= $.trim($("#valoreAssicurato").val());
        if (valore!=''){ return true; }
        else {
            txtvaloreCliente="compilare il valore assicurato";
            return false;
        }
    }
    function verificaPosticipata() {
        var valore= $("#dataPosticipata").val();
        if (valore!=''){ return true; }
        else {
            txtpostCliente="compilare la data di decorrenza polizza";
            return false;
        }
    }
    function verificaImmediata() {
        var valore= $("#dataDecorrenza").val();
        if (valore!=''){ return true; }
        else {
            txtpostCliente="compilare la data di decorrenza polizza";
            return false;
        }
    }
    function verificaCellulare() {
        var valore= $.trim($("#cellulare").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaareaLog(){
        var valore= $("textarea#areaLog").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificadataLog(){
        var valore= $("#dataLog").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    function validatextarea(){
        var areaLog= $("textarea#areaLog").val();
        if(areaLog!='' && verificadataLog()==true){
            $("#bottonSalvaComm").prop('disabled', false)
            $("#bottonSalvaComm").removeClass("btnSalvaDisabled")
            $("#bottonSalvaComm").addClass("btn-success");
        }else{
            $("#bottonSalvaComm").prop('disabled', true)
            $("#bottonSalvaComm").removeClass("btn-success")
            $("#bottonSalvaComm").addClass("btnSalvaDisabled");
        }
    }
    function verificatargaAcquisizione() {
        var valore= $("#targaAcquisizione").val();
        var regtarga=/^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$/;
        if(valore!='' && regtarga.test(valore)){ return true; }
        else {
            if(!regtarga.test(valore) && valore!=''){txttargaAcquisizione="il formato della targa è sbagliato";}
            else {txttargaCliente="compilare il campo targa";}
            return false;
        }
    }
    function verificaFileAcquisizione() {
        var valore= $("#input-idAcquisizione").val();

        if(valore!=''){ return true; }
        else  return false;
    }
    function verificaNuovaPolizza(){
        if($("#acquisizioneNuovaPolizza").is(":checked")){  return true;}
        else{return false;}
    }
    function verificaBersani(){
        if($("#acquisizioneBersani").is(":checked")){  return true;}
        else{return false;}
    }
    function verificaPassaggio(){
        if($("#acquisizionePassaggio").is(":checked")){  return true;}
        else{return false;}
    }
    /*function validaDataLog(){
        var data1 = $("#dataLog").val();
        if(data1 != '' && verificaareaLog()==true){
             $("#bottonSalvaComm").prop('disabled', false).removeClass("btnSalvaDisabled").addClass("btn-success");

        }else{
            $("#bottonSalvaComm").prop('disabled', true)
            $("#bottonSalvaComm").removeClass("btn-success")
            $("#bottonSalvaComm").addClass("btnSalvaDisabled");
        }
    }*/
    function aggiungeCommento (){
        $("#bottonSalvaComm").prop('disabled', true)
        $("#bottonSalvaComm").removeClass("btn-success")
        $("#bottonSalvaComm").addClass("btnSalvaDisabled");
        $("#modalCommento").modal({
           keyboard: true
        });
    }
    function inviaMailRichiesta(){
        var idPolizza=$("#idPolizza").val();
        var emailCliente=$("#emailInt").val();
        var nomeCliente=$("#nomeInt").val();
        var cognomeCliente=$("#cognomeInt").val();
        $.post("${createLink(controller: "polizze", action: 'inviaRichiesta')}",{ idPolizza: idPolizza,nomeCliente:nomeCliente,cognomeCliente:cognomeCliente,emailCliente:emailCliente, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\">Mail inviata al cliente <span class='testoAlert'></span></div>" );
                $("#avanti2").prop('disabled', false);
                //$("#invioRichiesta").prop('disabled', true);
            }else{
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\">Non e' stato possibile inviare la mail al cliente <span class='testoAlert'></span></div>" );
                $("#avanti2").prop('disabled', false);
                //$("#invioRichiesta").prop('disabled', true);
            }
        }, "json");
    }
    function setGetParameter(paramName, paramValue)
    {
        var url = $('#linkModulo').attr("href");
        var hash = location.hash;
        if(hash != '' && hash !=null){
            url = url.replace(hash, '');
            if(url != '' && url !=null){
                if (url.indexOf("?") < 0)
                    url += "?" + paramName + "=" + paramValue;
                else
                    url += "&" + paramName + "=" + paramValue;
            }

        }


        $('#linkModulo').attr("href",url);
    }
    function removeParameters()
    {
        var url = $('#linkModulo').attr("href");
        if(url != '' && url !=null){
            if (url.indexOf("?") < 0){     }
            else{
                var subfix=url.indexOf("?");
                url=url.substring(0,subfix);
            }
        }


        $('#linkModulo').attr("href",url);
    }
    $("#cliente").on("change", function(event) {
        if($("#cliente").is(":checked")){
            var idPolizza=$("#idPolizza").val();
            $.post("${createLink(controller: "polizze", action: 'getPolizza')}",{ idPolizza: idPolizza,  _csrf: "${request._csrf.token}"}, function(response) {
                if(response.risposta==true && response.stato=="PREVENTIVO"){
                    $( ".alert" ).remove();
                    $("#cognomeInt").val(response.cognomeInt);
                    $("#nomeInt").val(response.nomeInt);
                    $("#tipoClienteInt").val(response.tipoClienteInt);
                    $("#noPratica").val(response.noPratica);
                    $("#partitaIvaInt").val(response.partitaIvaInt);
                    $("#indirizzoInt").val(response.indirizzoInt);
                    $("#localitaInt").val(response.localitaInt);
                    $("#capInt").val(response.capInt);
                    $("#provinciaInt").val(response.provinciaInt);
                    $("#targa").prop("disabled",false);
                    $("#telaio").prop("disabled",false);
                    $("#marchio").prop("disabled",false);
                    $("#dataImmatricolazione").prop("disabled",false);
                    $("#versione").prop("disabled",false);
                    $("#motoveicolo").prop("disabled",true);
                    $("#ragionesociale").val(response.dealer);
                    $("#piva").val(response.piva);
                    $("#indirizzo").val(response.indirizzo);
                    $("#cap").val(response.cap);
                    $("#provincia").val(response.provincia);
                    $("#comune").val(response.comune);
                    var telefono;
                    if(response.telefono=="null"){
                        telefono='';
                    }else{
                        telefono=response.telefono;
                    }
                    $("#telefono").val(telefono);
                    var fax;
                    if(response.fax=="null"){
                        fax='';
                    }else{
                        fax=response.fax;
                    }
                    $("#fax").val(fax);
                    var email;
                    if(response.email=="null"){
                        email='';
                    }else{
                        email=response.email;
                    }
                    $("#email").val(email);
                    $("#idPolizza").val(idPolizza);
                    $("#bottonSalva").prop('disabled', true);
                    $("#avanti1").prop('disabled', true);
                    $("#linkModulo").addClass("not-active");
                    removeParameters();
                    $("#avanti2").prop('disabled', true);
                    $("#modalPolizza").modal({
                        keyboard: true
                    });
                }else{
                    if(response.risposta==true && response.stato=="POLIZZA POSTICIPATA"){
                        /*$( ".avviso" ).remove();
                         $( ".datiPoli" ).before( "<div class=\"avviso\"> <span>La polizza è stata già emessa, non può essere modficata</span></div>" );*/
                        $("#nomeInt").val(response.nomeInt).prop("disabled",true);
                        $("#cognomeInt").val(response.cognomeInt).prop("disabled",true);
                        $("#cellulare").val(response.cellulare);
                        $("#emailInt").val(response.emailInt);
                        $("#tipoClienteInt").val(response.tipoClienteInt).prop("disabled",true);
                        $("#noPratica").val(response.noPratica).prop("disabled",true);
                        $("#partitaIvaInt").val(response.partitaIvaInt);
                        $("#indirizzoInt").val(response.indirizzoInt);
                        $("#localitaInt").val(response.localitaInt);
                        $("#capInt").val(response.capInt);
                        $("#provinciaInt").val(response.provinciaInt);
                        $("#targa").val(response.targa);
                        $("#telaio").val(response.telaio);
                        $("#marchio").val(response.marchio);
                        $("#dataImmatricolazione").val(response.dataImmatricolazione);
                        $("#dataPosticipata").val(response.dataPosticipata);
                        $("#versione").val(response.versione);
                        $("#cv").val(response.cv);
                        $("#valoreAssicurato").val(response.valoreAssicurato);
                        if(response.motoveicolo==true){
                            $("#motoveicolo").attr("checked", true);
                            $("#motoveicolo").prop("disabled",true);
                        }
                        $("#immediata").attr("checked", false);
                        $("#posticipata").attr("checked", true);
                        $("#ragionesociale").val(response.dealer).prop("disabled",true);
                        $("#piva").val(response.piva).prop("disabled",true);
                        $("#indirizzo").val(response.indirizzo).prop("disabled",true);
                        $("#cap").val(response.cap).prop("disabled",true);
                        $("#provincia").val(response.provincia).prop("disabled",true);
                        $("#comune").val(response.comune).prop("disabled",true);

                        var telefono;
                        if(response.telefono=="null"){
                            telefono='';
                        }else{
                            telefono=response.telefono;
                        }
                        $("#telefono").val(telefono).prop("disabled",true);

                        var fax;
                        if(response.fax=="null"){
                            fax='';
                        }else{
                            fax=response.fax;
                        }
                        $("#fax").val(fax).prop("disabled",true);
                        var email;
                        if(response.email=="null"){
                            email='';
                        }else{
                            email=response.email;
                        }
                        $("#email").val(email).prop("disabled",true);
                        if(response.logAttivita_data){
                            var tbl = document.getElementById('tablecommenti'); // table reference
                            var tblBody = document.createElement("tbody");
                            tbl.appendChild(tblBody);
                            var trow;
                            $.each(response.logAttivita_data, function(i, star) {
                                trow = document.createElement("tr");
                                tblBody.appendChild(trow);
                                var valori=star.toString().split(',');
                                var data=new Date(valori[0]);
                                var datimese=data.getMonth() + 1;
                                var mesefix;
                                if(datimese<10){mesefix='0'+datimese} else{mesefix=''+datimese}
                                var commenti=valori[1];
                                var datareplace=commenti.toString().replace(/;/g,",");
                                createCell(tbl.rows[i+1].insertCell(tbl.rows[i+1].cells.length),(data.getDate() + '/' +mesefix+ '/' + data.getFullYear()) , 'col');
                                createCell(tbl.rows[i+1].insertCell(tbl.rows[i+1].cells.length),datareplace , 'col');

                            });
                        }
                        $("#idPolizza").val(idPolizza);
                        $("#avanti1").prop('disabled', true);
                        $("#linkModulo").addClass("not-active");
                        removeParameters();
                        $("#bottonEmmette").prop('disabled', true);
                        $(".nInteressato").show();
                        //$(".chiusura").hide();
                        $("#modalPolizza").modal({
                            keyboard: true
                        });
                    }
                }
            }, "json");
        }
    });
    $("#garante").on("change", function(event) {
        if($("#garante").is(":checked")){
            var idPolizza=$("#idPolizza").val();
            $( ".alert" ).remove();
            $("#cognomeInt").val("").prop("readonly",false);
            $("#nomeInt").val("").prop("readonly",false);
            $("#tipoClienteInt").prop("disabled",false);
            $("#partitaIvaInt").val("").prop("readonly",false);
            $("#cellulare").val("");
            $("#emailInt").val("");
            $("#localitaInt").val("").prop("readonly",false);
            $("#provinciaInt").val("").prop("disabled",false);
            $("#capInt").val("").prop("readonly",false);
            $("#indirizzoInt").val("").prop("readonly",false);
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            $("#modalPolizza").modal({
                keyboard: true
            });
        }
    });
    $("#nomeInt").on("change",function(event){
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }

    });
    $("#cognomeInt").on("change",function(event){
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }

    });
    $("#provinciaInt").on("change",function(event){
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }

    });
    $("#indirizzoInt").on("change",function(event){
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }

    });
    $("#localitaInt").on("change",function(event){
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }

    });
    $("#capInt").on("change",function(event){
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }

    });
    $("#partitaIva").on("change",function(event){
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
                $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }
    });
    $("#decorrenzaAnti").on("change", function(event){
        if($("#decorrenzaAnti").is(":checked")){
            $("#dataPosticipata").val("");
                if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaEmailCliente() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaCV() && verificaValore() && verificaImmediata() ){
                    removeParameters();
                    $("#avanti1").prop('disabled', false);
                    var nome= $("#nomeInt").val();
                    var cognome= $("#cognomeInt").val();
                    var targa= $("#targa").val();
                    var nopratica= $("#noPratica").val();
                    var id= $("#idPolizza").val();
                    setGetParameter("id",id);
                    setGetParameter("nome",nome);
                    setGetParameter("cognome",cognome);
                    setGetParameter("targa",targa);
                    setGetParameter("nopratica",nopratica);
                    $("#linkModulo").removeClass("not-active");
                    <sec:ifHasRole role="ADMINPOSTICIPATI">
                    $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
                    </sec:ifHasRole>
                }else{
                    $("#avanti1").prop('disabled', true);
                    $("#linkModulo").addClass("not-active");
                    removeParameters();
                    <sec:ifHasRole role="ADMINPOSTICIPATI">
                    $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
                    </sec:ifHasRole>
                }
        }
    });
    $("#decorrenzaPost").on("change", function(event){
        if($("#decorrenzaPost").is(":checked")){
            $("#dataDecorrenza").val("");
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaEmailCliente() && verificaTarga()  && verificaDataImmatricolazione()  && verificaTelaio()==true && verificaMarchio()==true && verificaVersione()&& verificaCV() && verificaValore() && verificaPosticipata() ){
                removeParameters();
                $("#avanti1").prop('disabled', false);
                $("#linkModulo").removeClass("not-active");
            }else{
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
            }
        }
    });

    $("#cellulare").on("change", function(event) {
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
                removeParameters();
                $("#avanti1").prop('disabled', false);
                var nome= $("#nomeInt").val();
                var cognome= $("#cognomeInt").val();
                var targa= $("#targa").val();
                var nopratica= $("#noPratica").val();
                var id= $("#idPolizza").val();
                setGetParameter("id",id);
                setGetParameter("nome",nome);
                setGetParameter("cognome",cognome);
                setGetParameter("targa",targa);
                setGetParameter("nopratica",nopratica);
                $("#linkModulo").removeClass("not-active");
                <sec:ifHasRole role="ADMINPOSTICIPATI">
                $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
                </sec:ifHasRole>
            }else{
                if(!verificaCapCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtcapCliente);
                }
                if(!verificaProvinciaCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtprovinciaCliente);
                }
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                removeParameters();
                <sec:ifHasRole role="ADMINPOSTICIPATI">
                $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
                </sec:ifHasRole>
            }
    });
    $("#emailInt").on("change", function(event) {
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() )&& verificaEmailCliente() ){
                removeParameters();
                $("#avanti1").prop('disabled', false);
                var nome= $("#nomeInt").val();
                var cognome= $("#cognomeInt").val();
                var targa= $("#targa").val();
                var nopratica= $("#noPratica").val();
                var id= $("#idPolizza").val();
                setGetParameter("id",id);
                setGetParameter("nome",nome);
                setGetParameter("cognome",cognome);
                setGetParameter("targa",targa);
                setGetParameter("nopratica",nopratica);
                $("#linkModulo").removeClass("not-active");
                <sec:ifHasRole role="ADMINPOSTICIPATI">
                $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
                </sec:ifHasRole>
            }else{
                if(!verificaEmailCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtemailCliente);
                }
                if(!verificaCapCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtcapCliente);
                }
                if(!verificaProvinciaCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtprovinciaCliente);
                }
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                removeParameters();
                <sec:ifHasRole role="ADMINPOSTICIPATI">
                $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
                </sec:ifHasRole>
            }

    });

    $("#targa").on("change", function(event) {
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaEmailCliente() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata())){
                removeParameters();
                $("#avanti1").prop('disabled', false);
                var nome= $("#nomeInt").val();
                var cognome= $("#cognomeInt").val();
                var targa= $("#targa").val();
                var nopratica= $("#noPratica").val();
                var id= $("#idPolizza").val();
                setGetParameter("id",id);
                setGetParameter("nome",nome);
                setGetParameter("cognome",cognome);
                setGetParameter("targa",targa);
                setGetParameter("nopratica",nopratica);
                $("#linkModulo").removeClass("not-active");
                <sec:ifHasRole role="ADMINPOSTICIPATI">
                $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
                </sec:ifHasRole>
            }else{
                if(!verificaTarga()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txttargaCliente);
                }
                if(!verificaCapCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtcapCliente);
                }
                if(!verificaProvinciaCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtprovinciaCliente);
                }
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                removeParameters();
                <sec:ifHasRole role="ADMINPOSTICIPATI">
                $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
                </sec:ifHasRole>
            }
    });
    $("#telaio").on("change", function(event) {
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTelaio() && verificaDataImmatricolazione() && verificaTarga() && verificaMarchio() && verificaVersione() && verificaCV() && verificaValore() && (verificaImmediata() || verificaPosticipata())){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            if(!verificaCapCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtcapCliente);
            }
            if(!verificaProvinciaCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtprovinciaCliente);
            }
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }
    });
    $("#marchio").on("change", function(event) {
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaMarchio() && verificaDataImmatricolazione() && verificaTarga() && verificaTelaio()  && verificaVersione() && verificaCV()  && verificaValore() && (verificaImmediata() || verificaPosticipata()) ){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            if(!verificaCapCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtcapCliente);
            }
            if(!verificaProvinciaCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtprovinciaCliente);
            }
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }
    });
    $("#versione").on("change", function(event) {
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaVersione() && verificaDataImmatricolazione() && verificaTarga() && verificaTelaio() && verificaMarchio() && verificaCV()  && verificaValore() && (verificaImmediata() || verificaPosticipata())){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            if(!verificaCapCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtcapCliente);
            }
            if(!verificaProvinciaCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtprovinciaCliente);
            }
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }
    });
    $("#cv").on("change", function(event) {
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaVersione() && verificaDataImmatricolazione() && verificaTarga() && verificaTelaio() && verificaMarchio() && verificaCV()  && verificaValore() && (verificaImmediata() || verificaPosticipata())){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            if(!verificaCapCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtcapCliente);
            }
            if(!verificaProvinciaCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtprovinciaCliente);
            }
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }
    });
    $("#valoreAssicurato").on("change", function(event) {
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTarga() && verificaValore() && verificaDataImmatricolazione() &&  verificaTelaio() && verificaMarchio() && verificaCV() && verificaVersione() && (verificaImmediata()|| verificaPosticipata())){
            removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            setGetParameter("id",id);
            setGetParameter("nome",nome);
            setGetParameter("cognome",cognome);
            setGetParameter("targa",targa);
            setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
            </sec:ifHasRole>
        }else{
            if(!verificaCapCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtcapCliente);
            }
            if(!verificaProvinciaCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtprovinciaCliente);
            }
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }
    });
    $("#dataImmatricolazione").on("changeDate", function(event) {
        var data1 = $("#dataImmatricolazione").val().split('-');
        if(data1 != ''){
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTelaio() && verificaTarga() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata()|| verificaPosticipata()) ){
                removeParameters();
                $("#avanti1").prop('disabled', false);
                var nome= $("#nomeInt").val();
                var cognome= $("#cognomeInt").val();
                var targa= $("#targa").val();
                var nopratica= $("#noPratica").val();
                var id= $("#idPolizza").val();
                setGetParameter("id",id);
                setGetParameter("nome",nome);
                setGetParameter("cognome",cognome);
                setGetParameter("targa",targa);
                setGetParameter("nopratica",nopratica);
                $("#linkModulo").removeClass("not-active");
                <sec:ifHasRole role="ADMINPOSTICIPATI">
                $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
                </sec:ifHasRole>
            }else{
                if(!verificaCapCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtcapCliente);
                }
                if(!verificaProvinciaCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtprovinciaCliente);
                }
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                removeParameters();
                <sec:ifHasRole role="ADMINPOSTICIPATI">
                $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
                </sec:ifHasRole>
            }
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }
    });
    $("#dataDecorrenza").on("changeDate", function(event) {
        var data1 = $("#dataDecorrenza").val();
        if(data1 != ''){
            $("#dataPosticipata").val("");
            removeParameters();
            $("#decorrenzaAnti").prop("checked", true);
                if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTelaio() && verificaTarga() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && verificaDataImmatricolazione() ){
                    removeParameters();
                    $("#avanti1").prop('disabled', false);
                    var nome= $("#nomeInt").val();
                    var cognome= $("#cognomeInt").val();
                    var targa= $("#targa").val();
                    var nopratica= $("#noPratica").val();
                    var id= $("#idPolizza").val();
                    setGetParameter("id",id);
                    setGetParameter("nome",nome);
                    setGetParameter("cognome",cognome);
                    setGetParameter("targa",targa);
                    setGetParameter("nopratica",nopratica);
                    $("#linkModulo").removeClass("not-active");
                    <sec:ifHasRole role="ADMINPOSTICIPATI">
                    $("#bottonEmmette").prop('disabled', false).removeClass("btnDisabled");
                    </sec:ifHasRole>
                }
                else{
                    if(!verificaCapCliente()){
                        $( ".alert " ).remove();
                        $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                        $( ".testoAlert").append(txtcapCliente);
                    }
                    if(!verificaProvinciaCliente()){
                        $( ".alert " ).remove();
                        $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                        $( ".testoAlert").append(txtprovinciaCliente);
                    }
                    $("#avanti1").prop('disabled', true);
                    $("#linkModulo").addClass("not-active");
                    removeParameters();
                    <sec:ifHasRole role="ADMINPOSTICIPATI">
                    $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
                    </sec:ifHasRole>
                }
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
            <sec:ifHasRole role="ADMINPOSTICIPATI">
            $("#bottonEmmette").prop('disabled', true).addClass("btnDisabled");
            </sec:ifHasRole>
        }
    });
    $("#dataPosticipata").on("changeDate", function(event) {
        var data1 = $("#dataPosticipata").val();
        if(data1 != ''){
            $("#dataDecorrenza").val("");
            removeParameters();
            $("#decorrenzaPost").prop("checked", true);
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTelaio() && verificaTarga() && verificaDataImmatricolazione() && verificaMarchio() && verificaCV() && verificaVersione() && verificaValore() && (verificaCellulare() ||verificaEmailCliente())){
                removeParameters();
                $("#avanti1").prop('disabled', false);
                var nome= $("#nomeInt").val();
                var cognome= $("#cognomeInt").val();
                var targa= $("#targa").val();
                var nopratica= $("#noPratica").val();
                var id= $("#idPolizza").val();
                setGetParameter("id",id);
                setGetParameter("nome",nome);
                setGetParameter("cognome",cognome);
                setGetParameter("targa",targa);
                setGetParameter("nopratica",nopratica);
                $("#linkModulo").removeClass("not-active");
            }
            else {
                if(!verificaCapCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtcapCliente);
                }
                if(!verificaProvinciaCliente()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txtprovinciaCliente);
                }
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                removeParameters();
            }
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            removeParameters();
        }
    });
    $("#email").on("change", function(event) {
        if(verificaEmailDealer() ){
            $("#bottonSalva").prop('disabled', false);
        }else{
            if(!verificaEmailDealer()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtemailDealer);
                $("#bottonSalva").prop('disabled', true);
            }

        }

    });
    $("#avanti1").on ("click", function(event){

        <sec:ifHasNotRole role="ADMINPOSTICIPATI">
        if(verificaPosticipata()){
            href="#sectionC";
            attivaTab('sectionC');
        }else if(verificaImmediata()){
            href="#sectionB";
            attivaTab('sectionB');
        }
        </sec:ifHasNotRole>

            verificapartitaIvaCliente();
            verificaTarga();
            verificaTelaio();
            verificaMarchio();
            verificaVersione();
            verificaDataImmatricolazione();
            verificaValore();
            verificaImmediata();
            verificaPosticipata();
            verificaCognomeCliente();
            verificaNomeCliente();
            verificaCapCliente();
            verificaProvinciaCliente();
            verificaIndirizzoCliente();
            verificaSessoCliente();
            verificaLocalitaCliente();

        if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaProvinciaCliente() || !verificaIndirizzoCliente() || !verificaSessoCliente() || !verificaLocalitaCliente() || !verificapartitaIvaCliente() || !verificaTarga() || !verificaTelaio() || !verificaMarchio() || !verificaVersione() || !verificaDataImmatricolazione()  || !verificaValore() || (!verificaImmediata() || !verificaPosticipata()) || !verificaEmailDealer()){
            $( ".alert" ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
            if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
            if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
            if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
            if(!verificaVersione()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
            if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
            if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
            if(!verificaImmediata() && !verificaPosticipata()) $( ".testoAlert").append(txtpostCliente).append("<br>");
            if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
            if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
            if(!verificaCapCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
            if(!verificaProvinciaCliente()) $( ".testoAlert").append(txtprovinciaCliente).append("<br>");
            if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
            if(!verificaSessoCliente()) $( ".testoAlert").append(txtsessoCliente).append("<br>");
            if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
            if(!verificaEmailDealer()) $( ".testoAlert").append(txtemailDealer).append("<br>");

        }
        if(verificaEmailDealer()){
            $("#bottonSalva").prop('disabled', false);
        }
    });
    $("#indietro1").on ("click", function(event){

        <sec:ifHasNotRole role="ADMINPOSTICIPATI">
        if(verificaPosticipata()){
            href="#sectionA";
            attivaTab('sectionA');
        }else if(verificaImmediata()){
            href="#sectionB";
            attivaTab('sectionB');
        }
        </sec:ifHasNotRole>

    });
    $("#avanti2").on ("click", function(event){
        <sec:ifHasRole role="ADMINPOSTICIPATI">
        if(verificaNuovaPolizza() ){
            $( ".alert " ).remove();
        }else {
            if(verificatargaAcquisizione() && verificaFileAcquisizione()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span>compilare i dati della targa e il documento</span></div>" );
            }

        }
        </sec:ifHasRole>

    });
    $("#invioRichiesta").on ("click", function(event){

    });
    $("#acquisizioneNuovaPolizza").on("change", function(event){
        if($("#acquisizioneNuovaPolizza").is(":checked")){
            $( ".alert " ).remove();
            $("#targaAcquisizione").val("").prop('disabled', true);
            if($('#input-idAcquisizione').val()!=''){
                $('#input-idAcquisizione').fileinput('clear').fileinput('disable');
            }
        }
    });
    $("#acquisizioneBersani").on("change", function(event){
        if($("#acquisizioneBersani").is(":checked")){
            $( ".alert " ).remove();
            $('#input-idAcquisizione').fileinput('enable');
            $("#targaAcquisizione").prop('disabled', false);
        }
    });
    $("#acquisizionePassaggio").on("change", function(event){
        if($("#acquisizionePassaggio").is(":checked")){
            $( ".alert " ).remove();
            $('#input-idAcquisizione').fileinput('enable');
            $("#targaAcquisizione").prop('disabled', false);
            if(verificatargaAcquisizione()==true && verificaFileAcquisizione()==true ){

            }else {

            }
        }
    });
    </script>
    </body>
    </html>