<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>Nuova polizza</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="col-md-12">
    <div class="col-md-6"><link:list page="1" controller="polizze" attrs="[class: 'btn btn-annulla pull-left']">Torna elenco polizze</link:list></div>
    <div class="col-md-6 col-md-push-5"><asset:image src="/loghi/mansutti.png" height="30px" style="margin-bottom: 25px;" /></div>

</div>
<div class="row">
    <div class="col-md-12 col-xs-10">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto " > ${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.message =="RICHIESTA_INVIATA"}">
            <script type="text/javascript">swal({
                title: "RICHIESTA INVIATA!",
                type: "success",
                timer: "1800",
                showConfirmButton: false
                });
            </script>
        </g:if>
        <g:elseif test="${flash.message=="POSTICIPATA"}" >
            <script type="text/javascript">swal({
                title: "Polizza posticipata!",
                type: "warning",
                timer: "1800",
                showConfirmButton: false
                 });
            </script>
        </g:elseif>
        <f:form  id="polizzaForm"  method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="">
                <div class="datiPoli"></div>
                <div class="bs-example">
                    <div id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a  href="#sectionA" class="legendaTabs" data-toggle="tab"><span class="fa fa-user"></span> DATI DEL CLIENTE</a></li>
                            <li><a  href="#sectionB" class="legendaTabs" data-toggle="tab"><span class="fa fa-files-o"></span> ACQ. POSIZIONE ASSICURATIVA</a></li>
                            <li><a  href="#sectionC" class="legendaTabs" data-toggle="tab"><span class="fa fa-users"></span> DATI DEL CONCESSIONARIO</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div id="sectionA" class="tab-pane fade in active">
                            <div class="col-xs-6 col-md-4 marginesopra">
                                <label class="testoParagraph control-label" >NR.PRATICA</label>
                                <input class="form-control testoParagraph" type="text" name="noPratica"  id="noPratica" value="" required />
                            </div>
                            <div class="col-xs-6 col-md-4 marginesopra">
                                <label class="testoParagraph control-label ">COGNOME</label>
                                <input class="form-control testoParagraph" type="text" name="cognomeInt" id="cognomeInt" value="" />
                            </div>
                            <div class="col-xs-6 col-md-4 marginesopra">
                                <label class="testoParagraph control-label ">NOME</label>
                                <input class="form-control testoParagraph" type="text" name="nomeInt" id="nomeInt" value="" />
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <label class="testoParagraph control-label">C.FISCALE / P.IVA</label>
                                <input class="form-control testoParagraph" type="text" name="partitaIvaInt"  id="partitaIvaInt" value="" />
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <label class="testoParagraph control-label">CELLULARE</label>
                                <input class="form-control testoParagraph" type="text" name="cellulare"  id="cellulare" value=""/>
                            </div>
                            <div class="col-xs-6 col-md-2" >
                                <label class="testoParagraph control-label">SESSO</label>
                                <select class="form-control testoParagraph" name="tipoClienteInt" id="tipoClienteInt">
                                    <option value="F" selected>F</option>
                                    <option value="M" >M</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">PROVINCIA</label>
                                <select class="form-control testoParagraph" name="provinciaInt" id="provinciaInt" >
                                    <g:each var="provincia" in="${province}">
                                        <option>${provincia}</option>
                                    </g:each>
                                </select>
                            </div>
                            <div class="col-xs-6 col-md-5">
                                <label class="testoParagraph control-label">INDIRIZZO</label>
                                <input  type="text" name="indirizzoInt" class="form-control testoParagraph"  id="indirizzoInt"  value="" />
                            </div>
                            <div class="col-xs-6 col-md-5">
                                <label class="testoParagraph control-label">LOCALITA'</label>
                                <input type="text" name="localitaInt" class="form-control testoParagraph" id="localitaInt"  value="" />
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">CAP</label>
                                <input type="text" name="capInt" class="form-control testoParagraph" id="capInt"  value="" />
                            </div>
                            <div class="col-xs-6 col-md-4 marginesotto">
                                <label class="testoParagraph control-label">EMAIL</label>
                                <input class="form-control testoParagraph" type="text" name="emailInt"  id="emailInt" value="" />
                            </div>
                            <div class="row"></div>
                            <legend class="legenda"><span class="fa fa-motorcycle"></span> DATI DEL MOTOVEICOLO</legend>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph targaLabel control-label">TARGA</label>
                                <f:input control-class="form"  type="text" name="targa" class="maiuscola testoParagraph"  id="targa" value="" />
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label">TELAIO</label>
                                <f:input control-class="form" type="text" name="telaio" class="maiuscola testoParagraph"  id="telaio" value="" />
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label">MARCHIO VETTURA</label>
                                <f:input control-class="form" type="text" name="marchio"  class="maiuscola testoParagraph" id="marchio" value="" />
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label">DESC. VERSIONE</label>
                                <f:input control-class="form" type="text" name="versione"  id="versione" class="maiuscola testoParagraph" value="" />
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label" style="margin-top: 20px;">DATA IMMATR.</label>
                                <f:input control-class="form" type="text" name="dataImmatricolazione"  id="dataImmatricolazione" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true" data-date-end-date="0d"
                                         data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph"
                                         data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" required="true"/>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label" style="margin-top: 20px;">CV</label>
                                <f:input control-class="form" type="text" name="cv"  class="maiuscola testoParagraph" id="cv" value="" />
                            </div>
                            <div class="col-xs-6 col-md-3" style="margin-bottom: 20px; margin-top: 20px;">
                                <label class="testoParagraph control-label">VALORE FATTURA (&euro;)</label>
                                <f:input control-class="form" type="number" name="valoreAssicurato"  required="true" id="valoreAssicurato" value="" />
                            </div>
                            <div class="col-xs-6 col-md-1" style="margin-top: 20px;">
                                <label class="testoParagraph control-label">CATEGORIA</label>
                                <f:checkbox  class="centered " name="motoveicolo"  value="" id="motoveicolo" text="MOTOVEICOLO" checked="true"  disabled="true"/>
                            </div>
                            <div class="row"></div>
                            <legend class="legenda"><span class="fa fa-calendar"></span> DECORRENZA POLIZZA <i>(non anteriore alla data di richiesta)</i></legend>
                            <div class="col-xs-12 col-md-12">
                                <div class="col-xs-6 col-md-3">
                                    <f:radiobutton  class="centered" name="decorrenza"  value="" id="decorrenzaAnti" checked="true" text="IMMEDIATA"/>
                                    <p>Si richiede la copertura con decorrenza del</p>
                                </div>
                                <div class="col-xs-6 col-md-3" style="margin-bottom: 10px; margin-top: 10px;">
                                    <f:input control-class="form" type="text" name="dataDecorrenza"  id="dataDecorrenza" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"  data-date-start-date="+1d"
                                             data-date-end-date="+1m" data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                             data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" required="false"/>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <f:radiobutton  class="centered " name="decorrenza"  value="" id="decorrenzaPost"  text="POSTICIPATA" />
                                    <p>La polizza attualmente in uso scadrà il</p>
                                </div>
                                <div class="col-xs-3 col-md-3" style="margin-bottom: 20px; margin-top: 10px; ">
                                    <f:input control-class="form" type="text" name="dataPosticipata"  id="dataPosticipata" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true" data-date-start-date="1d"
                                             data-date-end-date="+1y" data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                             data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" required="false" />
                                </div>
                            </div>
                            <div class="logAttivita"></div>
                            <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                <div class="col-xs-6 col-md-6">

                                    %{--<a href="${createLink(controller: "polizze", action: "compilaModulo")}" target="_blank" class="btn  btn-small not-active"  name="linkModulo" id="linkModulo" title="scaricare modulo di adesione"><b>modulo di adesione</b> <i class="fa fa-download"></i></a>--}%
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <button href="" data-toggle="tab" id="avanti1" name="avanti1" class="btn  pull-right disabilita" %{--onclick="attivaTab('sectionB')"--}% disabled>Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>
                                </div>
                            </div>
                        </div>
                        <div id="sectionB" class="tab-pane fade">
                            <div class="col-xs-12 col-md-12">
                                <div class="col-xs-6 col-md-12">
                                    <div class="col-xs-6 col-md-10" style="margin-top:20px;">
                                        <label>Richiedi al Cliente la documentazione per verificare la posizione assicurativa:</label>
                                        <button type="button" id="invioRichiesta" name="invioRichiesta" onclick="return inviaMailRichiesta();" class="btn btn-primary">Invio richiesta</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                <div class="col-xs-6 col-md-6">
                                    <a href="#sectionA" data-toggle="tab" id="indietro" class="btn pull-left button-ladda" onclick="attivaTab('sectionA')"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <button href="#sectionC" data-toggle="tab" id="avanti2" name="avanti2" class="btn  pull-right" onclick="attivaTab('sectionC')" disabled>Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>
                                </div>
                            </div>
                        </div>
                        <div id="sectionC" class="tab-pane fade">
                            <div class="col-xs-6 col-md-6" style="margin-top: 9px;">
                                <label class="testoParagraph control-label">RAGIONE SOCIALE</label>
                                <select class="form-control testoParagraph" name="dealer" id="dealer" onchange="loadDati.call(this, event);" >
                                    <option value="" selected="selected">SELEZIONA DEALER...</option>
                                    <g:each var="dealer" in="${dealers}">
                                        <option value="${dealer.id}">${dealer.ragioneSociale} -- ${dealer.indirizzo}</option>
                                    </g:each>
                                </select>
                            </div>
                            <div class="col-xs-6 col-md-6" style="margin-top: 9px;">
                                <label class="testoParagraph control-label">INDIRIZZO</label>
                                <f:input control-class="form" type="text" name="indirizzo" class="maiuscola testoParagraph"  id="indirizzo" readonly="true" value="" />
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label">PARTITA IVA</label>
                                <f:input control-class="form" type="text" name="piva" class="maiuscola testoParagraph"  id="piva" readonly="true" value="" />
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label">CAP</label>
                                <f:input control-class="form" type="text" name="cap" class="maiuscola testoParagraph" id="cap" readonly="true" value="" />
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <label class="testoParagraph control-label">COMUNE</label>
                                <f:input control-class="form" type="text" name="localita" class="maiuscola testoParagraph" id="localita" readonly="true" value="" />
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <label class="testoParagraph control-label">MAIL</label>
                                <f:input control-class="form" type="text" name="email"  id="email" class="testoParagraph" value="" />
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">PROV.</label>
                                <f:input control-class="form" type="text" name="provincia" class="maiuscola testoParagraph" id="provincia" readonly="true" value="" />
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <label class="testoParagraph control-label">TELEFONO</label>
                                <f:input control-class="form" type="text" name="telefono" class="maiuscola testoParagraph" id="telefono"  value="" />
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label">FAX</label>
                                <f:input control-class="form" type="text" name="fax"  id="fax" class="testoParagraph"  value="" />
                            </div>
                            <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                <div class="col-xs-6 col-md-4">
                                    <a href="#sectionB" data-toggle="tab" id="indietro1" class="btn pull-left" %{--onclick="attivaTab('sectionB')"--}%><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>
                                </div>

                                <div class="col-xs-6 col-md-4 col-md-push-4">
                                    <button type="submit" id="bottonSalva" value="Salva" class="btn pull-right" disabled>Salva</button>
                                    %{--<g:actionSubmit class="btn pull-right" value="Salva" action="aggiornaPolizza" />--}%
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </f:form>
    </div>
</div>
<script type="text/javascript">
    function attivaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };
    var txtnoPratica="";
    var txtpartitaIvaCliente="";
    var txtNomeCliente="";
    var txtCognomeCliente="";
    var txtIndirizzoCliente="";
    var txtLocalitaCliente="";
    var txtcapCliente="";
    var txtprovinciaCliente="";
    var txtsessoCliente="";
    var txtemailCliente="";
    var txtemailDealer="";
    var txtcellCliente="";
    var txttargaCliente="";
    var txttelaioCliente="";
    var txtmarchioCliente="";
    var txtversioneCliente="";
    var txtdataImmCliente="";
    var txtcvCliente="";
    var txtvaloreCliente="";
    var txtimmCliente="";
    var txtpostCliente="";
    var txtposizionePolizza="";
    var txttargaAcquisizione="";


    function loadDati(event){
        var idDealer=this.options[this.selectedIndex].value;
        $.post("${createLink(controller: "polizze", action: 'caricaDealer')}",{ idDealer: idDealer, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $("#piva").val(response.piva);
                $("#indirizzo").val(response.indirizzo);
                $("#localita").val(response.localita);
                $("#cap").val(response.cap);
                $("#provincia").val(response.provincia);
                $("#email").val(response.email);
                $("#telefono").val(response.telefono);
                $("#fax").val(response.fax);
            }else{
                $("#piva").val("");
                $("#indirizzo").val("");
                $("#localita").val("");
                $("#cap").val("");
                $("#provincia").val("");
                $("#email").val("");
                $("#telefono").val("");
                $("#fax").val("");
            }
        }, "json");
        $("#bottonSalva").prop('disabled', true);
    };
    function verificanoPratica(){
        var valore= $("#noPratica").val();
        if(valore!=''){ return true;}
        else{
            txtnoPratica="inserire il numero di pratica";
            return false;
        }
    }
    function verificapartitaIvaCliente(){
        var valore= $("#partitaIvaInt").val();
        if(valore!=''){ return true;}
        else{
            txtpartitaIvaCliente="inserire la partita iva";
            return false;
        }
    }
    function verificaNomeCliente(){
        var valore= $("#nomeInt").val();
        if(valore!=''){ return true;}
        else{
            txtNomeCliente="inserire il nome";
            return false;
        }
    }
    function verificaCognomeCliente(){
        var valore= $("#cognomeInt").val();
        if(valore!=''){ return true;}
        else{
            txtCognomeCliente="inserire il cognome";
            return false;
        }
    }
    function verificaIndirizzoCliente(){
        var valore= $("#indirizzoInt").val();
        if(valore!=''){ return true;}
        else{
            txtIndirizzoCliente="inserire il indirizzo";
            return false;
        }
    }
    function verificaLocalitaCliente(){
        var valore= $("#localitaInt").val();
        if(valore!=''){ return true;}
        else{
            txtLocalitaCliente="inserire la localita'";
            return false;
        }
    }
    function verificaSessoCliente(){
        var valore= $("#tipoClienteInt").val();
        if(valore!=''){ return true;}
        else{
            txtsessoCliente="inserire il sesso";
            return false;
        }
    }
    function verificaProvinciaCliente(){
        var valore= $("#provinciaInt").val();
        if(valore!=''){ return true;}
        else{
            txtprovinciaCliente="inserire la provincia";
            return false;
        }
    }
    function verificaCapCliente(){
        var valore= $("#capInt").val();
        if(valore!=''){ return true;}
        else{
            txtcapCliente="inserire il cap";
            return false;
        }
    }
    function verificaEmailCliente() {
        var valore= $("#emailInt").val();
        var regemailDef=/^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;
        if(valore!='' && regemailDef.test(valore)){
            $( ".alert" ).remove();
            return true;
        }
        else{
            if(!regemailDef.test(valore) && valore!=''){ txtemailCliente="verificare il formato del campo email";}
            else {txtemailCliente="compilare il campo email";}
            return false;
        }
    }
    function verificaEmailDealer() {
        var valore= $("#email").val();
        var regemailDef=/^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;
        if(valore!='' && regemailDef.test(valore)){
            $( ".alert" ).remove();
            return true;
        }
        else{
            if(!regemailDef.test(valore) && valore!=''){ txtemailDealer="verificare il formato del campo email";}
            else {txtemailDealer="compilare il campo email del dealer";}
            return false;
        }
    }
    function verificaTarga() {
        var valore= $.trim($("#targa").val());
        var regtarga=/^[a-zA-Z]{2}[0-9]{5}$/;
        if(valore!='' && regtarga.test(valore)){
            return true;
        }
        else {
            if(!regtarga.test(valore) && valore!=''){txttargaCliente="il formato della targa è sbagliato";}
            else {txttargaCliente="compilare il campo targa";}
            return false;
        }
    }
    function verificaDataImmatricolazione(){
        var data1 = $("#dataImmatricolazione").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataImmCliente="compilare la data Immatricolazione";
            return false;
        }
    }
    function verificaTelaio() {
        var valore= $.trim($("#telaio").val());
        if (valore!=''){
            return true;
        }
        else {
            txttelaioCliente="compilare il telaio";
            return false;
        }
    }
    function verificaMarchio() {
        var valore= $.trim($("#marchio").val());
        if (valore!=''){ return true; }
        else {
            txtmarchioCliente="compilare il marchio vettura";
            return false;
        }
    }
    function verificaCV() {
        var valore= $.trim($("#cv").val());
        if (valore!=''){ return true; }
        else {
            txtmarchioCliente="compilare il cv";
            return false;
        }
    }
    function verificaVersione() {
        var valore= $.trim($("#versione").val());
        if (valore!=''){ return true; }
        else {
            txtversioneCliente="compilare la descrizione versione";
            return false;
        }
    }
    function verificaValore() {
        var valore= $.trim($("#valoreAssicurato").val());
        if (valore!=''){ return true; }
        else {
            txtvaloreCliente="compilare il valore assicurato";
            return false;
        }
    }
    function verificaPosticipata() {
        var valore= $("#dataPosticipata").val();
        if (valore!=''){ return true; }
        else {
            txtpostCliente="compilare la data di decorrenza polizza";
            return false;
        }
    }
    function verificaImmediata() {
        var valore= $("#dataDecorrenza").val();
        if (valore!=''){ return true; }
        else {
            txtpostCliente="compilare la data di decorrenza polizza";
            return false;
        }
    }
    function verificaCellulare() {
        var valore= $.trim($("#cellulare").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaIndirizzoDealer() {
        var valore= $.trim($("#indirizzo").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaPIvaDealer() {
        var valore= $.trim($("#piva").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaComuneDealer() {
        var valore= $.trim($("#localita").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaCapDealer() {
        var valore= $.trim($("#cap").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaProvDealer() {
        var valore= $.trim($("#provincia").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function inviaMailRichiesta(){
        var noPratica=$("#noPratica").val();
        var emailCliente=$("#emailInt").val();
        var nomeCliente=$("#nomeInt").val();
        var cognomeCliente=$("#cognomeInt").val();
        $.post("${createLink(controller: "polizze", action: 'inviaRichiestaAdmin')}",{ noPratica: noPratica,nomeCliente:nomeCliente,cognomeCliente:cognomeCliente,emailCliente:emailCliente, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\">Mail inviata al cliente <span class='testoAlert'></span></div>" );
                $("#avanti2").prop('disabled', false);
                //$("#invioRichiesta").prop('disabled', true);
            }else{
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\">Non e' stato possibile inviare la mail al cliente <span class='testoAlert'></span></div>" );
                $("#avanti2").prop('disabled', false);
                //$("#invioRichiesta").prop('disabled', true);
            }
        }, "json");
    }
    function setGetParameter(paramName, paramValue)
    {
        var url = $('#linkModulo').attr("href");
        var hash = location.hash;
        url = url.replace(hash, '');

        if (url.indexOf("?") < 0)
            url += "?" + paramName + "=" + paramValue;
        else
            url += "&" + paramName + "=" + paramValue;
        //}
        $('#linkModulo').attr("href",url);
    }
    function removeParameters()
    {
        var url = $('#linkModulo').attr("href");

        if (url.indexOf("?") < 0){     }
        else{
            var subfix=url.indexOf("?");
            url=url.substring(0,subfix);
        }
        $('#linkModulo').attr("href",url);
    }
    $("#nomeInt").on("change",function(event){
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }

    });
    $("#cognomeInt").on("change",function(event){
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");

        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }

    });
    $("#provinciaInt").on("change",function(event){
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }

    });
    $("#indirizzoInt").on("change",function(event){
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }

    });
    $("#localitaInt").on("change",function(event){
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }

    });
    $("#capInt").on("change",function(event){
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();

        }

    });
    $("#partitaIva").on("change",function(event){
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            var id= $("#idPolizza").val();
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#decorrenzaAnti").on("change", function(event){
        if($("#decorrenzaAnti").is(":checked")){
            $("#dataPosticipata").val("");
            if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaEmailCliente() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaCV() && verificaValore() && verificaImmediata() ){
                //removeParameters();
                $("#avanti1").prop('disabled', false);
                var nome= $("#nomeInt").val();
                var cognome= $("#cognomeInt").val();
                var targa= $("#targa").val();
                var nopratica= $("#noPratica").val();
                //setGetParameter("nome",nome);
                //setGetParameter("cognome",cognome);
                //setGetParameter("targa",targa);
                //setGetParameter("nopratica",nopratica);
                $("#linkModulo").removeClass("not-active");
            }else{
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                //removeParameters();
            }
        }
    });
    $("#decorrenzaPost").on("change", function(event){
        if($("#decorrenzaPost").is(":checked")){
            $("#dataDecorrenza").val("");
            if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaEmailCliente() && verificaTarga()  && verificaDataImmatricolazione()  && verificaTelaio()==true && verificaMarchio()==true && verificaVersione()&& verificaCV() && verificaValore() && verificaPosticipata() ){
                //removeParameters();
                $("#avanti1").prop('disabled', false);
                $("#linkModulo").removeClass("not-active");
            }else{
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
            }
        }
    });

    $("#cellulare").on("change", function(event) {
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() ) && verificaEmailCliente() ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#emailInt").on("change", function(event) {
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata() )&& verificaEmailCliente() ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            if(!verificaEmailCliente()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtemailCliente);
            }
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }

    });

    $("#targa").on("change", function(event) {
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaCellulare() && verificaEmailCliente() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata() || verificaPosticipata())){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            if(!verificaTarga()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
            }
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#telaio").on("change", function(event) {
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTelaio() && verificaDataImmatricolazione() && verificaTarga() && verificaMarchio() && verificaVersione() && verificaCV() && verificaValore() && (verificaImmediata() || verificaPosticipata())){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#marchio").on("change", function(event) {
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaMarchio() && verificaDataImmatricolazione() && verificaTarga() && verificaTelaio()  && verificaVersione() && verificaCV()  && verificaValore() && (verificaImmediata() || verificaPosticipata()) ){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#versione").on("change", function(event) {
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaVersione() && verificaDataImmatricolazione() && verificaTarga() && verificaTelaio() && verificaMarchio() && verificaCV()  && verificaValore() && (verificaImmediata() || verificaPosticipata())){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#cv").on("change", function(event) {
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaVersione() && verificaDataImmatricolazione() && verificaTarga() && verificaTelaio() && verificaMarchio() && verificaCV()  && verificaValore() && (verificaImmediata() || verificaPosticipata())){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#valoreAssicurato").on("change", function(event) {
        if(verificanoPratica() && verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTarga() && verificaValore() && verificaDataImmatricolazione() &&  verificaTelaio() && verificaMarchio() && verificaCV() && verificaVersione() && (verificaImmediata()|| verificaPosticipata())){
            //removeParameters();
            $("#avanti1").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            $("#linkModulo").removeClass("not-active");

        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();

        }
    });
    $("#dataImmatricolazione").on("changeDate", function(event) {
        var data1 = $("#dataImmatricolazione").val().split('-');
        if(data1 != ''){
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTelaio() && verificaTarga() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && (verificaImmediata()|| verificaPosticipata()) ){
                //removeParameters();
                $("#avanti1").prop('disabled', false);
                var nome= $("#nomeInt").val();
                var cognome= $("#cognomeInt").val();
                var targa= $("#targa").val();
                var nopratica= $("#noPratica").val();
                //setGetParameter("nome",nome);
                //setGetParameter("cognome",cognome);
                //setGetParameter("targa",targa);
                //setGetParameter("nopratica",nopratica);
                $("#linkModulo").removeClass("not-active");
            }else{
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                //removeParameters();
            }
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#dataDecorrenza").on("changeDate", function(event) {
        var data1 = $("#dataDecorrenza").val();
        if(data1 != ''){
            $("#dataPosticipata").val("");
            //removeParameters();
            $("#decorrenzaAnti").prop("checked", true);
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTelaio() && verificaTarga() && verificaMarchio() && verificaVersione() && verificaValore() && verificaCV() && verificaDataImmatricolazione() ){
                //removeParameters();
                $("#avanti1").prop('disabled', false);
                var nome= $("#nomeInt").val();
                var cognome= $("#cognomeInt").val();
                var targa= $("#targa").val();
                var nopratica= $("#noPratica").val();
                //setGetParameter("nome",nome);
                //setGetParameter("cognome",cognome);
                //setGetParameter("targa",targa);
                //setGetParameter("nopratica",nopratica);
                $("#linkModulo").removeClass("not-active");
            }
            else{
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                //removeParameters();
            }
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#dataPosticipata").on("changeDate", function(event) {
        var data1 = $("#dataPosticipata").val();
        if(data1 != ''){
            $("#dataDecorrenza").val("");
            //removeParameters();
            $("#decorrenzaPost").prop("checked", true);
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTelaio() && verificaTarga() && verificaDataImmatricolazione() && verificaMarchio() && verificaCV() && verificaVersione() && verificaValore() && (verificaCellulare() ||verificaEmailCliente())){
                //removeParameters();
                $("#avanti1").prop('disabled', false);
                var nome= $("#nomeInt").val();
                var cognome= $("#cognomeInt").val();
                var targa= $("#targa").val();
                var nopratica= $("#noPratica").val();
                //setGetParameter("nome",nome);
                //setGetParameter("cognome",cognome);
                //setGetParameter("targa",targa);
                //setGetParameter("nopratica",nopratica);
                $("#linkModulo").removeClass("not-active");
            }
            else {
                $("#avanti1").prop('disabled', true);
                $("#linkModulo").addClass("not-active");
                //removeParameters();
            }
        }else{
            $("#avanti1").prop('disabled', true);
            $("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#email").on("change", function(event) {
        if(verificaEmailDealer() && verificaCapDealer()&& verificaComuneDealer()&& verificaIndirizzoDealer()&&verificaPIvaDealer()&&verificaProvDealer() &&verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente()&& verificaProvinciaCliente()&&verificaIndirizzoCliente()&&verificaSessoCliente()&&verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaCellulare() && verificaTelaio() && verificaTarga() && verificaDataImmatricolazione() && verificaMarchio() && verificaCV() && verificaVersione() && verificaValore() && (verificaCellulare() ||verificaEmailCliente()) ){
            $("#bottonSalva").prop('disabled', false);
        }else{
            if(!verificaEmailDealer()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtemailDealer);
                $("#bottonSalva").prop('disabled', true);
            }

        }

    });
    $("#avanti1").on ("click", function(event){
        if(verificaPosticipata()){
            href="#sectionC";
            attivaTab('sectionC');
        }else if(verificaImmediata()){
            href="#sectionB";
            attivaTab('sectionB');
        }
        verificapartitaIvaCliente();
        verificaTarga();
        verificaTelaio();
        verificaMarchio();
        verificaVersione();
        verificaDataImmatricolazione();
        verificaValore();
        verificaImmediata();
        verificaPosticipata();
        verificaCognomeCliente();
        verificaNomeCliente();
        verificaCapCliente();
        verificaProvinciaCliente();
        verificaIndirizzoCliente();
        verificaSessoCliente();
        verificaLocalitaCliente();

        if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaProvinciaCliente() || !verificaIndirizzoCliente() || !verificaSessoCliente() || !verificaLocalitaCliente() || !verificapartitaIvaCliente() || !verificaTarga() || !verificaTelaio() || !verificaMarchio() || !verificaVersione() || !verificaDataImmatricolazione()  || !verificaValore() || (!verificaImmediata() || !verificaPosticipata()) || !verificaEmailDealer()){
            $( ".alert" ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
            if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
            if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
            if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
            if(!verificaVersione()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
            if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
            if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
            if(!verificaImmediata() && !verificaPosticipata()) $( ".testoAlert").append(txtpostCliente).append("<br>");
            if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
            if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
            if(!verificaCapCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
            if(!verificaProvinciaCliente()) $( ".testoAlert").append(txtprovinciaCliente).append("<br>");
            if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
            if(!verificaSessoCliente()) $( ".testoAlert").append(txtsessoCliente).append("<br>");
            if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
            if(!verificaEmailDealer()) $( ".testoAlert").append(txtemailDealer).append("<br>");

        }
        if(verificaEmailDealer() && verificaCapDealer()&& verificaComuneDealer()&& verificaIndirizzoDealer()&&verificaPIvaDealer()&&verificaProvDealer() ){
            $("#bottonSalva").prop('disabled', false);
        }
    });
    $("#indietro1").on ("click", function(event){
        if(verificaPosticipata()){
            href="#sectionA";
            attivaTab('sectionA');
        }else if(verificaImmediata()){
            href="#sectionB";
            attivaTab('sectionB');
        }
    });

</script>
</body>
</html>