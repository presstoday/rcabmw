<html>
<head>
    <meta name="layout" content="main">
    <title>Login</title>

</head>
<body>
<div class="row animated zoomIn">
    <div class="col-xs-10 col-md-12">
        <div class="login-panel">
            <div class="row">
                <div class="col-xs-12 col-md-12 text-center">
                    <asset:image src="loghi/bmw_small.png" height="80px" id="logo-mach1" style="margin-right: 36px;"/>
                    <h1>RCA GRATUITA BMW MOTO</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <g:renderFlash/>
                    <f:form>
                        <div>
                            <sec:authenticationError/>
                        </div>
                        <div>
                            <input type="text" align="center" class="col-md-2 col-md-offset-5 col-xs-offset-3 col-xs-6 input-login" name="username"  id="username" required="true"  placeholder="CODICE" >
                        </div>
                        <div>
                            <input type="text" align="center" class="col-md-2 col-md-offset-5 col-xs-offset-3 col-xs-6 input-login" name="password"  id="password" required="true"  placeholder="PARTITA IVA" >
                        </div>
                        <div class="col-md-2 col-md-offset-5 col-xs-offset-4 col-xs-4">
                            <label for="remember-me" class="checkbox chk-login">
                                <input type="checkbox"  id="remember-me" name="remember-me" value="1" > Ricordami
                            </label>
                        </div>
                        <div>
                            <button class="col-md-2 col-md-offset-5 col-xs-6 col-xs-offset-3 btn" type="submit"><b>Login</b></button>
                        </div>
                    </f:form>
                </div>
            </div>

            <div class="col-md-12 col-xs-6">
                <footer class="col-md-10 col-md-push-1 col-xs-10 col-xs-push-1" >
                    <div class="col-md-10 col-md-push-1 col-xs-10 col-xs-push-1">
                        <p class="text-center"><asset:image src="/loghi/logo_mach1_small.png" height="30px" style="margin-top:90px; margin-left: 7px;" /></p>
                    </div>
                    <div  class="col-md-12 col-xs-12">
                        <p class="testofooter" align="center"> Mach1 s.r.l. - Via Vittor Pisani, 13/B  – 20124 Milano - TEL. 02 30465068 FAX 02 62694254 - Email: rcabmw@mach-1.it - CCIAA Milano - REA MI 1908726 - C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603 - Banco Posta Sede di Milano, Piazza Cordusio, 1 - IBAN IT 54 C 07601 01600 0000 99701393</p>
                    </div>
                </footer>
            </div>
        </div>
    </div>
</div>
</body>
</html>