<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>Dati Dealer</title>
</head>
<body>
<div class="col-md-12 col-md-push-11"><asset:image src="/loghi/mansutti.png" height="30px" style="margin-bottom: 25px;" /></div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h2 class="text-center h1Login">Inserimento nuovo dealer </h2>
                %{--<fa:icon name="user-plus" large="5x"/>--}%
            </div>
            <div class="panel-body">
                <f:form>
                    <div class="col-md-12">
                    <sec:ifHasRole role="ADMINDEALER">
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">CODICE</label>
                            <div class="input-group datiUtente">
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="username"  value="" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">RAGIONE SOCIALE</label>
                            <div class="input-group datiUtente">
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="ragioneSociale" style="text-transform:uppercase" value="" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">Indirizzo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-home"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="indirizzo" style="text-transform:uppercase" value=""/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Cap</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-inbox"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="cap" value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">Località</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="localita" style="text-transform:uppercase" value=""/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Provincia</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="provincia" style="text-transform:uppercase"  value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">P. IVA</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-list-alt"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="piva"  value=""/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">Telefono</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="telefono"  value=""/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">Fax</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-fax"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="fax"   value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">Mail</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="email"   value=""/>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 25px;">
                            <g:link page="1" controller="polizze" action="lista" class="btn btn-annulla col-md-3">Annulla</g:link>
                            %{-- <g:link controller="polizze" action="lista"><div class="btn btn-yellow col-md-offset-2  col-md-6"><b>Ok</b></div></g:link>--}%
                            <button type="submit" class="btn btn-yellow col-md-3 col-md-offset-6">Ok</button>
                        </div>
                    </sec:ifHasRole>
                </f:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>