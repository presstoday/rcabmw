<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>Dati Dealer</title>
</head>
<body>
<div class="col-md-12 col-md-push-11"><asset:image src="/loghi/mansutti.png" height="30px" style="margin-bottom: 25px;" /></div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h2 class="text-center">Dati Anagrafici </h2>
                %{--<fa:icon name="user-plus" large="5x"/>--}%
            </div>
            <div class="panel-body">
                <f:form>
                    <div class="col-md-12">
                    <sec:ifHasRole role="DEALER">
                        <div class="col-md-12"   role="alert">
                            <div> <i class="fa fa-hand-o-right fa-3x"></i><p class="testo-datiAnagrafici"> Verifica la correttezza dei dati. <br> Per segnalare eventuale modifiche mandare una mail a <ins>rcabmw@mach-1.it</ins></p></div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">RAGIONE SOCIALE</label>
                            <div class="input-group datiUtente">
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="ragioneSociale"  value="${utente.ragioneSociale}" readonly="true"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">Indirizzo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-home"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="indirizzo" readonly="true"  value="${utente.indirizzo}"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Cap</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-inbox"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="cap" readonly="true"  value="${utente.cap}"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">Località</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="localita" readonly="true"  value="${utente.localita}"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Provincia</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="provincia" readonly="true"  value="${utente.provincia}"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">P. IVA</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-list-alt"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="partitaiva"  value="${utente.piva}" readonly="true"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">Telefono</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="telefono"   value="${(utente.telefono!="null")?utente.telefono:""}"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">Fax</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-fax"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="fax" value="${(utente.fax!="null")?utente.fax:""}"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">Mail</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="email"  value="${utente.email!="null"?utente.email:""}"/>
                            </div>
                        </div>


                        </div>
                    </sec:ifHasRole>
                    <div class="col-md-4 col-xs-offset-9">
                        <br>
                        %{-- <g:link controller="polizze" action="lista"><div class="btn btn-yellow col-md-offset-2  col-md-6"><b>Ok</b></div></g:link>--}%
                     <button type="submit" class="btn btn-yellow col-xs-offset-2 col-sm-6">Ok</button>
                    </div>
                </f:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>