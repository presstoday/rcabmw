package rcabmw.polizze


class LogAttivita {
    Date dateCreated
    String descrizionePosticipazione
    Date dataAggiornamento

    static mapping = {
        descrizionePosticipazione type: "text"
    }
    static hasMany = [polizze: Polizza]
    static constraints = {

    }
}
