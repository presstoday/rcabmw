package rcabmw.polizze

class Tracciato {
    Date dateCreated
    Date dataCaricamento
    Date dataInvio
    String tracciato
    boolean annullata =false
    static belongsTo = [polizza: Polizza]

    static constraints = {
        tracciato nullable: false, size: 848..848
        polizza nullable: false
        dataCaricamento nullable: true
    }
    static mapping = {
        table "tracciati_iassicur"
        //tracciatoCompagnia defaultvalue="IT"

    }
    String toString() { return tracciato }
}
