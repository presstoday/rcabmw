package rcabmw.polizze

class Documenti{
    TipoDocumento tipo
    Date dateCreated, lastUpdated
    String fileName
    byte[] fileContent
    static belongsTo = [polizza: Polizza]

    static constraints = {
        fileName: unique: "polizza"
        fileContent nullable: false, size: 1..(1024 * 1024 * 5)
        tipo nullable: false
        polizza nullable: true
    }

    static mapping = {
        table "documenti_polizza"
    }
}
