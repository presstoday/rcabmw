package rcabmw.polizze
import rcabmw.utenti.Dealer
import groovy.time.TimeCategory as TC
import org.grails.databinding.BindingFormat


class Polizza {
    Date dateCreated = new Date()
    String noPolizza, noPratica, versione,cv, indirizzoInt,capInt,localitaInt,cellulare,emailInt,indirizzoGarante,capGarante,localitaGarante
    BigDecimal valoreAssicurato
    @BindingFormat("uppercase")String partitaIvaInt
    @BindingFormat("uppercase")String nomeInt
    @BindingFormat("uppercase")String cognomeInt
    @BindingFormat("uppercase")String provinciaInt
    @BindingFormat("uppercase")String partitaIvaGarante
    @BindingFormat("uppercase")String nomeGarante
    @BindingFormat("uppercase")String cognomeGarante
    @BindingFormat("uppercase")String provinciaGarante
    @BindingFormat("uppercase")String targa
    @BindingFormat("uppercase")String targaAcquisizione
    @BindingFormat("uppercase")String telaio
    @BindingFormat("uppercase")String marchio
    StatoPolizza stato = StatoPolizza.PREVENTIVO
    Acquisizione tipoAcquisizione = Acquisizione.NUOVA_POLIZZA
    TipoCliente tipoClienteInt = TipoCliente.M
    TipoCliente tipoClienteGarante = TipoCliente.M
    Contraente tipoContraente = Contraente.INTESTATARIO
    Date dataDecorrenza
    Date dataPosticipata
    Date dataImmatricolazione
    Date dataScadenza
    boolean accettata =false
    boolean motoveicolo =false
    Tracciato tracciato
    TracciatoCompagnia tracciatoCompagnia

    static belongsTo = [dealer: Dealer]
    static hasMany = [documenti: Documenti]

    static constraints = {
        noPratica nullable: false, unique: true
        noPolizza nullable: true
        telaio nullable: true, unique: true
        dataDecorrenza nullable: true
        dataPosticipata nullable: true
        tracciato nullable: true
        tracciatoCompagnia nullable: true
        dealer nullable: false
        dataImmatricolazione nullable: true
        dataScadenza nullable: true
        indirizzoInt nullable: false
        indirizzoGarante nullable: true
        versione nullable: true
        cellulare nullable: true
        partitaIvaInt nullable: false
        partitaIvaGarante nullable: true
        nomeInt nullable: false
        nomeGarante nullable: true
        cognomeInt nullable: false
        cognomeGarante nullable: true
        capInt nullable: false
        capGarante nullable: true
        localitaInt nullable: false
        localitaGarante nullable: true
        cv nullable: true
        provinciaInt nullable: false
        provinciaGarante nullable: true
        targa nullable: true, unique: true
        targaAcquisizione nullable: true, unique: true
        marchio nullable: true
        emailInt nullable: true
        valoreAssicurato nullable: true


        /* dataImmatricolazione validator: { data ->
             /* if(data > dataMassima) return ["polizze.dataImmatricolazione.data.max", data, dataMassima]
             def today = new Date(), minimo = use(TC) { today - 4.years }, massimo = use(TC) { today - 8.years }
             if(data > minimo) return "polizza.dataImmatricolazione.min.notmet"
             if(data < massimo) return "polizza.dataImmatricolazione.max.exceeded"*/
        // }
    }
    static mapping = {

    }

    def afterInsert() {
        if(stato in [StatoPolizza.RICHIESTA_INVIATA, StatoPolizza.POLIZZA_POSTICIPATA]) noPolizza = "BMR" + id.toString().padLeft(7, "0")
        else noPolizza = "PREV" + id.toString().padLeft(6, "0")
    }

    def afterUpdate() {
        if(stato in [StatoPolizza.POLIZZA, StatoPolizza.POLIZZA_POSTICIPATA]) noPolizza = "BMR" + id.toString().padLeft(7, "0")
    }

    void setDataPosticipata(Date data) {
        if(data) {
            dataPosticipata = data
            stato = StatoPolizza.POLIZZA_POSTICIPATA
        }
    }

    void setDataDecorrenza(Date data) {
        if(data) {
            dataDecorrenza = data
            stato = StatoPolizza.RICHIESTA_INVIATA
        }
    }
}
