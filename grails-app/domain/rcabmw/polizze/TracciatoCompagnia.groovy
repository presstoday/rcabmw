package rcabmw.polizze

class TracciatoCompagnia {
    Date dateCreated
    Date dataCaricamento
    Date dataInvio
    String tracciatoCompagnia
    boolean annullata =false
    static belongsTo = [polizza: Polizza]

    static constraints = {
        tracciatoCompagnia nullable: false
        polizza nullable: false
        dataCaricamento nullable: true
    }
    static mapping = {
        table "tracciati_compagnia"
        tracciatoCompagnia type: "text"
    }

    String toString() { return tracciatoCompagnia }
}
