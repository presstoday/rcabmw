package rcabmw.polizze

enum TipoDocumento {
    PASSAGGIO("passaggio"),
    BERSANI("bersani"),
    CERTIFICATO("certificato_polizza")


    final String value
    private TipoDocumento(String value) {
        this.value = value
    }
    String toString() { return value }
    static list() { return [PASSAGGIO, BERSANI,CERTIFICATO] }
}
