package rcabmw.polizze

enum StatoPolizza {
    PREVENTIVO("preventivo"), PREVENTIVO_SCADUTO("preventivo scaduto"), POLIZZA_POSTICIPATA("polizza posticipata"), POLIZZA("polizza"), RICHIESTA_INVIATA ("richiesta inviata"),POLIZZA_SCADUTA("polizza scaduta"),ANNULLATA("annullata"), NON_INTERESSATO("Non interessato")
    final String stato
    private StatoPolizza(String stato) { this.stato = stato }
    String toString() { return stato }
    static list() { return [PREVENTIVO, POLIZZA_POSTICIPATA, POLIZZA, RICHIESTA_INVIATA, PREVENTIVO_SCADUTO,ANNULLATA,NON_INTERESSATO] }
}
enum Acquisizione {
    NUOVA_POLIZZA("nuova polizza"),PASSAGGIO("passaggio"), BERSANI("Bersani")
    final String acquisizione
    private Acquisizione(String acquisizione) { this.acquisizione = acquisizione }
    String toString() { return acquisizione }
    static list() { return [PASSAGGIO,  BERSANI] }
}
enum TipoCliente {
    M("uomo", "M"), F("donna", "F"), DITTA_INDIVIDUALE("ditta individuale", ""), SOCIETA("società", "")
    final String tipo, value
    private TipoCliente(String tipo, String value) {
        this.tipo = tipo
        this.value = value
    }
    String toString() { return tipo }
    static list() { return [M, F, SOCIETA, DITTA_INDIVIDUALE] }
}
enum Contraente {
    INTESTATARIO("intestatario"),GARANTE("garante")
    final String contraente
    private Contraente(String contraente) { this.contraente = contraente }
    String toString() { return contraente }
    static list() { return [INTESTATARIO,  GARANTE] }
}

