package rcabmw.utenti


import org.grails.databinding.BindingFormat
import rcabmw.polizze.Polizza

class Dealer extends Utente  {

    @BindingFormat("capitalize")String ragioneSociale
    @BindingFormat("uppercase") String piva
    @BindingFormat("capitalize") String indirizzo
    @BindingFormat("capitalize") String localita
    @BindingFormat("capitalize") String provincia
    String cap,telefono,fax,pref

    //String codice
    boolean attivo = false

    static constraints = {
        ragioneSociale blank: false
        piva blank: false
        provincia nullable: true
        cap nullable: true
        telefono nullable: true
        fax nullable: true
        pref nullable: true

    }
    static hasMany =  [polizza: Polizza]


    static mapping = {
        table "dealers"
    }

    String toString() { return "${username}" }
    Set<Ruolo> getDefaultRoles() { return [Ruolo.DEALER] }
}
