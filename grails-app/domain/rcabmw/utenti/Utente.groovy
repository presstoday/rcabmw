package rcabmw.utenti


abstract class Utente {

    def passwordEncoder

    static transients = ["passwordEncoder"]

    String username
    String password
    String email
    boolean deleted = false
    boolean enabled = true
    boolean passwordScaduta = false
    Date dateCreated, lastUpdated

    static hasMany = [ruoli: Ruolo]

    static constraints = {
        username blank: false, unique: true
        password blank: false
        email blank: true, nullable: true
    }

    static mapping = {
        cache false
        version true
        dateCreated column: "creato_il"
        lastUpdated column: "modificato_il"
        ruoli joinTable: [name: "utenti_ruoli", key: "utente_id", column: "ruolo"]
        tablePerHierarchy false
        id generator: "increment"
        tablePerConcreteClass true
    }

    Utente addRuolo(Ruolo ruolo) { return addRuoli(ruolo) }

    Utente addRuoli(Ruolo... ruoli) {
        ruoli.each { Ruolo ruolo -> addToRuoli(ruolo) }
        return this
    }

    Utente removeRuolo(Ruolo ruolo) { return removeRuoli(ruolo) }

    Utente removeRuoli(Ruolo... ruoli) {
        ruoli.each { Ruolo ruolo -> removeFromRuoli(ruolo) }
        return this
    }

    def beforeInsert() {
        encodePassword()
        if(!ruoli) ruoli = defaultRoles
        else ruoli += defaultRoles
    }

    def beforeUpdate() { if(isDirty('password')) encodePassword() }

    def beforeDelete() {
        Utente.withNewSession { Utente.executeUpdate("update Utente as u set u.deleted = true where u.id = :id", [id: id]) }
        return false
    }

    protected void encodePassword() { password = passwordEncoder.encode(password) }

    String toString() { return username }

    abstract Set<Ruolo> getDefaultRoles()
}
