package tracciati

import groovy.time.TimeCategory
import rcabmw.polizze.*
import grails.converters.JSON



class TracciatiService {
    static transactional = false

    def generaTracciatoIAssicur(Polizza polizza) {
        def datiContabili=DatiContabili.getAll().first()
        def provincia=(polizza.tipoContraente.toString()=="garante")? polizza.provinciaGarante.toString().trim():polizza.provinciaInt.toString().trim()
        def tasse=Tasse.findByProvincia(provincia)
        def cognome=(polizza.tipoContraente.toString()=="garante")? polizza.cognomeGarante.toString().trim() : polizza.cognomeInt.toString().trim()
        def nome=(polizza.tipoContraente.toString()=="garante")? polizza.nomeGarante.toString().trim() : polizza.nomeInt.toString().trim()
        def provinciaContr=(polizza.tipoContraente.toString()=="garante")? polizza.provinciaGarante.toString().trim() : polizza.provinciaInt.toString().trim()
        def indirizzo=(polizza.tipoContraente.toString()=="garante")? polizza.indirizzoGarante.toString().trim() : polizza.indirizzoInt.toString().trim()
        def cap=(polizza.tipoContraente.toString()=="garante")? polizza.capGarante.toString().trim() : polizza.capInt.toString().trim()
        def citta=(polizza.tipoContraente.toString()=="garante")? polizza.localitaGarante.toString().trim() : polizza.localitaInt.toString().trim()
        def sesso=(polizza.tipoContraente.toString()=="garante")? polizza.tipoClienteGarante.value.toString().trim() : polizza.tipoClienteInt.value.toString().trim()
        def partitaIva=(polizza.tipoContraente.toString()=="garante")? polizza.partitaIvaGarante.toString().trim() : polizza.partitaIvaInt.toString().trim()
        /*def dataScadenza= use(TimeCategory) { polizza.dataDecorrenza +12.months}
        GregorianCalendar calendar = new GregorianCalendar()
        def yearInt = Integer.parseInt(dataScadenza.format("yyyy"))
        def monthInt = Integer.parseInt(dataScadenza.format("MM"))
        monthInt = monthInt - 1
        calendar.set(yearInt, monthInt, 1)
        def dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
        dataScadenza=new GregorianCalendar(yearInt,monthInt,dayInt).format("yyyyMMdd")*/
        def tracciato = [
                "Kasko":"I".padLeft(1),
                "Filler":"".padLeft(5),
                "modelloVeicolo":"${polizza.versione.toString().trim()}".padLeft(20),
                "Targa":"${polizza.targa}".toString().trim().padLeft(15),
                "Cognome":"${cognome +" "+nome}".toString().trim().padLeft(40),
                "Nome": "".padLeft(40),
                "Provincia":"${provincia}".padLeft(2),
                "Valuta":"E".padLeft(1),
                "Valore assicurato":(polizza.valoreAssicurato)? "${(polizza.valoreAssicurato*100).round()}".padLeft(9):"".padLeft(9),
                "Data immatricolazione":(polizza.dataImmatricolazione)? "${polizza.dataImmatricolazione.format('yyyyMMdd')}".padLeft(8):"".padLeft(8),
                "Durata furto incendio": "".padLeft(3),
                "Indirizzo": "${indirizzo}".padLeft(30),
                "Cap": "${cap}".padLeft(5),
                "Città": "${citta}".padLeft(30),
                "Data di nascita": "".padLeft(8),
                "filler2":"".padLeft(2),
                "Sesso":"${sesso}".padLeft(1),
                "Numero polizza":"${polizza.noPratica}".padLeft(10),
                "Codice operazione": "0".padLeft(3),
                "filler3":"".padLeft(8),
                "filler4":"".padLeft(9),
                "Codice fiscale / Partita IVA":"${partitaIva}".padLeft(16),
                "Marca":  "${(datiContabili.marca.toString().trim())}".padLeft(5),
                "Autocarro": "N".padLeft(1),
                "filler5":"".padLeft(1),
                "Scadenza vincolo": "".padLeft(8),
                "filler6": "".padLeft(5),
                "Numero telaio": polizza.telaio.trim().padLeft(17),
                "filler7": "".padLeft(2),
                "Vincolo": "".padLeft(1),
                "Pcl":  "".padLeft(9),
                "filler8": "".padLeft(10),
                "Cv fisc": "".padLeft(4),
                "filler9": "".padLeft(4),
                "Data decorrenza": (polizza.dataDecorrenza)?polizza.dataDecorrenza.format("yyyyMMdd").padLeft(8):"".padLeft(8),
                "filler10": "".padLeft(1),
                "Satellitare": "".padLeft(1),
                "Data scadenza": (polizza.dataDecorrenza)?polizza.dataScadenza.format("yyyyMMdd").padLeft(8):"".padLeft(8),
                "Rc": "S".padLeft(1),
                "Telefono casa": "".padLeft(15),
                "Telefono ufficio": "".padLeft(15),
                "filler11": "".padLeft(15),
                "Cellulare": (polizza.cellulare)?"${polizza.cellulare.toString().trim()}".padLeft(15):"".padLeft(15),
                "Provvigioni dealer":  "".padLeft(9),
                "Provvigioni Mach1":  "${(datiContabili.imponibile*0.15* 100).round()}".padLeft(9),
                "Provvigioni mansutti": "".padLeft(9),
                "Totale caricamenti":  "${((datiContabili.provvigioniDealer+(datiContabili.imponibile*0.15)+datiContabili.provvigioniMansutti)* 100).round()}".padLeft(9),
                "Imponibile":  "${(datiContabili.imponibile* 100).round()}".padLeft(9),
                "Imposte":  "${(datiContabili.imponibile*tasse.tasse).round()}".padLeft(9),
                "Pdu":  "${(datiContabili.pdu* 100).round()}".padLeft(9),
                "filler12": "".padLeft(34),
                "Collisione": "".padLeft(1),
                "Data operazione": "".padLeft(8),
                "Codice prodotto":  "RC".padLeft(2),
                "Tipologia veicolo": "".padLeft(1),
                "Rischio": "".padLeft(1),
                "Zona": "".padLeft(1),
                "Email": (polizza.emailInt)? "${polizza.emailInt.toString().trim()}".padLeft(50):"".padLeft(50),
                "filler13": "".padLeft(2),
                "filler14": "".padLeft(2),
                "filler15": "".padLeft(2),
                "Durata kasko": "00".padLeft(2),
                "Durata collisione": "00".padLeft(2),
                "Durata valore a nuovo": "00".padLeft(2),
                "Codice società commerciale": "".padLeft(3),
                "Codice venditore": "".padLeft(3),
                "Provvigioni venditore": "".padLeft(9),
                "filler16": "".padLeft(3),
                "filler17": "".padLeft(9),
                "Integra": "".padLeft(1),
                "Numero polizza integrata": "".padLeft(10),
                "Durata integra": "".padLeft(2),
                "filler18": "".padLeft(27),
                "filler19": "".padLeft(30),
                "filler20": "".padLeft(16),
                "filler21": "".padLeft(40),
                "filler22": "".padLeft(11),
                "filler23": "".padLeft(30),
                "filler24": "".padLeft(30),
                "filler25": "".padLeft(5),
                "filler26": "".padLeft(2),
                "filler27": "".padLeft(16),
                "filler28": "".padLeft(8),
                "filler29": "".padLeft(1),
                "filler30": "".padLeft(7)
        ]
        def dataInvioTracciato=new Date()
        /***
         * da usare per rca gratuita
         * ***/
         /*def datecreatedPolizza= Integer.parseInt(polizza.dateCreated.format('H'))
         if(datecreatedPolizza>10){
            dataInvioTracciato=new Date(dataInvioTracciato.getTime() + (24 * 60 * 60 * 1000));
         }*/

        return new Tracciato(polizza: polizza, tracciato: tracciato.values().join(), dataInvio: dataInvioTracciato)
    }

    private Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.ITALY);
        cal.setTime(date);
        return cal;
    }
}
