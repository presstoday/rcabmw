package mail

import grails.core.GrailsApplication
import grails.transaction.Transactional
import grails.util.Environment
import mandrill.MandrillAttachment
import mandrill.MandrillMessage
import mandrill.MandrillRecipient
import mandrill.MergeVar
import org.apache.commons.mail.EmailException
import org.apache.commons.mail.HtmlEmail

import javax.mail.util.ByteArrayDataSource
import javax.mail.util.ByteArrayDataSource as ByteArrayDS
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class MailService {
    def mandrillService
    GrailsApplication grailsApplication

    def invioMail(email, nome, cognome, dataPosticipata, noPratica) {
        def recpts = []
        def attachs=[]
        def contents = []
        def datiUsuario= "${nome}  ${cognome}"
        def datiPratica= "${noPratica}"
        def vars = [new MergeVar(name: "CREDENZIALI", content: "${datiUsuario}"),new MergeVar(name: "POSTICIPATA", content: "${dataPosticipata}"),new MergeVar(name: "PRATICA", content: "${datiPratica}")]
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:nome, email: "priscila@presstoday.com"))
        }else if(Environment.current == Environment.TEST) {
            recpts.add(new MandrillRecipient(name:nome, email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:nome, email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:nome, email: "s.carino@mach-1.it"))
        }
        else{
            recpts.add(new MandrillRecipient(name:nome, email: "${email}"))
        }

        /** quando andremmo in prodrecpts.add(new MandrillRecipient(name:"${nome}${cognome}", email: email))*/
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Preventivo polizza",
                from_email:"clienti-bmw@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "rcabmw-polizzaposticipata", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return inviatoCliente
    }

    def invioRichiesta(email, nome, cognome, noPratica) {
        def recpts = []
        def attachs=[]
        def contents = []
        def datiUsuario= "${nome}  ${cognome}"
        def datiPratica= "${noPratica}"
        def vars = [new MergeVar(name: "CREDENZIALI", content: "${datiUsuario}"),new MergeVar(name: "PRATICA", content: "${datiPratica}")]
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:nome, email: "priscila@presstoday.com"))
        }else if(Environment.current == Environment.TEST) {
            recpts.add(new MandrillRecipient(name:nome, email: "priscila@presstoday.com"))
           recpts.add(new MandrillRecipient(name:nome, email: "c.sivelli@mach-1.it"))
          recpts.add(new MandrillRecipient(name:nome, email: "s.carino@mach-1.it"))
        }
        else{
            recpts.add(new MandrillRecipient(name:nome, email:  "${email}"))
            recpts.add(new MandrillRecipient(name:nome, email: "documenti1@mach-1.it"))
        }

        /** quando andremmo in prodrecpts.add(new MandrillRecipient(name:"${nome}${cognome}", email: email))*/
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Richiesta posizione assicurativa",
                from_email:"documenti-bmw@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "rcabmw-acquisizionepolizza", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return inviatoCliente
    }
    private class EmailCategory {
        static void to(HtmlEmail email, to = "") {
            def addresses = []
            if(to instanceof String) addresses = to.split(",").collect { address -> address.trim() }
            else if(to instanceof List) addresses = to
            else if(to instanceof String[]) addresses = to as List
            addresses.each { address -> email.addTo(address) }
        }
        static void cc(HtmlEmail email, cc = "") {
            def addresses = []
            if(cc instanceof String) addresses = cc.split(",").collect { address -> address.trim() }
            else if(cc instanceof List) addresses = cc
            else if(cc instanceof String[]) addresses = cc as List
            addresses.each { address -> email.addTo(address) }
        }
        static void attach(HtmlEmail email, file, fileName, type = "text/plain", description = "") {
            def data = null
            if(file instanceof byte[] || file instanceof InputStream) data = file
            else if(file instanceof File) data = file.bytes
            if(data) {
                def source = new ByteArrayDataSource(data, type)
                email.attach(source, fileName, description)
            }
        }
        static void subject(HtmlEmail email, subject = "") { email.subject = subject }
        static void msg(HtmlEmail email, msg = "") { email.msg = msg }
    }

    def sendMail(Closure closure) throws EmailException {
        def emailConfig = grailsApplication.config.mail
        def email = new HtmlEmail()
        use(EmailCategory) {
            email.hostName = emailConfig.host
            email.smtpPort = emailConfig.port
            email.from = emailConfig.from
            closure.resolveStrategy = Closure.DELEGATE_ONLY
            closure.delegate = email
            closure.call()
            return email.send()
        }
    }

    def invioMailCaricamento(risposta, filename) {
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaFinale, fileNameRisposta
        rispostaFinale=risposta
        fileNameRisposta = "risposta_${filename}.txt"
        zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
        zipRiassunti.write(rispostaFinale.bytes)
        zipRiassunti.close()
        streamRiassunti.close()
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def vars = [new MergeVar(name: "DATAC", content: "${new Date().format("dd-MMM-yyyy")}")]
        fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "riassunto_${filename}.zip", content: fileEncode))
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia Carino", email: "s.carino@mach-1.it"))
        }else{
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco pratiche non caricate Portale RCA DEUTSCHE",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-caricamento-pratiche", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }

}
