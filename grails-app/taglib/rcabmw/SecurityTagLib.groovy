package rcabmw

import utils.*
import org.springframework.security.web.WebAttributes
import org.springframework.security.authentication.*
import security.SecurityUtils



class SecurityTagLib implements TagLibUtils {

    static namespace = "sec"

    def securityService

    def authenticationError = { attrs ->
        def exception = session[WebAttributes.AUTHENTICATION_EXCEPTION]
        if(exception) {
            def message = ""
            if(exception instanceof BadCredentialsException) message = "Credenziali di accesso errate"
            else if(exception instanceof DisabledException) message = "L'account è stato disabilitato"
            else if(exception instanceof LockedException) message = "L'account è stato chiuso"
            else if(exception instanceof CredentialsExpiredException) message = "Le credenziali di accesso sono scadute"
            else if(exception instanceof AccountExpiredException) message = "L'account è scaduto"
            def html = new HtmlBuilder(out)
            html.div(class: "col-md-4 col-md-offset-4 col-xs-offset-4 col-xs-4  alert alert-info text-center") {
                mkp.yield message
            }
        }
    }

    def loggedInUser = { attrs ->
        out << securityService.utente
    }

    def ifLoggedIn = { attrs, body ->
        if(SecurityUtils.isLoggedIn()) out << body()
    }

    def ifHasRole = { attrs, body ->
        def role = attrs.remove("role") ?: throwTagError("Specificare l'attributo [role] per il tag [sec:ifHasRole]")
        if(SecurityUtils.hasRole(role)) out << body()
    }

    def ifHasNotRole = { attrs, body ->
        def role = attrs.remove("role") ?: throwTagError("Specificare l'attributo [role] per il tag [sec:ifHasNotRole]")
        if(SecurityUtils.hasNotRole(role)) out << body()
    }

    def ifHasAllRoles = { attrs, body ->
        def roles = attrs.remove("roles") ?: throwTagError("Specificare l'attributo [roles] per il tag [sec:ifHasAllRoles]")
        if(SecurityUtils.hasAllRoles(roles.split(" ")*.split(",").flatten())) out << body()
    }

    def ifHasAnyRoles = { attrs, body ->
        def roles = attrs.remove("roles") ?: throwTagError("Specificare l'attributo [roles] per il tag [sec:ifHasAnyRoles]")
        if(SecurityUtils.hasAnyRoles(roles.split(" ")*.split(",").flatten())) out << body()
    }

    def ifHasNotRoles = { attrs, body ->
        def roles = attrs.remove("roles") ?: throwTagError("Specificare l'attributo [roles] per il tag [sec:ifHasNotRoles]")
        if(SecurityUtils.hasNotRoles(roles.split(" ")*.split(",").flatten())) out << body()
    }

    def loginAs = { attrs, body ->
        def username = attrs.remove("username") ?: throwTagError("Specificare l'attributo [username] per il tag [sec:loginAs]")
        if(!attrs.params) attrs.params = [:]
        attrs.params["username"] = username
        attrs.uri = "/loginAs"
        out << g.link(attrs, body)
    }

    def logout = { attrs, body ->
        if(SecurityUtils.isSwitched()) attrs.uri = "/logoutAs"
        else attrs.uri = "/logout"
        out << g.link(attrs, body)
    }

    def ifSwitched = { attrs, body ->
        if(SecurityUtils.isSwitched()) out << body()
    }

    def switchedUser = { attrs ->
        out << securityService.switchedUtente
    }

}
