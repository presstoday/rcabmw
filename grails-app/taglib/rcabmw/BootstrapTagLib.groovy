package rcabmw

import utils.*

class BootstrapTagLib implements TagLibUtils {

    static namespace = "bs"

    def alert = { attrs, body ->
        def text = attrs.remove("message") ?: attrs.remove("text") ?: body() ?: ""
        def id = attrs.remove("id") ?: randomId("alert")
        def dismissable = true, dismissAfter = -1
        if(attrs.containsKey("dismissable")) dismissable = attrs.remove("dismissable").with { it == "false" || it == false } ? false : true
        else if(attrs.containsKey("dismiss-after")) {
            dismissable = false
            dismissAfter = Integer.valueOf(attrs.remove("dismiss-after")) ?: 5000
        }
        Map alertAttrs = attrs + [id: id, role: "alert"]
        addHtmlClass(alertAttrs, "alert")
        if(dismissable) addHtmlClass(alertAttrs, " alert-dismissable fade in")
        def html = new HtmlBuilder(out)
        html.div(alertAttrs) {
            if(dismissable) button(type: "button", class: "close", "data-dismiss": "alert", "aria-label": "Chiudi") { span("aria-hidden": true) { mkp.yieldUnescaped "&times;" } }
            mkp.yieldUnescaped text
        }
        if(dismissable == false && dismissAfter > 0) asset.script { /setTimeout(function() { $("#$id").alert("close"); }, ${dismissAfter});/ }
    }

    def modal = { attrs, body ->
        def title = attrs.remove("title") ?: ""
        def content = body()?.trim() ?: ""
        def id = attrs.remove("id") ?: randomId("modal")
        def dialogAttrs = attrs + [id: id]
        addHtmlClass(dialogAttrs, "modal fade")
        def html = new HtmlBuilder(out)
        html.div(dialogAttrs) {
            div(class: "modal-dialog") {
                div(class: "modal-content") {
                    div(class: "modal-header") {
                        button(type: "button", class: "close", "data-dismiss": "modal", "aria-label": "Chiudi") { span("aria-hidden": true) { mkp.yieldUnescaped "&times;" } }
                        h4(class: "modal-title") { mkp.yieldUnescaped title }
                    }
                    div(class: "modal-body") { mkp.yieldUnescaped content }
                }
            }
        }
    }

}
