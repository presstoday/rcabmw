import org.springframework.security.access.AccessDeniedException

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        name list: "/$controller/Polizze/$page" {
            //controller = "polizze"
            action = "lista"
        }
        //"/$page?"(controller: "polizze", action: "lista")
        "/login"(view: "/login")
        "/passwordScaduta"(controller: "security", action: "passwordScaduta")
        "/notAuthorized"(view: "/notAuthorized")
        "/"(controller: "polizze", action: "lista")
        "/grails"(view: "/grails")
        //"/"(view:"/index")
        "500"(view:'/error')
        "500"(view: "/notAuthorized", exception: AccessDeniedException)
        "403"(view:'/notAuthorized')
        "404"(view:'/notFound')
    }
}
