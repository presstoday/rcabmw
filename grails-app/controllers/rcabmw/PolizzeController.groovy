package rcabmw

import com.itextpdf.text.Document
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper
import com.itextpdf.text.pdf.PdfWriter
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import excel.reader.*
import grails.converters.JSON
import groovy.time.TimeCategory
import org.grails.databinding.BindingFormat
import rcabmw.polizze.Acquisizione
import rcabmw.polizze.Contraente
import rcabmw.polizze.Documenti
import rcabmw.polizze.LogAttivita
import rcabmw.polizze.Polizza
import rcabmw.polizze.StatoPolizza
import rcabmw.polizze.Tasse
import rcabmw.polizze.TipoCliente
import rcabmw.polizze.TipoDocumento
import rcabmw.utenti.Admin
import rcabmw.utenti.Dealer
import rcabmw.utenti.*
import security.SecurityUtils
import groovy.time.TimeCategory as TC
import sun.rmi.runtime.Log
import javax.validation.constraints.Null
import java.security.Security
import java.util.regex.Pattern
import java.util.zip.ZipEntry


class PolizzeController {
    def tracciatiService
    def mailService
    def index() {}
    def lista(){
        //CaricaPolizzeJob.triggerNow()
        def principal = SecurityUtils.principal
        def utente,dealer,admin,posticipato
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }
        if( SecurityUtils.hasRole("ADMINPOSTICIPATI")){
            posticipato=Admin.get(principal.id)
            utente=posticipato
        }
        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search
        def polizze = Polizza.createCriteria().list(max: max, offset: offset) {
                if(posticipato){
                    or {
                        and{
                            eq "stato", StatoPolizza.NON_INTERESSATO
                        }
                        and{
                            eq "stato", StatoPolizza.POLIZZA_POSTICIPATA
                        }

                    }

                }
                if(dealer) {
                    eq "dealer", dealer
                }
                else {
                    projections {
                        createAlias ('dealer', 'd')
                    }
                }
                if(cerca) {
                    or {
                        ilike "noPratica", "%${cerca}%"
                        ilike "nomeInt", "%${cerca}%"
                        ilike "partitaIvaInt", "%${cerca}%"
                        //ilike "tipoCliente",TipoCliente.DITTA_INDIVIDUALE.tipo
                        if(!dealer){
                            ilike "d.ragioneSociale", "%${cerca}%"
                        }
                    }
                }
                and {
                    order('stato', 'desc')
                    if(!dealer){
                        order('d.ragioneSociale', 'asc')
                    }

                }
            }

        def pages = Math.ceil(polizze.totalCount / max) as int
        def totalPages=pages
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "lista", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "lista", params: [page: 1, search: params.search]
        }
        def province = Tasse.withCriteria {
            projections {
                property "provincia"
            }
        }
        [polizze: polizze, pages: pages, page: page,utente:utente, province: province,totalPages:totalPages]
    }
    def getPolizza(String idPolizza){
        def polizza=Polizza.get(idPolizza)
        def risposta
        def logAttivita
        if(polizza){
            logAttivita=LogAttivita.createCriteria().list(){
                projections {
                    property('dataAggiornamento')
                    property('descrizionePosticipazione')
                }
                polizze{
                    eq ('id', polizza.id)
                }
                and {
                    order('dataAggiornamento', 'asc')
                }
            }
            def cognome,nome,sesso,partitaIva,indirizzo,localita,provincia,cap
            if(polizza.tipoContraente==Contraente.GARANTE){
                  cognome=polizza.cognomeGarante
                  nome=polizza.nomeGarante
                  sesso=polizza.tipoClienteGarante.value
                  partitaIva=polizza.partitaIvaGarante
                  indirizzo=polizza.indirizzoGarante
                  localita=polizza.localitaGarante
                  provincia=polizza.provinciaGarante
                  cap=  polizza.capGarante

            }else{
                cognome=polizza.cognomeInt
                nome=polizza.nomeInt
                sesso=polizza.tipoClienteInt.value
                partitaIva=polizza.partitaIvaInt
                indirizzo=polizza.indirizzoInt
                localita=polizza.localitaInt
                provincia=polizza.provinciaInt
                cap=  polizza.capInt
            }

           risposta=[noPratica:polizza.noPratica,stato:polizza.stato.toString().toUpperCase(),cognomeInt:cognome.toString().toUpperCase(),nomeInt:nome.toString().toUpperCase(),tipoClienteInt:sesso.toString().toUpperCase(),partitaIvaInt:partitaIva,indirizzoInt:indirizzo?:"",capInt:cap?:"",localitaInt:localita?:"",provinciaInt:provincia?:"",dealer:polizza.dealer.ragioneSociale,piva:polizza.dealer.piva,indirizzo:polizza.dealer.indirizzo,cap:polizza.dealer.cap,provincia:polizza.dealer.provincia,pref:polizza.dealer.pref?:"",telefono:polizza.dealer.telefono?:"",fax:polizza.dealer.fax?:"",comune:polizza.dealer.localita,email:polizza.dealer.email?:"",targa:polizza.targa?:"",telaio:polizza.telaio?:"",marchio:polizza.marchio?:"",dataImmatricolazione:polizza.dataImmatricolazione?.format("dd-MM-yyyy")?:"",dataPosticipata:polizza.dataPosticipata?.format("dd-MM-yyyy")?:"",versione:polizza.versione?:"",motoveicolo:polizza.motoveicolo?:"",cellulare:polizza.cellulare?:"",emailInt:polizza.emailInt?:"",valoreAssicurato:polizza.valoreAssicurato?:"",cv:polizza.cv?:"",logAttivita_data:logAttivita?:"",targaAcquisizione:polizza.targaAcquisizione?:"",tipoAcquisizione:polizza.tipoAcquisizione.toString()?:"",risposta:true]
        }
        else{risposta=[risposta:false]}
        render risposta as JSON
    }
    def aggiornaPolizza(long idPolizza){
        def logg
        if(Polizza.exists(idPolizza)) {
            def polizza = Polizza.get(idPolizza)
            bindData(polizza, params, [exclude: ['indirizzoInt', 'capInt','localitaInt','partitaIvaInt','nomeInt','cognomeInt','provinciaInt','tipoClienteInt','indirizzoGarante','capGarante','localitaGarante','partitaIvaGarante','nomeGarante','cognomeGarante','provinciaGarante','tipoClienteGarante','noPratica']])
            /****data scadenzaa*/
            def dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +12.months}:""
            if(dataScadenza!=""){
                GregorianCalendar calendar = new GregorianCalendar()
                def yearInt = Integer.parseInt(dataScadenza.format("yyyy"))
                def monthInt = Integer.parseInt(dataScadenza.format("MM"))
                monthInt = monthInt - 1
                calendar.set(yearInt, monthInt, 1)
                def dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
                dataScadenza=new GregorianCalendar(yearInt,monthInt,dayInt).format("dd-MM-yyyy")
                Date dataFormatata = Date.parse( "dd-MM-yyyy", dataScadenza )
                polizza.dataScadenza=dataFormatata
            }
            //aggiornamenti intestatario
            if(params.tipoContraente=="GARANTE"){
                polizza.nomeGarante=params.nomeInt.toString().trim()?:null
                polizza.cognomeGarante=params.cognomeInt.toString().trim()?:null
                polizza.tipoClienteGarante=params.tipoClienteInt.toString().trim()?:null
                polizza.partitaIvaGarante=params.partitaIvaInt.toString().trim()?:null
                polizza.indirizzoGarante=params.indirizzoInt.toString().trim()?:null
                polizza.localitaGarante=params.localitaInt.toString().trim()?:null
                polizza.capGarante=params.capInt.toString().trim()?:null
                polizza.provinciaGarante=params.provinciaInt.toString().trim()?:null
            }
            //aggiornamenti dati dealer
            if((params.telefono.toString().trim()!='' && params.telefono.toString().trim() != polizza.dealer.telefono ))polizza.dealer.telefono=params.telefono.toString().trim()
            if((params.fax.toString().trim()!='' && params.fax.toString()!= polizza.dealer.fax))polizza.dealer.fax=params.fax.toString().trim()
            if((params.email.toString().trim()!='' && params.email.toString()!= polizza.dealer.email)) polizza.dealer.email=params.email.toString().trim()

            if(params.tipoAcquisizione=="NUOVA_POLIZZA"){
                polizza.targaAcquisizione=null
            }else if((params.tipoAcquisizione=="PASSAGGIO" || params.tipoAcquisizione=="BERSANI")){
                if((params.fileContentAcquisizione?.size>0)){
                    def documentiAcquisizione=new Documenti()
                    if (polizza.stato!=StatoPolizza.POLIZZA_POSTICIPATA){
                        if(params.tipoAcquisizione=="PASSAGGIO"){
                            documentiAcquisizione.tipo=TipoDocumento.PASSAGGIO

                        }else if(params.tipoAcquisizione=="BERSANI"){
                            documentiAcquisizione.tipo=TipoDocumento.BERSANI
                        }
                        documentiAcquisizione.fileName=params.fileContentAcquisizione?.originalFilename?:null
                        documentiAcquisizione.fileContent=params.fileContentAcquisizione?.bytes?:null
                        polizza.targaAcquisizione=params.targaAcquisizione?:null
                    }else{
                        if(params.tipoAcquisizione=="PASSAGGIO"){
                            documentiAcquisizione.tipo=TipoDocumento.PASSAGGIO

                        }else if(params.tipoAcquisizione=="BERSANI"){
                            documentiAcquisizione.tipo=TipoDocumento.BERSANI
                        }
                        documentiAcquisizione.fileName=params.fileContentAcquisizione?.originalFilename?:null
                        documentiAcquisizione.fileContent=params.fileContentAcquisizione?.bytes?:null
                        if(params.targaAcquisizione && (params.targaAcquisizione.toString().trim()!=polizza.targaAcquisizione))polizza.targaAcquisizione=params.targaAcquisizione
                    }
                    polizza.addToDocumenti(documentiAcquisizione)
                }
            }
            if(polizza.save(flush:true)) {
                logg =new rcabmw.utenti.Log(parametri: "Polizza salvata-->${polizza.noPolizza}", operazione: " aggiorna polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(polizza.stato==StatoPolizza.RICHIESTA_INVIATA){
                    def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                    if(!tracciatoPolizza.save(flush: true)){
                        logg =new rcabmw.utenti.Log(parametri: "Errore generazione tracciato ${renderErrors(bean: tracciatoPolizza)}", operazione: " aggiorna polizza", pagina: "POLIZZE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error = renderErrors(bean: tracciatoPolizza)
                    } else{
                        logg =new rcabmw.utenti.Log(parametri: "RICHIESTA_INVIATA per polizza -->${polizza.noPolizza} ", operazione: " aggiorna polizza", pagina: "POLIZZE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.message="RICHIESTA_INVIATA"
                    }
                } else if (polizza.stato==StatoPolizza.POLIZZA_POSTICIPATA) {
                    flash.message="POSTICIPATA"
                    logg =new rcabmw.utenti.Log(parametri: "POSTICIPATA -->${polizza.noPolizza}", operazione: " aggiorna polizza", pagina: "POLIZZE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    def nome,cognome
                    if(polizza.tipoContraente=="GARANTE"){
                        nome= polizza.nomeGarante
                        cognome= polizza.cognomeGarante
                    }else{
                        nome= polizza.nomeInt
                        cognome= polizza.cognomeInt
                    }
                    mailService.invioMail(polizza.emailInt, nome, cognome, polizza.dataPosticipata.format("dd-MM-yyyy").toString(),polizza.noPratica)
                }

            } else {
                logg =new rcabmw.utenti.Log(parametri: "errore aggiornamento polizza-->${renderErrors(bean: polizza)}", operazione: " aggiorna polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "errori-->${renderErrors(bean: polizza)}"
                flash.error = renderErrors(bean: polizza)
            }


            redirect action: "lista", params: params.search ? [search: params.search] : [:]
        } else response.sendError(404)
    }
    def nuovaPolizza(){
        def logg
        def polizza=new Polizza()
        def dealer =  Dealer.createCriteria().list() {
           /* projections {
                property "ragioneSociale"
            }*/
        }
        def province = Tasse.withCriteria {
            projections {
                property "provincia"
            }
        }
        if(request.post) {
            bindData(polizza, params, [exclude: ['indirizzoGarante','capGarante','localitaGarante','partitaIvaGarante','nomeGarante','cognomeGarante','provinciaGarante','tipoClienteGarante']])

            /****data scadenzaa*/
            def dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +12.months}:""
            if(dataScadenza!=""){
                GregorianCalendar calendar = new GregorianCalendar()
                def yearInt = Integer.parseInt(dataScadenza.format("yyyy"))
                def monthInt = Integer.parseInt(dataScadenza.format("MM"))
                monthInt = monthInt - 1
                calendar.set(yearInt, monthInt, 1)
                def dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
                dataScadenza=new GregorianCalendar(yearInt,monthInt,dayInt).format("dd-MM-yyyy")
                Date dataFormatata = Date.parse( "dd-MM-yyyy", dataScadenza )
                polizza.dataScadenza=dataFormatata
            }

            //aggiornamenti dati dealer
            if((params.telefono.toString().trim()!='' && params.telefono.toString().trim() != polizza.dealer.telefono ))polizza.dealer.telefono=params.telefono.toString().trim()
            if((params.fax.toString().trim()!='' && params.fax.toString()!= polizza.dealer.fax))polizza.dealer.fax=params.fax.toString().trim()
            if((params.email.toString().trim()!='' && params.email.toString()!= polizza.dealer.email)) polizza.dealer.email=params.email.toString().trim()

            if(params.tipoAcquisizione=="NUOVA_POLIZZA"){
                polizza.targaAcquisizione=null
            }else if((params.tipoAcquisizione=="PASSAGGIO" || params.tipoAcquisizione=="BERSANI")){
                if((params.fileContentAcquisizione?.size>0)){
                    def documentiAcquisizione=new Documenti()
                    if (polizza.stato!=StatoPolizza.POLIZZA_POSTICIPATA){
                        if(params.tipoAcquisizione=="PASSAGGIO"){
                            documentiAcquisizione.tipo=TipoDocumento.PASSAGGIO

                        }else if(params.tipoAcquisizione=="BERSANI"){
                            documentiAcquisizione.tipo=TipoDocumento.BERSANI
                        }
                        documentiAcquisizione.fileName=params.fileContentAcquisizione?.originalFilename?:null
                        documentiAcquisizione.fileContent=params.fileContentAcquisizione?.bytes?:null
                        polizza.targaAcquisizione=params.targaAcquisizione?:null
                    }else{
                        if(params.tipoAcquisizione=="PASSAGGIO"){
                            documentiAcquisizione.tipo=TipoDocumento.PASSAGGIO

                        }else if(params.tipoAcquisizione=="BERSANI"){
                            documentiAcquisizione.tipo=TipoDocumento.BERSANI
                        }
                        documentiAcquisizione.fileName=params.fileContentAcquisizione?.originalFilename?:null
                        documentiAcquisizione.fileContent=params.fileContentAcquisizione?.bytes?:null
                        if(params.targaAcquisizione && (params.targaAcquisizione.toString().trim()!=polizza.targaAcquisizione))polizza.targaAcquisizione=params.targaAcquisizione
                    }
                    polizza.addToDocumenti(documentiAcquisizione)
                }
            }
            if(polizza.save(flush:true)) {
                logg =new rcabmw.utenti.Log(parametri: "Polizza salvata --> ${polizza.noPolizza}", operazione: " nuova polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(polizza.stato==StatoPolizza.RICHIESTA_INVIATA){
                    def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                    if(!tracciatoPolizza.save(flush: true)){
                        logg =new rcabmw.utenti.Log(parametri: "Errore generazione tracciato ${renderErrors(bean: tracciatoPolizza)}", operazione: " nuova polizza", pagina: "POLIZZE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error = renderErrors(bean: tracciatoPolizza)
                    } else{
                        flash.message="RICHIESTA_INVIATA"
                        logg =new rcabmw.utenti.Log(parametri: "RICHIESTA_INVIATA  --> ${polizza.noPolizza}", operazione: " nuova polizza", pagina: "POLIZZE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                } else if (polizza.stato==StatoPolizza.POLIZZA_POSTICIPATA) {
                    flash.message="POSTICIPATA"
                    logg =new rcabmw.utenti.Log(parametri: "POLIZZA POSTICIPATA --> ${polizza.noPolizza}", operazione: " nuova polizza", pagina: "POLIZZE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    def nome,cognome
                    if(polizza.tipoContraente=="GARANTE"){
                        nome= polizza.nomeGarante
                        cognome= polizza.cognomeGarante
                    }else{
                        nome= polizza.nomeInt
                        cognome= polizza.cognomeInt
                    }
                    mailService.invioMail(polizza.emailInt, nome, cognome, polizza.dataPosticipata.format("dd-MM-yyyy").toString(),polizza.noPratica)
                }
                redirect action: "lista", params: params.search ? [search: params.search] : [:]
            } else {
                logg =new rcabmw.utenti.Log(parametri: "errore creazione polizza ${renderErrors(bean: polizza)}", operazione: " nuova polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "errori-->${renderErrors(bean: polizza)}"
                flash.error = renderErrors(bean: polizza)
            }
        }
        [polizza: polizza, dealers: dealer, province: province]
    }
    def caricaDealer(long idDealer){
        def risposta
        if(Dealer.exists(idDealer)) {
            def dealer = Dealer.get(idDealer)
            risposta=[indirizzo:dealer.indirizzo,piva:dealer.piva,localita:dealer.localita,cap:dealer.cap,provincia:dealer.provincia,email:dealer.email?:"",telefono:dealer.telefono?:"",fax:dealer.fax?:"",risposta:true]
        } else{risposta=[risposta:false]}
        render risposta as JSON
    }
    def nonInteressato(String idPolizza){
        def logg
        def polizza=Polizza.get(idPolizza)
        def risposta
        if(polizza){
            polizza.stato=StatoPolizza.NON_INTERESSATO
            if(polizza.save(flush:true)){
                risposta=[risposta:true]
                render risposta as JSON
            }
            else {
                risposta =[risposta: "Errore nel aggiornamento dello stato del preventivo ${renderErrors(bean: polizza)}"]
                logg =new rcabmw.utenti.Log(parametri: "Errore nel aggiornamento dello stato del preventivo ${renderErrors(bean: polizza)}", operazione: "non interesato", pagina: "Polizza posticipata")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Errore caricamento polizza: ${polizza.errors}"
                render risposta as JSON
            }
        }
    }
    def inserisceLog(String idPolizza,String areaLog, String dataLog){
        def logg
        def risposta
        def polizza=Polizza.get(idPolizza)
        def logAttivita=new LogAttivita()
        logAttivita.descrizionePosticipazione=areaLog.replace(",",";")
        logAttivita.dataAggiornamento=dataLog.parseDate("dd-MM-yyyy")
        if(logAttivita.save(flush:true)){
            logAttivita.addToPolizze(polizza)
            risposta=[risposta:true]
        }else{
            risposta=[risposta:false, errore:renderErrors(bean: logAttivita).toString().replaceAll("\\<[^>]*>","")]
            logg =new rcabmw.utenti.Log(parametri: "Errore nell'inserimento ${renderErrors(bean: logAttivita).toString()}", operazione: "non interesato", pagina: "Polizza posticipata")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
        render risposta as JSON
    }
    def elencoCommenti(String id){
        def risposta
        def polizza=Polizza.get(id)
        def logAttivita=LogAttivita.createCriteria().list(){
            projections {
                property('dataAggiornamento')
                property('descrizionePosticipazione')
            }
            polizze{
                eq ('id', polizza.id)
            }
            and {
                order('dataAggiornamento', 'asc')
            }
        }
        if(logAttivita){
            risposta=[logAttivita_data:logAttivita, risposta: true]
        }else{
            risposta=[risposta:false]
        }
        render risposta as JSON
    }
    def scaricaFascicoloInformativo() {
        def src = this.class.getResource("/pdf/fascicolo_informativo.pdf").file
        src = src.replaceAll("%23", "#")
        if (src) {
            def stream = new FileInputStream(src)
            response.contentType = "application/octet-stream"
            response.addHeader "Content-disposition", "inline; filename=Fascicolo informativo.pdf"
            response.outputStream << stream.bytes
        }
    }
    def scaricamoduloAdesione() {
        def src = this.class.getResource("/pdf/mod_adesione_bmw.pdf").file
        src = src.replaceAll("%23", "#")
        if (src) {
            def stream = new FileInputStream(src)
            response.contentType = "application/octet-stream"
            response.addHeader "Content-disposition", "inline; filename=Modulo adesione BMW.pdf"
            response.outputStream << stream.bytes
        }
    }
    private def modulo_polizza(long id,String nomeCliente, String cognomeCliente, String targa, String nopratica){
        def polizza=Polizza.get(id)
        def nome = nomeCliente
        def cognome = cognomeCliente

        def src = this.class.getResource("/pdf/mod_adesione_bmw.pdf").file
        src = src.replaceAll("%23", "#")
        if (src) {
            def stream = new FileInputStream(src)
            PdfReader reader = new PdfReader(stream)
            def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
            canvas.saveState()
            writeText(canvas, 233, 466, "${nome}  ${cognome}", 10, "nero","normal")
            writeText(canvas, 141, 443, targa.toString().toUpperCase(), 10, "nero","normal")
            writeText(canvas, 310, 416, nopratica, 10, "nero","normal")
            //writeText(canvas, 395, 340, polizza.dataDecorrenza.format("dd/MM/yyyy"), 10, "nero","normal")

            writeText(canvas, 395, 340, new Date().format("dd/MM/YYYY"), 10, "nero","normal")
            /*writeText(canvas, 410, 340, new Date().format("MM"), 8, "nero","normal")
            writeText(canvas, 425, 340, new Date().format("yyyy"), 8, "nero","normal")*/
            canvas = writer.getOverContent(2)
            writer.close()
            reader.close()

           // response.contentType = "application/pdf"
            //response.addHeader "Content-disposition", "inline; filename=modulodiAttivazione.pdf"
            //response.outputStream << output.toByteArray()
            return output.toByteArray()
        }
    }
    def generaFilePolizzeattivate(){

        def polizze=Polizza.createCriteria().list(){
            eq "stato", StatoPolizza.POLIZZA
        }

        def wb = Stopwatch.log { ExcelBuilder.create {
            style("header") {
                background bisque
                font {
                    bold(true)
                }
            }
            sheet("Polizze attivate") {
                row(style: "header") {
                    cell("Dealer")
                    cell("Numero")
                    cell("Nome")
                    cell("Cognome")
                    cell("Marchio")
                    cell("Targa")
                    cell("Telaio")
                    cell("Data Immatricolazione")
                    cell("Data Decorrenza")
                    cell("Data Scadenza")

                }
                if(polizze){
                    polizze.each { polizza ->
                        /* def dataScadenza= use(TimeCategory) { polizza.dataDecorrenza +12.months}
                         GregorianCalendar calendar = new GregorianCalendar()
                         def yearInt = Integer.parseInt(dataScadenza.format("yyyy"))
                         def monthInt = Integer.parseInt(dataScadenza.format("MM"))
                         monthInt = monthInt - 1
                         calendar.set(yearInt, monthInt, 1)
                         def dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
                         dataScadenza=new GregorianCalendar(yearInt,monthInt,dayInt).format("dd-MM-yyyy")*/
                        row {
                            cell(polizza.dealer.ragioneSociale)
                            cell(polizza.noPolizza)
                            cell((polizza.tipoContraente.toString()=="garante")? polizza.nomeGarante : polizza.nomeInt)
                            cell((polizza.tipoContraente.toString()=="garante")? polizza.cognomeGarante : polizza.cognomeInt)
                            cell((polizza.tipoContraente.toString()=="garante")? polizza.partitaIvaGarante : polizza.partitaIvaInt)
                            cell(polizza.marchio)
                            cell(polizza.targa)
                            cell(polizza.telaio)
                            cell(polizza.dataImmatricolazione)
                            cell(polizza.dataDecorrenza)
                            cell(polizza.dataScadenza)
                        }
                        polizza.discard()
                    }
                    for (int i = 0; i < 12; i++) {
                        sheet.autoSizeColumn(i);
                    }
                }else{
                    row{
                        cell("non ci sono polizze attivate")
                    }
                }
            }

        } }

        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response.addHeader "Content-disposition", "inline; filename=polizze_${new Date().format("ddMMyyyy")}.xlsx"
        response.outputStream << stream.toByteArray()
    }
    def writeText(canvas, x, y, String text, fontSize, color, font){
        def fontsB = [
                "normal": BaseFont.HELVETICA,
                "bold": BaseFont.COURIER_BOLD
        ]
        def carattere = BaseFont.createFont(fontsB[font], BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        canvas.beginText()
        canvas.moveText(x as float,y as float)
        canvas.setFontAndSize(carattere, fontSize as float)
        if(color == "rosso") canvas.setRGBColorFill(192, 0, 0)
        else if (color == "bianco") canvas.setRGBColorFill(255, 255, 255)
        else if (color == "grigio") canvas.setRGBColorFill(147, 147, 147)
        else canvas.setRGBColorFill(0, 0, 0)
        canvas.showText(text)
        canvas.endText()
    }
    def createCanvasAndStreams(document, page) {
        def output = new ByteArrayOutputStream()
        def writer = new PdfStamper(document, output)
        PdfContentByte canvas = writer.getOverContent(page)
        return [canvas, output, writer]
    }
    def scaricaCertificato(long id) {
        def polizza = Polizza.read(id)
        if(polizza) {
            def documento=Documenti.createCriteria().get() {
                eq "polizza", polizza
                eq "tipo", TipoDocumento.CERTIFICATO
            }
            if(documento) {
                response.with {
                    contentType = "application/octet-stream"
                    setHeader("Content-disposition", "attachment; filename=${documento.fileName}")
                    outputStream << documento.fileContent
                }
            } else response.sendError(404)
        }else response.sendError(404)
    }
    def inviaRichiesta(long idPolizza, String nomeCliente, String cognomeCliente,String emailCliente){
        def logg
        def polizza=Polizza.get(idPolizza)
        def risposta, returnMail
        if(polizza && emailCliente.toString().trim()!=''){
            returnMail= mailService.invioRichiesta(emailCliente, nomeCliente, cognomeCliente, polizza.noPratica)
            if(returnMail){
                logg =new rcabmw.utenti.Log(parametri: "Mail invita al cliente ${nomeCliente}", operazione: " aggiornamento polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                risposta=[risposta:true]
                render risposta as JSON
            }else{
                logg =new rcabmw.utenti.Log(parametri: "Errore nel invio della mail ${nomeCliente}", operazione: " aggiornamento polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                risposta =[risposta: "Errore nel invio della mail"]
                println "Errore nel invio della mail"
                render risposta as JSON
            }
        }else{
            logg =new rcabmw.utenti.Log(parametri: "Errore nel invio della mail ${nomeCliente}, controllare che la mail sia stata inserita", operazione: " aggiornamento polizza", pagina: "POLIZZE")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            risposta =[risposta: "Errore nel invio della mail, controllare che la mail sia stata inserita"]
            println "Errore nel invio della mail, controllare che la mail sia stata inserita"
            render risposta as JSON
        }

    }
    def inviaRichiestaAdmin(String noPratica, String nomeCliente, String cognomeCliente,String emailCliente){
        def logg
        def risposta, returnMail
        if(emailCliente.toString().trim()!=''){
            returnMail= mailService.invioRichiesta(emailCliente, nomeCliente, cognomeCliente, noPratica)
            if(returnMail){
                logg =new rcabmw.utenti.Log(parametri: "Mail invita al cliente ${nomeCliente}", operazione: " aggiornamento polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                risposta=[risposta:true]
                render risposta as JSON
            }else{
                logg =new rcabmw.utenti.Log(parametri: "Errore nel invio della mail ${nomeCliente}", operazione: " aggiornamento polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                risposta =[risposta: "Errore nel invio della mail"]
                println "Errore nel invio della mail"
                render risposta as JSON
            }
        }else{
            logg =new rcabmw.utenti.Log(parametri: "Errore nel invio della mail ${nomeCliente}, controllare che la mail sia stata inserita", operazione: " aggiornamento polizza", pagina: "POLIZZE")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            risposta =[risposta: "Errore nel invio della mail, controllare che la mail sia stata inserita"]
            println "Errore nel invio della mail, controllare che la mail sia stata inserita"
            render risposta as JSON
        }

    }
    def compilaModulo(long id,String nome, String cognome, String targa, String nopratica) {
        def polizza=Polizza.findById(id)
        if(polizza) {
            def fileContent = modulo_polizza(id,nome, cognome, targa, nopratica)
            def certificatoTriplicato = unisciCertificati([fileContent, fileContent, fileContent])
            response.contentType = "application/pdf"
            response.addHeader "Content-disposition", "attachment; filename=modulodiAdesione.pdf"
            response.outputStream << certificatoTriplicato
        }else response.sendError(404)

    }
    def compilaModuloSalvato(long id) {

        def polizza=Polizza.get(id)
        if(polizza) {
            def nome= (polizza.tipoContraente.toString()=="garante")? polizza.nomeGarante : polizza.nomeInt
            def cognome= (polizza.tipoContraente.toString()=="garante")? polizza.cognomeGarante : polizza.cognomeInt
            def fileContent = modulo_polizza(id,nome, cognome, polizza.targa, polizza.noPratica)
            def certificatoTriplicato = unisciCertificati([fileContent, fileContent, fileContent])
            response.contentType = "application/pdf"
            response.addHeader "Content-disposition", "attachment; filename=modulodiAdesione.pdf"
            response.outputStream << certificatoTriplicato
        }else response.sendError(404)

    }
    private byte[] unisciCertificati(Collection<byte[]> certificati) {
        log.debug "begin merge ${certificati.size()} pdf"
        def output = new ByteArrayOutputStream()
        if(certificati) {
            def pdf = new Document()
            def writer = PdfWriter.getInstance(pdf, output)
            pdf.open()
            certificati.each { byte[] certificato ->
                if (certificato) {
                    def pdfReader = new PdfReader(certificato)
                    for (def i = 1; i <= pdfReader.numberOfPages; i++) {
                        pdf.newPage()
                        def page = writer.getImportedPage(pdfReader, i)
                        writer.directContent.addTemplate(page, 0, 0)
                    }
                }
            }
            pdf.close()
        }
        output.close()
        log.debug "${certificati.size()} pdf merged"
        return output.toByteArray()
    }
    def caricaPratica() {
        def logg
        if(request.post) {
            def pratichePresente=[], praticheNuove=[], errorePratica=[]
            def dealerMancante=[]
            def file =params.excelPratica
            def filename=""
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def response =[]
                Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
                Pattern pattern = ~/(\w+)/
                String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>";
                def x_value=[]
                def dealer
                def dealers=[]
                String line
                try {
                    def wb = ExcelReader.readXlsx(myInputStream) {
                            sheet {
                                rows(from: 1) {
                                    def cellnoPratica = cell("A").value
                                    def cellCognome  = cell("C").value
                                    def cellPIva = cell("D").value
                                    def cellSesso = cell("E").value
                                    def cellIndirizzo = cell("F").value
                                    def cellProvincia = cell("G").value
                                    def cellLocalita = cell("H").value
                                    def cellCap = cell("I").value
                                    def cellaDealer = cell("J").value
                                    def cellaRagSocDealer = cell("K").value

                                    int numParole=pattern.matcher(cellCognome).count
                                    //println numParole
                                    def nome=""
                                    def cognome=" "
                                    def parole=[]
                                    parole= cellCognome.split(" ")

                                    if(parole.size()==2){
                                        nome=parole[0]
                                        cognome=parole[1]
                                    }else if(parole.size()>2){
                                        nome=parole[0]
                                        for ( int ind=1; ind<parole.size();ind++) {

                                            cognome+=parole[ind]+" "
                                        }
                                    }
                                    if(cellnoPratica.toString().equals("null")){
                                        cellnoPratica=""
                                    }else{
                                        println "nopratica1 ${cellnoPratica}"
                                        logg =new rcabmw.utenti.Log(parametri: "nopratica1 ${cellnoPratica}", operazione: " carica pratica", pagina: "POLIZZE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        //cellnoPratica=cellnoPratica.toString().replaceFirst ("^0*", "")
                                        cellnoPratica=cellnoPratica.toString().replaceAll ("\\.", "")
                                        logg =new rcabmw.utenti.Log(parametri: "nopratica1 dopo replace punto ${cellnoPratica}", operazione: " carica pratica", pagina: "POLIZZE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        if(cellnoPratica.toString().contains("E")){
                                            def punto=cellnoPratica.toString().indexOf("E")
                                            cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                        }
                                        logg =new rcabmw.utenti.Log(parametri: "nopratica dopo containsE ${cellnoPratica}", operazione: " carica pratica", pagina: "POLIZZE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                    if(cellCap.toString().equals("null")){
                                        cellCap=""
                                    }else{
                                        if(cellCap.toString().contains(".")){
                                            def punto=cellCap.toString().indexOf(".")
                                            cellCap=cellCap.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                        }
                                    }
                                    if(cellaDealer.toString().equals("null")){
                                        cellaDealer=""
                                    }else{
                                        cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                                        if(cellaDealer.toString().contains(".")){
                                            def punto=cellaDealer.toString().indexOf(".")
                                            cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                        }
                                        dealer=Dealer.findByUsername(cellaDealer.toString())
                                    }
                                    cellPIva=cellPIva.toString().replaceAll("[^a-z,A-Z,0-9]","")
                                    def sesso
                                    if(cellSesso.toString().trim().equals("null")){
                                        sesso=""
                                    }else if (cellSesso.toString().trim().equalsIgnoreCase("M")) sesso=TipoCliente.M
                                    else if (cellSesso.toString().trim().equalsIgnoreCase("F")) sesso=TipoCliente.F
                                    else sesso=TipoCliente.DITTA_INDIVIDUALE
                                    if(dealer && cellnoPratica) {
                                        println "nopratica ${cellnoPratica}"
                                        def polizzaesistente = Polizza.findByNoPraticaIlike(cellnoPratica)
                                        if (polizzaesistente) {
                                            errorePratica.add("la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale")
                                            logg =new rcabmw.utenti.Log(parametri: "la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale ${polizzaesistente}", operazione: " carica pratica", pagina: "POLIZZE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        } else {
                                            def polizza = Polizza.findOrCreateWhere(
                                                    noPratica: cellnoPratica,
                                                    dealer: dealer,
                                                    nomeInt: nome.toString().trim(),
                                                    cognomeInt: cognome.toString().trim(),
                                                    partitaIvaInt: cellPIva.toString().trim(),
                                                    tipoClienteInt: sesso,
                                                    indirizzoInt: cellIndirizzo.toString().trim(),
                                                    localitaInt: cellLocalita.toString().trim(),
                                                    capInt: cellCap.toString().trim(),
                                                    provinciaInt: cellProvincia.toString().trim()
                                            )
                                            if (polizza.save(flush: true)) {
                                                logg =new rcabmw.utenti.Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: " carica pratica", pagina: "POLIZZE")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                response.add("polizza creata correttamente ${polizza.noPolizza}")
                                                praticheNuove.add("polizza creata correttamente ${polizza.noPolizza}")
                                            } else {
                                                logg =new rcabmw.utenti.Log(parametri: "Errore creazione polizza ${cellnoPratica}: ${polizza.errors}", operazione: " carica pratica", pagina: "POLIZZE")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                response.add(noPratica: cellnoPratica, errore: "Errore creazione polizza: ${polizza.errors}")
                                                errorePratica.add("Errore creazione polizza: ${polizza.errors}")
                                            }
                                    }
                                    }else if(!dealer){
                                        logg =new rcabmw.utenti.Log(parametri: "Errore creazione polizza  ${cellnoPratica}: il dealer con codice ${cellaDealer} e ragione sociale ${cellaRagSocDealer} non \u00E8 presente", operazione: " carica pratica", pagina: "POLIZZE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza ${cellnoPratica} : il dealer con codice ${cellaDealer}  e ragione sociale ${cellaRagSocDealer} non \u00E8 presente")
                                        errorePratica.add("Errore creazione polizza: il dealer non \u00E8 presente")
                                    }else if(!cellnoPratica){
                                        logg =new rcabmw.utenti.Log(parametri: "Errore creazione polizza: ${cellnoPratica} non \u00E8 presente un numero di pratica", operazione: " carica pratica", pagina: "POLIZZE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: non \u00E8 presente un numero di pratica")
                                        errorePratica.add("Errore creazione polizza: non \u00E8 presente un numero di pratica")
                                    }
                                }
                            }
                    }
                }
                 catch (e){
                println "verificare il formato del file"
                errorePratica.add("verificare il formato del file")
                 logg =new rcabmw.utenti.Log(parametri: "Errore caricamento file pratiche ${e.toString()}", operazione: " carica pratica", pagina: "POLIZZE")
                 if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
                if(errorePratica.size()>0){
                    flash.errorePratica=[]
                    flash.errorePratica=errorePratica
                }
                if(praticheNuove.size()>0){
                    flash.praticheNuove=[]
                    flash.praticheNuove=praticheNuove
                }

            }
            else flash.error = "Specificare l'elenco xlsx"
            redirect action: "lista"
        }else response.sendError(404)
    }

}
