package rcabmw

import rcabmw.utenti.Dealer
import security.SecurityUtils

class UserController {

    def index() {}

    def datiDealer(){
        def principal = SecurityUtils.principal
        def utente
        utente=Dealer.get(principal.id)

        if(request.post) {
            if(SecurityUtils.hasRole("ADMIN")){
                redirect controller: "menu", action: "sceltaMenu"
            }else if(SecurityUtils.hasRole("DEALER")){
                if((params.telefono.toString().trim()!='' && params.telefono.toString().trim() != utente.telefono )) utente.telefono=params.telefono.toString().trim()
                if((params.fax.toString().trim()!='' && params.fax.toString()!= utente.fax)) utente.fax=params.fax.toString().trim()
                if((params.email.toString().trim()!='' && params.email.toString().trim() != utente.email )) utente.email=params.email.toString().trim()

                utente.attivo=true
                if(utente.save(flush: true)) {
                    //render (view:"nu/sceltaMenu", model: [utente: dealer] )
                    redirect uri:  "/", model: [utente: utente]
                }else{
                    println utente.errors
                }
            }
        }
        [utente:utente]
    }
    def inserisceDealer(){
        def dealer=new Dealer()
        if(request.post) {
            if(SecurityUtils.hasRole("ADMINDEALER")){
                params.ragioneSociale=params.ragioneSociale.toString().trim().toUpperCase()
                params.localita=params.localita.toString().trim().toUpperCase()
                params.provincia=params.provincia.toString().trim().toUpperCase()
                params.indirizzo=params.indirizzo.toString().trim().toUpperCase()
                bindData(dealer, params,[exclude: ['username','piva','telefono','fax','email']])
                def piva,username
                if(params.piva.toString().length()<11){
                    piva=params.piva.toString().trim().padLeft(11,"0")
                }else{
                    piva=params.piva
                }
                username=params.username.toString().trim().replaceAll("^0*","")
                dealer.username=username
                dealer.password=piva
                dealer.piva=piva
                dealer.attivo=false
                dealer.enabled=true
                dealer.passwordScaduta=false
                if((params.telefono.toString().trim()!='' )) dealer.telefono=params.telefono.toString().trim()
                if((params.fax.toString().trim()!='')) dealer.fax=params.fax.toString().trim()
                if((params.email.toString().trim()!='')) dealer.email=params.email.toString().trim()
                if(dealer.save(flush:true)){
                    flash.message="DEALER INSERITO"
                }else{
                    println "errori-->${renderErrors(bean: dealer)}"
                    flash.error = renderErrors(bean: dealer)
                }
            }else if(SecurityUtils.hasRole("DEALER")){
                redirect uri:  "/"
            }
            redirect uri:  "/"
        }

    }
}
