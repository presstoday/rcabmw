package rcabmw

import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch

import grails.transaction.Transactional
import grails.util.Environment
import groovy.time.TimeCategory
import excel.reader.*
import rcabmw.polizze.Polizza
import rcabmw.polizze.TipoCliente

import java.text.DecimalFormat
import java.util.regex.Pattern
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import rcabmw.utenti.*


@Transactional
class CaricaPolizzeJob {
    def mailService
    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: "CaricaPolizze", cronExpression: "0 15 12 * * ?"
        }else{
          //  cron name: "CaricaPolizze", cronExpression: "0 30  9 * * ?"
        }

    }

    def group = "CaricamentoPolizze"
    def description = "job per caricare le polizze"
    def execute() {
        try {
            downloadFile()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def downloadFile() {
        def logg
        def host,port,username,password

            host = "5.249.141.51"
            port = 22
            username = "flussi-bmw"
            password = "Camb1a1a"


        try {
            def jsch = new JSch()
            def session = jsch.getSession(username, host, port)
            def properties = new Properties()
            properties.put("StrictHostKeyChecking", "no")
            session.config = properties
            session.userName = username
            session.password = password
            session.connect()
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new rcabmw.utenti.Log(parametri: "Connected to $host:$port", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Connected to $host:$port"
            logg =new rcabmw.utenti.Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Current sftp directory: ${channel.pwd()}"
            if(Environment.current == Environment.PRODUCTION) {
                channel.cd("BMWNOTTE")
            }else{
                channel.cd("BMWNOTTE/TEST")
            }

            logg =new rcabmw.utenti.Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Current sftp directory: ${channel.pwd()}"
            def streamRiassunti = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamRiassunti)
            Vector<ChannelSftp.LsEntry> list =  channel.ls("*.CSV")
            def inserimento, fileNameRisposta, risposta, rispostaFinale,rispostaMail,filename
            def i=0
            if(list.size()>0){
                logg =new rcabmw.utenti.Log(parametri: "sono stati trovati:${list.size()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "sono stati trovati:${list.size()}"
                for(ChannelSftp.LsEntry entry : list) {
                    boolean errori=false
                    logg =new rcabmw.utenti.Log(parametri: "downloaded: ${entry.filename}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "downloaded: ${entry.filename}"
                    InputStream stream =  channel.get(entry.filename)
                    //createPolizzeCSV(stream)
                    filename="${entry.filename}"
                    filename = filename.substring(0, filename.lastIndexOf("."));
                    inserimento = createPolizzeCSV(stream)
                    fileNameRisposta = "risposta_${filename}${i}_${new Date().format("yyyyMMdd")}.txt"
                    rispostaMail="Riassunto inserimento:\r\n"
                    inserimento.collect{ commento ->
                        if (commento.messaggio){
                            rispostaMail =rispostaMail +" "+ commento.noPratica +" "+ commento.messaggio+"\r\n"
                        }else if (commento.errore){
                            errori=true
                            rispostaMail =rispostaMail+" "+ commento.noPratica +" "+ commento.errore+"\r\n"
                        }
                    }.grep().join("\n")
                    i++
                    zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
                    zipRiassunti.write(rispostaMail.bytes)
                   /* if(errori){
                        zipRiassunti.write(rispostaMail.bytes)
                    }*/
                    if(Environment.current == Environment.PRODUCTION) {
                        channel.rename("/home/flussi-bmw/BMWNOTTE/"+entry.filename,"/home/flussi-bmw/BMWNOTTE/"+entry.filename+".old")
                        println "moved: ${entry.filename}"
                        logg =new rcabmw.utenti.Log(parametri: "moved: ${entry.filename}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                    if(errori){
                        def inviaMailCaricamento=mailService.invioMailCaricamento(rispostaMail, filename)
                        if(inviaMailCaricamento.contains("queued")||inviaMailCaricamento.contains("sent") ||inviaMailCaricamento.contains("scheduled") ){
                            println "la mail di carimenti falliti \u00E8 stata inviata"
                            logg =new rcabmw.utenti.Log(parametri: "la mail di carimenti falliti \u00E8 stata inviata", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            //response.add(noPratica:'', messaggio:"la mail di carimenti falliti \u00E8 stata inviata")
                        }else{
                            logg =new rcabmw.utenti.Log(parametri: "Non \u00E8 stato possibile inviare la mail di carimenti falliti", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            println "Non \u00E8 stato possibile inviare la mail di carimenti falliti"
                            // response.add(noPratica:'', errore:"Non \u00E8 stato possibile inviare la mail di carimenti falliti")
                        }
                    }
                }
                zipRiassunti.close()
                streamRiassunti.close()
                channel.put(new ByteArrayInputStream(streamRiassunti.toByteArray()), "riassunto_${new Date().format("yyyyMMdd")}.zip")
                println "riassunto_${new Date().format("yyyyMMdd")}"
                logg =new rcabmw.utenti.Log(parametri: "riassunto_${new Date().format("yyyyMMdd")}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Current sftp directory: ${channel.pwd()}"
                logg =new rcabmw.utenti.Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }else {
                list = channel.ls("*.csv")
                if(list.size()>0){
                    println "sono stati trovati:${list.size()}"
                    logg =new rcabmw.utenti.Log(parametri: "sono stati trovati:${list.size()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    for(ChannelSftp.LsEntry entry : list) {
                        boolean errori=false
                        println "downloaded: ${entry.filename}"
                        logg =new rcabmw.utenti.Log(parametri: "downloaded: ${entry.filename}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        InputStream stream =  channel.get(entry.filename)
                        //createPolizzeCSV(stream)
                        filename="${entry.filename}"
                        filename = filename.substring(0, filename.lastIndexOf("."));
                        inserimento = createPolizzeCSV(stream)
                        fileNameRisposta = "risposta_${filename}${i}_${new Date().format("yyyyMMdd")}.txt"
                        risposta="Riassunto inserimento:\r\n"
                        rispostaMail="Riassunto errori:\r\n"
                        inserimento.collect{ commento ->
                            if (commento.messaggio){
                                risposta =risposta +" "+ commento.noPratica +" "+ commento.messaggio+"\r\n"
                            }else if (commento.errore){
                                errori=true
                                rispostaMail =rispostaMail+" "+ commento.noPratica +" "+ commento.errore+"\r\n"
                            }
                        }.grep().join("\n")
                        rispostaFinale=risposta
                        i++
                        zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
                        zipRiassunti.write(rispostaFinale.bytes)
                        if(errori){
                            zipRiassunti.write(rispostaMail.bytes)
                        }
                        if(Environment.current == Environment.PRODUCTION) {
                            channel.rename("/home/flussi-bmw/BMWNOTTE/"+entry.filename,"/home/flussi-bmw/BMWNOTTE/"+entry.filename+".old")
                            println "moved: ${entry.filename}"
                            logg =new rcabmw.utenti.Log(parametri: "moved: ${entry.filename}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }

                        if(errori){
                            def inviaMailCaricamento=mailService.invioMailCaricamento(rispostaMail, filename)
                            if(inviaMailCaricamento==true){
                                logg =new rcabmw.utenti.Log(parametri: "la mail di carimenti falliti \u00E8 stata inviata", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                println "la mail di carimenti falliti \u00E8 stata inviata"
                                //response.add(noPratica:'', messaggio:"la mail di carimenti falliti \u00E8 stata inviata")
                            }else{
                                logg =new rcabmw.utenti.Log(parametri: "Non \u00E8 stato possibile inviare la mail di carimenti falliti", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                println "Non \u00E8 stato possibile inviare la mail di carimenti falliti"
                                // response.add(noPratica:'', errore:"Non \u00E8 stato possibile inviare la mail di carimenti falliti")
                            }
                        }
                    }
                    zipRiassunti.close()
                    streamRiassunti.close()
                    if(Environment.current == Environment.PRODUCTION) {
                        channel.put(new ByteArrayInputStream(streamRiassunti.toByteArray()), "riassunto_${new Date().format("yyyyMMdd")}.zip")
                        logg =new rcabmw.utenti.Log(parametri: "riassunto_${new Date().format("yyyyMMdd")}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        println "riassunto_${new Date().format("yyyyMMdd")}"
                        logg =new rcabmw.utenti.Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        println "Current sftp directory: ${channel.pwd()}"
                    }

                }
            }
            channel.disconnect()
            session.disconnect()
        } catch(e) { e.printStackTrace() }
    }

    def createPolizze(InputStream file){
        def response =[]
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{5}$)/
        Pattern pattern = ~/(\w+)/
        def dealer
        try {
            def wb = ExcelReader.readXlsx(file) {
                sheet {
                    rows(from: 1) {
                        def cellnoPratica = cell("A").value
                        def cellCodiceCampagana  = cell("B").value
                        def cellCognome  = cell("C").value
                        def cellPIva = cell("D").value
                        def cellSesso = cell("E").value
                        def cellIndirizzo = cell("F").value
                        def cellProvincia = cell("G").value
                        def cellLocalita = cell("H").value
                        def cellCap = cell("I").value
                        def cellaDealer = cell("J").value

                        int numParole=pattern.matcher(cellCognome).count
                        //println numParole
                        def nome=""
                        def cognome=" "
                        def parole=[]
                        parole= cellCognome.split(" ")

                        if(parole.size()==2){
                            nome=parole[0]
                            cognome=parole[1]
                        }else if(parole.size()>2){
                            nome=parole[0]
                            for ( int ind=1; ind<parole.size();ind++) {

                                cognome+=parole[ind]+" "
                            }
                        }
                        if(cellnoPratica.toString().equals("null")){
                            cellnoPratica="";
                        }else{
                            cellnoPratica=cellnoPratica.toString().replaceFirst ("^0*", "")
                            if(cellnoPratica.toString().contains(".")){
                                def punto=cellnoPratica.toString().indexOf(".")
                                cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        if(cellCap.toString().equals("null")){
                            cellCap="";
                        }else{
                            if(cellCap.toString().contains(".")){
                                def punto=cellCap.toString().indexOf(".")
                                cellCap=cellCap.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        if(cellaDealer.toString().equals("null")){
                            cellaDealer="";
                        }else{
                            cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                            if(cellaDealer.toString().contains(".")){
                                def punto=cellaDealer.toString().indexOf(".")
                                cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                            dealer=Dealer.findByUsername(cellaDealer.toString())
                        }
                        cellPIva=cellPIva.toString().replaceAll("[^a-z,A-Z,0-9]","")
                        def sesso
                        if(cellSesso.toString().trim().equals("null")){
                            sesso="";
                        }else if (cellSesso.toString().trim().equalsIgnoreCase("M")) sesso=TipoCliente.M
                        else if (cellSesso.toString().trim().equalsIgnoreCase("F")) sesso=TipoCliente.F
                        else sesso=TipoCliente.DITTA_INDIVIDUALE
                        if(dealer && cellnoPratica){
                            def polizza = Polizza.findOrCreateWhere(
                                    noPratica: cellnoPratica,
                                    dealer: dealer,
                                    nomeInt: nome.toString().trim(),
                                    cognomeInt:  cognome.toString().trim(),
                                    partitaIvaInt: cellPIva.toString().trim(),
                                    tipoClienteInt: sesso,
                                    indirizzoInt: cellIndirizzo.toString().trim(),
                                    localitaInt: cellLocalita.toString().trim(),
                                    capInt: cellCap.toString().trim(),
                                    provinciaInt: cellProvincia.toString().trim()
                            )
                            if(polizza.save(flush:true)) {
                                response.add(noPratica:cellnoPratica, messaggio:"polizza creata correttamente ${polizza.noPolizza}")
                            }
                            else response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizza.errors}")
                        }else if(!dealer){
                            response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: il dealer non \u00E8 presente")
                        }else if(!cellnoPratica){
                            response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: non \u00E8 presente un numero di pratica")
                        }
                    }
                }
            }
        }catch (e){
            println "c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli"
            response.add(codice:null, errore:"c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli")
        }

        return response
    }
    def createPolizzeCSV(InputStream file){
        def logg
        def response =[]
        Pattern pattern = ~/(\w+)/
        def fileReader = null
        def reader = null
        //Delimiter used in CSV file
        def delimiter = ";"
        def dealer

        try {
            def line = ""
            //Create the file reader
            reader = new InputStreamReader(file)
            fileReader = new BufferedReader(reader)
            fileReader.readLine()
            while ((line = fileReader.readLine()) != null)
            {

                //Get all tokens available in line
                def tokens = []
                tokens = line.split(delimiter)
                def cellnoPratica = tokens[0].toString().trim()
                def cellCognome  = tokens[2].toString().trim()
                def cellPIva = tokens[3].toString().trim()
                def cellSesso = tokens[4].toString().trim()
                def cellIndirizzo = tokens[5].toString().trim()
                def cellProvincia = tokens[6].toString().trim()
                def cellLocalita = tokens[7].toString().trim()
                def cellCap = tokens[8].toString().trim().replace("'","")
                def cellaDealer = tokens[9].toString().trim()
                //int numParole=pattern.matcher(cellCognome).count
                //println numParole
                def nome=""
                def cognome=" "
                def parole=[]
                parole= cellCognome.split(" ")
                if(parole.size()==2){
                    nome=parole[1]
                    cognome=parole[0]
                }else if(parole.size()>2){
                    cognome=parole[0]
                    for ( int ind=1; ind<parole.size();ind++) {
                        nome+=parole[ind]+" "
                    }
                }
                if(cellnoPratica.equals("null")){
                    cellnoPratica=""
                }else{
                    cellnoPratica=cellnoPratica.replaceFirst ("^0*", "")
                    if(cellnoPratica.contains(".")){
                        def punto=cellnoPratica.indexOf(".")
                        cellnoPratica=cellnoPratica.substring(0,punto).replaceAll("[^0-9]", "")
                    }
                }
                if(cellCap.equals("null")){
                    cellCap=""
                }else{
                    if(cellCap.toString().contains(".")){
                        def punto=cellCap.indexOf(".")
                        cellCap=cellCap.substring(0,punto).replaceAll("[^0-9]", "")
                    }
                }
                if(cellaDealer.equals("null")){
                    cellaDealer=""
                }else{
                    cellaDealer=cellaDealer.replaceFirst ("^0*", "")
                    if(cellaDealer.contains(".")){
                        def punto=cellaDealer.indexOf(".")
                        cellaDealer=cellaDealer.substring(0,punto).replaceAll("[^0-9]", "")
                    }
                    dealer=Dealer.findByUsername(cellaDealer)
                }
                cellPIva=cellPIva.replaceAll("[^a-z,A-Z,0-9]","")
                def sesso
                if(cellSesso.equals("null")){
                    sesso=""
                }else if (cellSesso.equalsIgnoreCase("M")) sesso=TipoCliente.M
                else if (cellSesso.equalsIgnoreCase("F")) sesso=TipoCliente.F
                else sesso=TipoCliente.DITTA_INDIVIDUALE
                if(dealer && cellnoPratica){
                    def praticaCaricata=Polizza.findByNoPratica(cellnoPratica.toString().trim())
                    if(!praticaCaricata){
                        def polizza = Polizza.findOrCreateWhere(
                                noPratica: cellnoPratica,
                                dealer: dealer,
                                nomeInt: nome.toString().trim(),
                                cognomeInt:  cognome.toString().trim(),
                                partitaIvaInt: cellPIva.toString().trim(),
                                tipoClienteInt: sesso,
                                indirizzoInt: cellIndirizzo.toString().trim(),
                                localitaInt: cellLocalita.toString().trim(),
                                capInt: cellCap.toString().trim(),
                                provinciaInt: cellProvincia.toString().trim()
                        )
                        if(polizza.save(flush:true)) {
                            logg =new rcabmw.utenti.Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            response.add(noPratica:"${polizza.noPratica}", messaggio:"polizza creata correttamente ${polizza.noPolizza}")
                        }
                        else{
                            logg =new rcabmw.utenti.Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizza.errors}")
                        }
                    }else{
                        logg =new rcabmw.utenti.Log(parametri: "La pratica \u00E8 presente a sistema", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        response.add(noPratica:cellnoPratica, errore:"La pratica  \u00E8 presente a sistema")
                    }

                }else if(!dealer){
                    logg =new rcabmw.utenti.Log(parametri: "Errore creazione polizza: il dealer non \u00E8 presente", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: il dealer non \u00E8 presente")
                }else if(!cellnoPratica){
                    logg =new rcabmw.utenti.Log(parametri: "Errore creazione polizza: non \u00E8 presente un numero di pratica", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: non \u00E8 presente un numero di pratica")
                }
            }

       }catch (e){
            logg =new rcabmw.utenti.Log(parametri: "c'\u00E8 un problema con il file, ${file}--> ${e.toString()} ", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli sino nel csv"
            response.add(codice:null, errore:"c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli sono nel csv")
        }

        return response
    }
}
