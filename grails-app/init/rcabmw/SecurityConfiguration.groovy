package rcabmw

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.security.SecurityProperties
import org.springframework.context.annotation.*
import org.springframework.core.annotation.Order
import org.springframework.security.config.annotation.web.configuration.*
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.util.matcher.*
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler
import org.springframework.security.web.authentication.RememberMeServices
import org.springframework.security.web.authentication.rememberme.*
import javax.servlet.http.HttpServletRequest
import java.util.regex.Pattern
import security.*

import javax.servlet.http.HttpServletRequest
import java.util.regex.Pattern

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean PasswordEncoder passwordEncoder() { return new BCryptPasswordEncoder() }

    @Bean UserDetailsService userDetailsService() { return new RcabmwUserDetailsService() }

    @Bean ExceptionMappingAuthenticationFailureHandler authenticationFailureHandler() {
        def failureHandler = new RcabmwAuthenticationFailureHandler()
        failureHandler.defaultFailureUrl = "/login"
        failureHandler.setExceptionMappings([
                /*"org.springframework.security.authentication.DisabledException": "...",
                "org.springframework.security.authentication.LockedException": "...",
                "org.springframework.security.authentication.AccountExpiredException": "..."*/
                "org.springframework.security.authentication.CredentialsExpiredException": "/passwordScaduta",
                /*"org.springframework.security.authentication.BadCredentialsException": "/login"*/
        ])
        return failureHandler
    }

    @Bean RememberMeServices rememberMeService() {
        def rememberMeService = new TokenBasedRememberMeServices(UUID.randomUUID().toString(), userDetailsService())
        rememberMeService.parameter = "remember-me"
        rememberMeService.cookieName = "spring-security-remember-me"
        rememberMeService.tokenValiditySeconds = 60 * 60 * 24 * 7
        return rememberMeService
    }

    /*@Bean SwitchUserFilter switchUserProcessingFilter() {
        def filter = new SwitchUserFilter(
                userDetailsService: userDetailsService(),
                switchUserUrl: "/loginAs",
                exitUserUrl: "/logoutAs",
                targetUrl: "/",
                usernameParameter: "username"
        )
        return filter
    }*/

    @Autowired void configureGlobal(AuthenticationManagerBuilder auth) throws Exception { auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder()) }

    @Override protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/passwordScaduta").permitAll()//gli asset (css, javascript e immagini) sono sempre accessibili
                .anyRequest().authenticated()                                   //per accedere a tutto il resto serve un autenticazione
                .and()
                .formLogin()
                .loginPage("/login")
                .failureHandler(authenticationFailureHandler())
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login")
                .deleteCookies("remember-me")
                .permitAll()
                .and()
                .rememberMe()
                .rememberMeServices(rememberMeService())
                .and()
                .exceptionHandling()
                .accessDeniedPage("/notAuthorized")
                .and()
                .csrf()
                .requireCsrfProtectionMatcher(new CsrfRequestMatcher())
    }

    /**
     * La protezione CSRF entra in gioco durante il post di un form
     * Lo disabilito per il controller della console altrimenti non la posso usare
     */
    private static class CsrfRequestMatcher implements RequestMatcher {
        private allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)\$")
        private def consoleMatcher = new AntPathRequestMatcher("/console/**")
        boolean matches(HttpServletRequest request) {
            if(allowedMethods.matcher(request.getMethod()).matches()) return false
            if(consoleMatcher.matches(request)) return false
            return true
        }

    }

}
