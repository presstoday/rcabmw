package utils

import org.hibernate.criterion.CriteriaSpecification
import org.hibernate.transform.ResultTransformer

trait CriteriaUtils {

    ResultTransformer getAliasToMap() { return CriteriaSpecification.ALIAS_TO_ENTITY_MAP }

    ResultTransformer getRootEntity() { return CriteriaSpecification.ROOT_ENTITY }

    ResultTransformer getDistinctRootEntity() { return CriteriaSpecification.DISTINCT_ROOT_ENTITY }

    ResultTransformer getProjection() { return CriteriaSpecification.PROJECTION }

}