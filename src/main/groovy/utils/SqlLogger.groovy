package utils

import ch.qos.logback.classic.*
import org.slf4j.*

class SqlLogger {

    static def log(Closure query) {
        Logger sql = LoggerFactory.getLogger("org.hibernate.SQL")
        def level = sql.level
        sql.level = Level.DEBUG
        def result = query()
        sql.level = level
        return result
    }

    static def logWithTrace(Closure query) {
        Logger sql = LoggerFactory.getLogger("org.hibernate.SQL")
        def sqlLevel = sql.level
        Logger params = LoggerFactory.getLogger("org.hibernate.type.descriptor.sql.BasicBinder")
        def paramsLevel = params.level
        sql.level = Level.DEBUG
        params.level = Level.TRACE
        def result = query()
        sql.level = sqlLevel
        params.level = paramsLevel
        return result
    }

}
