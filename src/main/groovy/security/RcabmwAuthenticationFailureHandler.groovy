package security

import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.*
import javax.servlet.ServletException
import javax.servlet.http.*

public class RcabmwAuthenticationFailureHandler  extends ExceptionMappingAuthenticationFailureHandler {
    static final String LAST_USERNAME_KEY = "LAST_USERNAME"

    @Override
    void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        super.onAuthenticationFailure(request, response, exception)
        def username = request.getParameter("username") ?: request.getParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY);
        logger.debug "Storing last username in session"
        def session = request.session
        session.setAttribute(LAST_USERNAME_KEY, username)
        logger.debug "Session[${LAST_USERNAME_KEY}] = ${username}"
    }
}
