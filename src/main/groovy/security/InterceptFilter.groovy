package security

import groovy.util.logging.Slf4j

import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Slf4j
class InterceptFilter implements Filter {

    void init(FilterConfig filterConfig) throws ServletException {}

    void destroy() {}

    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        def url = ((HttpServletRequest)request).requestURI
        log.debug "Intercepted url: ${url}"
        if(url == "/RCABMW") {
            log.debug "Url match /RCABMW, performing redirect to /RCABMW/"
            ((HttpServletResponse)response).sendRedirect("/RCABMW/")
        } else chain.doFilter(request, response)
    }
}