package extensions

import groovy.transform.CompileStatic
import javax.servlet.http.HttpServletRequest

@CompileStatic
class HttpServletRequestExtension {

    static boolean isPut(HttpServletRequest request) { return request.method == "PUT" }

    static boolean isDelete(HttpServletRequest request) { return request.method == "DELETE" }

}