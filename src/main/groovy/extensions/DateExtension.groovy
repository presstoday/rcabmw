package extensions

import groovy.transform.CompileStatic
import java.text.SimpleDateFormat

@CompileStatic
class DateExtension {

    static String format(Date target, String format = "dd-MM-yyyy") { return new SimpleDateFormat(format).format(target) }

}
