package extensions
import groovy.transform.CompileStatic

import java.text.*
import org.codehaus.groovy.runtime.GStringImpl
import org.apache.commons.lang3.StringUtils

class StringExtension {
    @CompileStatic
    static Date parseDate(String target, String format = "dd-MM-yyyy") {
        def formatter = new SimpleDateFormat(format)
        try { return formatter.parse(target) }
        catch(e) { return null }
    }

    @CompileStatic
    static BigDecimal parseNumber(String target) {
        DecimalFormat formatter = (DecimalFormat)DecimalFormat.getInstance(Locale.default)
        formatter.setParseBigDecimal(true)
        try { return formatter.parse(target) }
        catch(e) { return null }
    }

    @CompileStatic
    static String padLeft(String target, int length, String paddingChar = " ") {
        def string = target.length() > length ? target.substring(0, length) : target
        return StringUtils.leftPad(string, length, paddingChar)
    }

    @CompileStatic
    static String padRight(String target, int length, String paddingChar = " ") {
        def string = target.length() > length ? target.substring(0, length) : target
        return StringUtils.rightPad(string, length, paddingChar)
    }

    @CompileStatic
    static String center(String target, int length, String paddingChar = " ") {
        def string = target.length() > length ? target.substring(0, length) : target
        return StringUtils.center(string, length, paddingChar)
    }

    static Date parseDate(GStringImpl target, String format = "dd-MM-yyyy") { return target.toString().parseDate(format) }

    static String padLeft(GStringImpl target, int length, String paddingChar = " ") { return target.toString().padLeft(length, paddingChar) }

    static String padRight(GStringImpl target, int length, String paddingChar = " ") { return target.toString().padRight(length, paddingChar) }

    static String center(GStringImpl target, int length, String paddingChar = " ") { return target.toString().center(length, paddingChar) }

    @CompileStatic
    static String splitCamelCase(String target, boolean capitalize = false) {
        def string = capitalize ? target.capitalize() : target
        return string.replaceAll(String.format("%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])",  "(?<=[A-Za-z])(?=[^A-Za-z])"), " ")
    }

    @CompileStatic
    static String uncapitalize(String target) { return StringUtils.uncapitalize(target) }

    static String hyphenate(String target) { return target?.replaceAll("\\.|_| |\\[|\\]", "-")?.replaceAll(/\B[A-Z]/, { "-" + it })?.toLowerCase() }
}
